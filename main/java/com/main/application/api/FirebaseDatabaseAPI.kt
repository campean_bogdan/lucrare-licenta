package com.main.application.api

import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonElement
import com.google.gson.JsonNull
import com.google.gson.JsonObject
import com.main.application.model.firebase.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

// firebase database
interface FirebaseDatabaseAPI {

    /**
     * Register new user in database.
     * @param user new user object
     */
    @PUT("users/{userUID}.json")
    @Headers("Content-Type: application/json")
    fun registerUser(@Path("userUID") userUID: String, @Body user: User): Deferred<Response<JsonObject>>

    /**
     * Get a user by 'name'. This is a generic method
     * @param orderBy is the parameter (ex. name, email)
     * @param equalTo value of the parameter (ex. "Alice", "alice_33@gmail.com")
     */
    @GET("users.json")
    @Headers("Content-Type: application/json")
    fun getUserByEmail(@Query("auth") auth: String, @Query("orderBy") orderBy: String, @Query("equalTo") equalTo: String): Deferred<Response<JsonObject>>

    /**
     * Get a list of users based on roles.
     */
    @GET("users.json")
    @Headers("Content-Type: application/json")
    fun getUsersByRole(@Query("auth") auth: String, @Query("orderBy") orderBy: String, @Query("equalTo") equalTo: String): Deferred<Response<JsonObject>>

    /**
     * Insert new route when user choose a destination.
     * @param auth firebase access token
     * @param route new route object
     */
    @POST("routes/{userUID}.json")
    @Headers("Content-Type: application/json")
    fun addNewRoute(@Path("userUID") userUID: String, @Query("auth") auth: String, @Body route: Route): Deferred<Response<JsonObject>>

    /**
     * Update new added route. It will insert new informations about route when the route is over.
     * @param routeKey uid of the route, which is saved in local storage until it is over
     * @param auth firebase access token
     * @param route object with new informations which are added
     */
    @PATCH("routes/{userUID}/{routeKey}.json")
    @Headers("Content-Type: application/json")
    fun updateRoute(@Path("userUID") userUID: String, @Path("routeKey") routeKey: String, @Query("auth") auth: String, @Body route: RouteUpdate): Deferred<Response<JsonObject>>

    /**
     * Add new point of interest.
     * @param auth firebase access token
     */
    @POST("points_of_interest/{userUID}.json")
    @Headers("Content-Type: application/json")
    fun addNewPointOfInterest(@Path("userUID") userUID: String, @Query("auth") auth: String, @Body pointOfInterest: PointOfInterest): Deferred<Response<JsonObject>>

    /**
     * Get a point of interest by a parameter. To get a POI by name "home":
     * orderBy="name", equalTo="home"
     * @param userUID user's local id
     * @param auth firebase access token
     * @param orderBy is the parameter (ex. name, date)
     * @param equalTo value of the parameter (ex. "home", "Lidl shop")
     */
    @GET("points_of_interest/{userUID}.json")
    @Headers("Content-Type: application/json")
    fun getPointOfInterest(@Path("userUID") userUID: String, @Query("auth") auth: String, @Query("orderBy") orderBy: String, @Query("equalTo") equalTo: String): Deferred<Response<JsonObject>>

    /**
     * Delete a point of interest by key
     * @param poiKey point of interest's key
     * @param auth firebase access token
     */
    @DELETE("points_of_interest/{userUID}/{poiKey}.json")
    @Headers("Content-Type: application/json")
    fun deletePointOfInterest(@Path("userUID") userUID: String, @Path("poiKey") poiKey: String, @Query("auth") auth: String): Deferred<Response<JsonNull>>

    /**
     * Send/Response to an invitation to an email address.
     * @param patientUID person's UID which receives the email
     * @param caregiverUID person's UID which sends the email
     * @param invitation invitation response; by default its value is PENDING
     */
    @PUT("users/{patientUID}/caregivers/{caregiverUID}.json")
    @Headers("Content-Type: application/json")
    fun sendInvitationToEmail(@Path("patientUID") patientUID: String, @Path("caregiverUID") caregiverUID: String, @Query("auth") auth: String, @Body invitation: String): Deferred<Response<String>>

    /**
     * Delete invitation from database.
     * @param patientUID person's UID which receives the email
     * @param caregiverUID person's UID which sends the email
     */
    @DELETE("users/{patientUID}/caregivers/{caregiverUID}.json")
    @Headers("Content-Type: application/json")
    fun deleteInvitation(@Path("patientUID") patientUID: String, @Path("caregiverUID") caregiverUID: String, @Query("auth") auth: String): Deferred<Response<JsonElement>>

    /**
     * Insert last known location of a patient in database.
     * @param patientUID patient's UID
     * @param auth firebase access token
     * @param location patient's location
     */
    @PUT("last_location/{patientUID}/location.json")
    @Headers("Content-Type: application/json")
    fun addPatientLastKnownLocation(@Path("patientUID") patientUID: String, @Query("auth") auth: String, @Body location: LatLng): Deferred<Response<JsonObject>>

    @DELETE("last_location/{patientUID}.json")
    @Headers("Content-Type: application/json")
    fun deletePatientLastKnownLocation(@Path("patientUID") patientUID: String, @Query("auth") auth: String): Deferred<Response<JsonNull>>

    /**
     * Insert help when a patient needs help.
     * @param patientUID patient's UID
     * @param auth firebase access token
     * @param help a string with "TRUE" value used for notifying caregivers
     */
    @PUT("last_location/{patientUID}/help.json")
    @Headers("Content-Type: application/json")
    fun addPatientNeedHelp(@Path("patientUID") patientUID: String, @Query("auth") auth: String, @Body help: String): Deferred<Response<String>>

    @GET("last_location/{patientUID}/location.json")
    @Headers("Content-Type: application/json")
    fun getHelpedPatientLocation(@Path("patientUID") patientUID: String, @Query("auth") auth: String): Deferred<Response<JsonObject>>

    @DELETE("last_location/{patientUID}/help.json")
    @Headers("Content-Type: application/json")
    fun deleteHelp(@Path("patientUID") patientUID: String, @Query("auth") auth: String): Deferred<Response<JsonNull>>

    /**
     * @param params query parameters from firebase: "orderBy", "startAt", "endAt"
     * @return A JSON object which contains all road events in a given interval made by latitude
     */
    @GET("road_events.json")
    @Headers("Content-Type: application/json")
    fun getRoadEventsFromInterval(@QueryMap params: Map<String, String>): Deferred<Response<JsonElement>>

    /**
     * Insert a new road event.
     */
    @POST("road_events.json")
    @Headers("Content-Type: application/json")
    fun insertRoadEvent(@Query("auth") auth: String, @Body roadEvent: RoadEvent): Deferred<Response<JsonObject>>

    /**
     * Get all road events.
     */
    @GET("road_events.json")
    @Headers("Content-Type: application/json")
    fun getAllRoadEvents(@Query("auth") auth: String): Deferred<Response<JsonElement>>

    @DELETE("road_events/{roadEventUid}.json")
    @Headers("Content-Type: application/json")
    fun deleteRoadEvent(@Path("roadEventUid") roadEventKey: String, @Query("auth") auth: String): Deferred<Response<JsonElement>>

    /**
     * Add a defined route.
     */
    @POST("defined_routes/{key}.json")
    @Headers("Content-Type: application/json")
    fun addDefinedRoute(@Path("key") path: String, @Query("auth") auth: String, @Body definedRoute: DefinedRoute): Deferred<Response<JsonElement>>

    /**
     * Get all defined routes for a patient.
     */
    @GET("defined_routes/{key}.json")
    @Headers("Content-Type: application/json")
    fun getPatientDefinedRoutes(@Path("key") path: String, @Query("auth") auth: String): Deferred<Response<JsonElement>>

    /**
     * Delete a defined route from a patient.
     * @param patientKey patient uuid
     * @param definedRouteKey defined route key from DB
     */
    @DELETE("defined_routes/{patientKey}/{definedRouteKey}.json")
    @Headers("Content-Type: application/json")
    fun deleteDefinedRoute(@Path("patientKey") patientKey: String, @Path("definedRouteKey") definedRouteKey: String, @Query("auth") auth: String): Deferred<Response<JsonElement>>

    @GET("defined_routes/{patientKey}/{definedRouteKey}.json")
    fun getDefinedRouteDetails(@Path("patientKey") patientKey: String, @Path("definedRouteKey") definedRouteKey: String, @Query("auth") auth: String): Deferred<Response<JsonElement>>
}