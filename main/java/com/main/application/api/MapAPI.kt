package com.main.application.api

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface MapAPI {

    /**
     * send options to server to get directions
     * @param options origin, destination & optionals (first is origin, the second is destination)
     */
    @GET("directions/json")
    fun getDirections(@QueryMap options: Map<String, String>): Call<JsonElement>

    /**
     * send options to server to get current location
     * @param options lat, lng & other optional parameters
     */
    @GET("geocode/json")
    fun getCurrentLocation(@QueryMap options: Map<String, String>): Call<JsonObject>

    @GET("http://www.overpass-api.de/api/interpreter")
    fun osmGetLocationDetails(@Query("data") query: String): Call<String>
}