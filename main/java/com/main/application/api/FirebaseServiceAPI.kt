package com.main.application.api

import com.google.gson.JsonElement
import com.main.application.model.auth.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

// firebase authentication
interface FirebaseServiceAPI {

    @POST(":signUp")
    @Headers("Content-Type: application/json")
    fun registerUserInFirebase(@Query("key") key: String, @Body authRequest: AuthRequest): Deferred<Response<AuthResponse>>

    @POST(":signInWithPassword")
    @Headers("Content-Type: application/json")
    fun loginUser(@Query("key") key: String, @Body authRequest: AuthRequest): Deferred<Response<AuthResponse>>

    @POST("token")
    @Headers("Content-Type: application/json")
    fun changeRefreshTokenForIDToken(@Query("key") key: String, @Body refreshTokenBody: RefreshRequest): Deferred<Response<RefreshResponse>>

    @POST(":sendOobCode")
    @Headers("Content-Type: application/json")
    fun resetPassword(@Query("key") key: String, @Body resetPassword: ResetPasswordRequest): Deferred<Response<JsonElement>>
}