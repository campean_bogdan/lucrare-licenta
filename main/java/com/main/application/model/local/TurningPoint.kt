package com.main.application.model.local

import com.google.android.gms.maps.model.LatLng

data class TurningPoint(
    var location: LatLng,
    var direction: String,
    var angle: Int
)