package com.main.application.model.local

import com.google.android.gms.maps.model.LatLng

data class Point(
    var coordinates: LatLng
)