package com.main.application.model.firebase

import com.google.android.gms.maps.model.LatLng
import java.util.*

data class PointOfInterest(
    val location: LatLng? = null,
    val name: String = "",
    val date: Date = Date()
)