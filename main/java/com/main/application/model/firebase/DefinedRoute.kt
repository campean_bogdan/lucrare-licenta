package com.main.application.model.firebase

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import java.util.*

data class DefinedRoute(
    @SerializedName("name") var name: String = "",
    @SerializedName("points") var points: List<LatLng>? = null,
    @SerializedName("total_length") var totalLength: Int = 0,
    @Transient var key: String = "",
    @SerializedName("date") var date: Date = Date()
)