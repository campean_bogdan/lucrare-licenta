package com.main.application.model.firebase

import com.google.android.gms.maps.model.LatLng
import java.util.*

/**
 * @param averageSpeed in meters per second
 * @param totalTime in seconds
 * @param distance in meters
 */
data class Route(
    var startPoint: LatLng = LatLng(0.0, 0.0),
    var endPoint: LatLng = LatLng(0.0, 0.0),
    var date: Date = Date(),
    var distance: Int = -1,
    var totalTime: Int = -1,
    var averageSpeed: Int = -1
)