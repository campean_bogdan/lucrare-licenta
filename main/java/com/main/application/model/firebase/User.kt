package com.main.application.model.firebase

import com.google.gson.annotations.SerializedName

data class User(
    val name: String,
    val email: String,
    @Transient
    val password: String,
    val city: String,
    @SerializedName("device_register_token")
    val deviceRegisterToken: String,
    val role: String = "DEFAULT",
    val caregivers: Map<String, String>? = null,
    @Transient var key: String = ""
)