package com.main.application.model.firebase

import com.google.android.gms.maps.model.LatLng
import com.main.application.constants.AppConstants
import java.util.*

// data class RoadEvents in model/service module
// which is later mapped to another data class
data class RoadEvent(
    val category: AppConstants.RoadEventsCategory,
    val latitude: Double,
    val longitude: Double,
    val email: String,
    val date: Date = Date()
) {
    fun toLatLng(): LatLng {
        return LatLng(latitude, longitude)
    }
}