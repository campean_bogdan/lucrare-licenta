package com.main.application.model.firebase

/**
 * This class is used when user arrives at destination and data in Route must be updated
 */
data class RouteUpdate(
    var distance: Int = -1,
    var totalTime: Int = -1,
    var averageSpeed: Float = -1.0f
)