package com.main.application.model.auth

data class ResetPasswordRequest(
    val email: String,
    val requestType: String = "PASSWORD_RESET"
)