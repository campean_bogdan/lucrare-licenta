package com.main.application.model.auth

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    @SerializedName("localId") var uuid: String? = null,
    var idToken: String? = null,
    var refreshToken: String? = null,
    val expiresIn: String? = null
)