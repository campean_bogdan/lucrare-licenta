package com.main.application.model.auth

import com.google.gson.annotations.SerializedName

data class RefreshResponse(
    @SerializedName("id_token") val idToken: String,
    @SerializedName("user_id") val userId: String,
    @SerializedName("refresh_token") val refreshToken: String,
    @SerializedName("expires_in") val expiresIn: String,
    @SerializedName("token_type") val tokenType: String,
    @SerializedName("project_id") val projectId: String
)