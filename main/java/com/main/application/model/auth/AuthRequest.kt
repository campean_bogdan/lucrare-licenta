package com.main.application.model.auth

data class AuthRequest(
    val email: String,
    val password: String,
    private val returnSecureToken: String = "true"
)