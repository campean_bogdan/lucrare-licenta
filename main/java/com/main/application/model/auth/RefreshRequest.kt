package com.main.application.model.auth

import com.google.gson.annotations.SerializedName

data class RefreshRequest(
    @SerializedName("refresh_token")
    var refreshToken: String,
    @SerializedName("grant_type")
    private var grantType: String = "refresh_token"
)