package com.main.application.service.firebase_database

import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.main.application.api.FirebaseDatabaseAPI
import com.main.application.constants.InvitationResponse
import com.main.application.constants.Roles
import com.main.application.model.firebase.*
import com.main.application.retrofit.BaseUrls
import com.main.application.retrofit.RetrofitFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.HttpException

class FirebaseDatabaseImpl: FirebaseDatabase {

    private companion object {
        const val POINT_OF_INTEREST_ORDER_BY_NAME = "\"name\""
        const val USER_ORDER_BY_EMAIL = "\"email\""
        const val USER_ORDER_BY_ROLE = "\"role\""
        const val ROAD_EVENT_ORDER_BY_LATITUDE = "\"latitude\""
        const val ROLE_PATIENT = "\"PATIENT\""

        const val ORDER_BY = "orderBy"
        const val START_AT = "startAt"
        const val END_AT = "endAt"
        const val TOKEN = "auth"
    }


    override fun registerUserInDatabase(
        userUID: String,
        user: User,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit) {

        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.registerUser(userUID, user).await()
                if (response.code() in 200..299) {
                    onSuccess()
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun getUserByEmail(
        token: String,
        email: String,
        onSuccess: (User) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getUserByEmail(
                    token,
                    USER_ORDER_BY_EMAIL,
                    "\"$email\""
                ).await()
                if (response.code() in 200..299) {
                    val res = response.body()!!.entrySet()
                    println("RES:   $res")
                    if (res.isNotEmpty()) {
                        val user = gson.fromJson(response.body()!!.entrySet().first().value, User::class.java)
                        onSuccess(user)
                    } else {
                        onError(1)
                    }
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun getAllPatients(
        uuid: String,
        token: String,
        onSuccess: (List<Pair<User, String>>) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            val patients = mutableListOf<Pair<User, String>>()
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getUsersByRole(token, USER_ORDER_BY_ROLE, ROLE_PATIENT).await()
                if (response.isSuccessful) {
                    if (response.code() in 200..299) {
                        // iterate al patients from DB
                        for (element in response.body()!!.entrySet()) {
                            val patient = gson.fromJson(element.value!!, User::class.java)
                            // iterate all caregivers of a patient
                            var ok = false
                            if (patient.caregivers != null) {
                                for (key in patient.caregivers.keys) {
                                    if (key == uuid) {
                                        patient.key = element.key!!
                                        patients.add(Pair(patient!!, patient.caregivers[key]!!))
                                        ok = true
                                    }
                                }
                                if (!ok) {
                                    patient.key = element.key!!
                                    patients.add(Pair(patient!!, InvitationResponse.NOT_CONNECTED))
                                }
                            } else {
                                patients.add(Pair(patient, InvitationResponse.NOT_CONNECTED))
                            }
                        }
                    }
                }
                onSuccess(patients)
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun getUserLocalId(
        token: String,
        email: String,
        onSuccess: (String) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getUserByEmail(
                    token,
                    USER_ORDER_BY_EMAIL,
                    "\"$email\""
                ).await()
                if (response.isSuccessful) {
                    if (response.code() in 200..299) {
                        if (!response.body()!!.entrySet().isNullOrEmpty()) {
                            onSuccess(response.body()!!.entrySet().first().key)
                        } else {
                            onError(1)
                        }
                    } else {
                        onError(response.code())
                    }
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    // verific campurile caregivers din obiectul JSON daca exista inainte sa fac conversia JSON -> User pentru eficienta
    override fun getCaregiverPatients(
        userUID: String,
        token: String,
        onSuccess: (MutableList<Pair<User, String>>) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            val patients = mutableListOf<Pair<User, String>>()
            coroutineScope.launch {
                val allPatientsResponse = firebaseDatabaseRetrofit.getUsersByRole(token, USER_ORDER_BY_ROLE, "\"${Roles.ROLE_PATIENT}\"").await()
                if (allPatientsResponse.code() in 200..299) {
                    val responseAsJson = allPatientsResponse.body()
                    if (responseAsJson != null && responseAsJson.entrySet().size > 0) {
                        for (entry in responseAsJson.entrySet()) {
                            val caregiversAsJson = entry.value.asJsonObject["caregivers"]
                            if (caregiversAsJson != null) {
                                val caregiverInvitationResponse = (caregiversAsJson as JsonObject)[userUID]
                                if (caregiverInvitationResponse != null) {
                                    val caregiverInvitationResponseAsString = caregiverInvitationResponse.asString
                                    if (!caregiverInvitationResponseAsString.isNullOrEmpty()) {
                                        val patientAsJson = responseAsJson[entry.key]
                                        val patient = gson.fromJson<User>(patientAsJson, User::class.java)
                                        patients.add(Pair(patient, caregiverInvitationResponseAsString))
                                    }
                                }
                            }
                        }
                    }
                }
                onSuccess(patients)
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun addNewRoute(userUID: String, route: Route, token: String, onSuccess: (JsonObject) -> Unit, onError: (Int) -> Unit) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.addNewRoute(userUID, token, route).await()
                if (response.code() in 200..299) {
                    onSuccess(response.body()!!)
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun updateRoute(
        userUID: String,
        route: RouteUpdate,
        routeKey: String,
        token: String,
        onSuccess: (JsonObject) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.updateRoute(userUID, routeKey, token, route).await()
                if (response.code() in 200..299) {
                    onSuccess(response.body()!!)
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun addNewPointOfInterest(
        userUID: String,
        pointOfInterest: PointOfInterest,
        token: String,
        onSuccess: (JsonObject) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.addNewPointOfInterest(userUID, token, pointOfInterest).await()
                if (response.code() in 200..299) {
                    onSuccess(response.body()!!)
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun getPointOfInterestByName(
        userUID: String,
        poiName: String,
        token: String,
        onSuccess: (JsonObject) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getPointOfInterest(userUID, token, POINT_OF_INTEREST_ORDER_BY_NAME, "\"" + poiName + "\"").await()
                if (response.code() in 200..299) {
                    onSuccess(response.body()!!)
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun deletePointOfInterestByKey(
        userUID: String,
        poiName: String,
        token: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val poiToDelete = firebaseDatabaseRetrofit.getPointOfInterest(userUID, token, POINT_OF_INTEREST_ORDER_BY_NAME, "\"$poiName\"").await()
                if (poiToDelete.body()!!.entrySet().size > 0) {
                    if (poiToDelete.code() in 200..299) {
                        val poiKey = poiToDelete.body()!!.entrySet().first().key!!
                        coroutineScope.launch {
                            val response = firebaseDatabaseRetrofit.deletePointOfInterest(userUID, poiKey, token).await()
                            if (response.code() in 200..299) {
                                onSuccess()
                            } else {
                                onError(response.code())
                            }
                        }
                    } else {
                        onError(poiToDelete.code())
                    }
                } else {
                    onError(1)
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun sendInvitationToEmail(
        currentUserUID: String,
        email: String,
        token: String,
        invitationResponse: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                // request pentru un pacient dupa email
                val getUserResponse = firebaseDatabaseRetrofit.getUserByEmail(token, USER_ORDER_BY_EMAIL, "\"$email\"").await()
                // daca pacientul exista in baza de date se trimite invitatie
                if (getUserResponse.code() in 200..299) {
                    val patientObject = getUserResponse.body()
                    var patientKey: String? = null
                    if (patientObject != null && patientObject.entrySet().size > 0) {
                        patientKey = patientObject.entrySet().first().key
                    }
                    if (patientKey != null) {
                        try {
                            val patient = Gson().fromJson<User>(patientObject!!.entrySet().first().value, User::class.java)
                            // verificare daca emailul apartine unui pacient
                            if (patient.role != Roles.ROLE_PATIENT) {
                                onError(1)
                            } else {
                                // request cu invitatie pentru un pacient
                                val sendInvitationResponse = firebaseDatabaseRetrofit.sendInvitationToEmail(patientKey, currentUserUID, token, invitationResponse).await()
                                if (sendInvitationResponse.code() in 200..299) {
                                    onSuccess()
                                } else {
                                    onError(sendInvitationResponse.code())
                                }
                            }
                        } catch (httpException: HttpException) {
                            onError(-1)
                        }
                    } else {
                        onError(0)
                    }
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun respondInvitation(
        caregiverUID: String,
        patientUID: String,
        token: String,
        invitationResponse: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.sendInvitationToEmail(patientUID, caregiverUID, token, invitationResponse).await()
                if (response.code() in 200..299) {
                    onSuccess()
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun deleteInvitation(
        caregiverUID: String,
        patientUID: String,
        token: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.deleteInvitation(patientUID, caregiverUID, token).await()
                if (response.code() in 200..299) {
                    onSuccess()
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun addPatientLastKnownLocation(
        patientUID: String,
        token: String,
        location: LatLng,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.addPatientLastKnownLocation(patientUID, token, location).await()
                if (response.code() in 200..299) {
                    onSuccess()
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun deletePatientLastKnownLocation(
        patientUID: String,
        token: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.deletePatientLastKnownLocation(patientUID, token).await()
                if (response.code() in 200..299) {
                    onSuccess()
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun addHelpForPatient(
        patientUID: String,
        token: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.addPatientNeedHelp(patientUID, token, "TRUE").await()
                if (response.code() in 200..299) {
                    onSuccess()
                }
            }
        } catch (httpException: HttpException) {
            onError(-1)
        }
    }

    override fun getHelpedPatientLocation(
        patientUID: String,
        token: String,
        onExecute: (LatLng) -> Unit,
        onSuccess: (execute: (LatLng) -> Unit, location: LatLng) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getHelpedPatientLocation(patientUID, token).await()
                if (response.code() in 200..299 && response.body() != null) {
                    val responseAsObject = gson.fromJson(response.body(), LatLng::class.java)
                    onSuccess(onExecute, responseAsObject)
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun deleteHelp(
        patientUID: String,
        token: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.deleteHelp(patientUID, token).await()
                if (response.isSuccessful) {
                    onSuccess()
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
        }
    }

    override fun getRoadEventsFromInterval(
        token: String,
        latLon: Pair<Pair<Double, Double>, Pair<Double, Double>>,
        function: (List<RoadEvent>) -> Unit,
        onSuccess: (execute: (List<RoadEvent>) -> Unit, List<RoadEvent>) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            val startLatitude = latLon.first.first
            val endLatitude = latLon.first.second
            val startLongitude = latLon.second.first
            val endLongitude = latLon.second.second
            val listOfRoadEvents = mutableListOf<RoadEvent>()
            val params = mutableMapOf<String, String>()
            params[TOKEN] = token
            params[ORDER_BY] = ROAD_EVENT_ORDER_BY_LATITUDE
            params[START_AT] = startLatitude.toString()
            params[END_AT] = endLatitude.toString()
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getRoadEventsFromInterval(params).await()
                if (response.code() in 200..299) {
                    if (!response.body()!!.isJsonNull) {
                        for (entry in response.body()!!.asJsonObject.entrySet()) {
                            val roadEvent = gson.fromJson(entry.value, RoadEvent::class.java)
                            // daca longitudinea punctului se incadreaza in limitele traseului => il adaug in lista
                            if (roadEvent.longitude > startLongitude && roadEvent.longitude < endLongitude)
                                listOfRoadEvents.add(roadEvent)
                        }
                        onSuccess(function, listOfRoadEvents)
                    } else {
                        onError(1)
                    }
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun addRoadEvent(
        token: String,
        roadEvent: RoadEvent,
        onSuccess: (String) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.insertRoadEvent(token, roadEvent).await()
                if (response.code() in 200..299) {
                    val key = response.body()!!.entrySet().first().value!!.asString
                    onSuccess(key)
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun getAllRoadEvents(
        token: String,
        execute: (Map<String, RoadEvent>) -> Unit,
        onSuccess: (function: (Map<String, RoadEvent>) -> Unit, Map<String, RoadEvent>) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getAllRoadEvents(token).await()
                if (response.code() in 200..299) {
                    if (!response.body()!!.isJsonNull) {
                        val listOfRoadEvents = mutableListOf<RoadEvent>()
                        val mapOfRoadEvents = mutableMapOf<String, RoadEvent>()
                        for (entry in response.body()!!.asJsonObject.entrySet()) {
                            val roadEvent = gson.fromJson(entry.value, RoadEvent::class.java)
                            //listOfRoadEvents.add(roadEvent)
                            mapOfRoadEvents[entry.key!!] =roadEvent
                        }
                        //onSuccess(execute, listOfRoadEvents)
                        onSuccess(execute, mapOfRoadEvents)
                    } else {
                        onError(1)
                    }
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun deleteRoadEvent(
        key: String,
        token: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.deleteRoadEvent(key, token).await()
                if (response.isSuccessful) {
                    onSuccess()
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun addDefinedRoute(
        token: String,
        localId: String,
        patientEmail: String,
        definedRoute: DefinedRoute,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val userResponse = firebaseDatabaseRetrofit.getUserByEmail(token, USER_ORDER_BY_EMAIL, "\"$patientEmail\"").await()
                if (userResponse.isSuccessful) {
                    val userJsonObj = userResponse.body()!!
                    if (userJsonObj.entrySet().isNotEmpty()) {
                        val paientLocalId = userJsonObj.entrySet().first().key!!
                        val patientObj = gson.fromJson(userJsonObj.entrySet().first().value!!, User::class.java)
                        if (patientObj.caregivers != null) {
                            if (patientObj.caregivers.contains(localId)) {
                                val definedRouteResponse = firebaseDatabaseRetrofit.getPatientDefinedRoutes(paientLocalId, token).await()
                                if (definedRouteResponse.isSuccessful && !definedRouteResponse.body()!!.isJsonNull) {
                                    var ok = true
                                    for (element in definedRouteResponse.body()!!.asJsonObject.entrySet()) {
                                        val drResponse = gson.fromJson(element.value, DefinedRoute::class.java)
                                        if (drResponse.name == definedRoute.name)
                                            ok = false
                                    }
                                    if (ok) {
                                        definedRoute.name = definedRoute.name.toLowerCase()
                                        val response = firebaseDatabaseRetrofit.addDefinedRoute(paientLocalId, token, definedRoute).await()
                                        if (response.isSuccessful) {
                                            onSuccess()
                                        } else {
                                            onError(2)
                                        }
                                    } else
                                        onError(4)
                                } else if (definedRouteResponse.isSuccessful && definedRouteResponse.body()!!.isJsonNull) {
                                    definedRoute.name = definedRoute.name.toLowerCase()
                                    val response = firebaseDatabaseRetrofit.addDefinedRoute(paientLocalId, token, definedRoute).await()
                                    if (response.isSuccessful) {
                                        onSuccess()
                                    } else {
                                        onError(2)
                                    }
                                }
                            } else
                                onError(5)
                        } else
                            onError(5)
                    } else {
                        onError(1)
                    }
                } else {
                    onError(3)
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun getPatientDefinedRouteByName(
        token: String,
        patientLocalId: String,
        name: String,
        sorted: Boolean,
        onSuccess: (List<LatLng>) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val allRoutes = firebaseDatabaseRetrofit.getPatientDefinedRoutes(patientLocalId, token).await()
                if (allRoutes.isSuccessful) {
                    if (!allRoutes.body()!!.isJsonNull) {
                        val nameLowerCase = name.toLowerCase()
                        var ok = false
                        for (routeAsJson in allRoutes.body()!!.asJsonObject.entrySet()) {
                            val route = gson.fromJson(routeAsJson.value, DefinedRoute::class.java)
                            if (route.name == nameLowerCase) {
                                ok = true
                                if (sorted)
                                    onSuccess(route.points!!)
                                else {
                                    val list = route.points!!.asReversed()
                                    onSuccess(route.points!!.asReversed())
                                }
                            }
                        }
                        if (!ok)
                            onError(3)
                    } else {
                        onError(2)
                    }
                } else {
                    onError(1)
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun getPatientDefinedRoutes(
        token: String,
        patientLocalId: String,
        onSuccess: (List<DefinedRoute>) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getPatientDefinedRoutes(patientLocalId, token).await()
                if (response.isSuccessful) {
                    if (response.code() in 200..299) {
                        if (!response.body()!!.isJsonNull) {
                            val entrySet = response.body()!!.asJsonObject.entrySet()
                            val definedRoutes = mutableListOf<DefinedRoute>()
                            for (element in entrySet) {
                                val definedRoute = gson.fromJson(element.value, DefinedRoute::class.java)
                                definedRoute.key = element.key
                                definedRoutes.add(definedRoute)
                            }
                            onSuccess(definedRoutes)
                        } else {
                            onError(1)
                        }
                    }
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun deleteDefinedRoute(
        token: String,
        patientLocalId: String,
        definedRouteKey: String,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.deleteDefinedRoute(patientLocalId, definedRouteKey, token).await()
                if (response.isSuccessful) {
                    if (response.code() in 200..299) {
                        onSuccess()
                    } else {
                        onError(1)
                    }
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun getDefinedRouteDetails(
        token: String,
        patientLocalId: String,
        definedRouteKey: String,
        onSuccess: (DefinedRoute) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseDatabaseRetrofit.getDefinedRouteDetails(patientLocalId, definedRouteKey, token).await()
                if (response.isSuccessful) {
                    if (response.code() in 200..299) {
                        val definedRoute = gson.fromJson(response.body()!!.asJsonObject, DefinedRoute::class.java)
                        onSuccess(definedRoute)
                    } else
                        onError(response.code())
                } else {
                    onError(response.code())
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }
}