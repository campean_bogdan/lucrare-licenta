package com.main.application.service.notifications

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.main.application.constants.InvitationResponse
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils

class InvitationResponseViewModel(application: Application): AndroidViewModel(application) {

    val invitationText = MutableLiveData<String>()
    val responseText = MutableLiveData<String>()
    val responseTitle = MutableLiveData<String>()
    val onLoadProgressBar = MutableLiveData<Boolean>()
    val onResponseSent = MutableLiveData<Boolean>()
    val accept = MutableLiveData<Boolean>()
    val decline = MutableLiveData<Boolean>()

    private var caregiverUid: String? = null
    private var patientUid: String? = null
    private var caregiverEmail: String? = null
    private val firebaseDatabase: FirebaseDatabase by lazy { FirebaseDatabaseImpl() }


    init {
        onLoadProgressBar.value = false
        onResponseSent.value = false
        accept.value = false
        decline.value = false
    }

    fun setInvitationText(email: String) {
        caregiverEmail = email
        invitationText.value = "$email sent you an invitation. He wants to be your caregiver. Do you accept?"
    }

    // users/{patientUID}/caregivers/{caregiverUID} = "ACCEPT"
    fun acceptInvitation() {
        onLoadProgressBar.value = true
        accept.value = true
        val token = LocalStorageUtils.getUserAndToken(getApplication()).second.idToken!!
        if (!caregiverUid.isNullOrEmpty() && !patientUid.isNullOrEmpty()) {
            firebaseDatabase.respondInvitation(
                caregiverUid!!,
                patientUid!!,
                token,
                InvitationResponse.ACCEPT,
                ::onSuccessAcceptInvitation,
                ::onErrorAcceptInvitation
            )
        }
    }

    fun declineInvitation() {
        onLoadProgressBar.value = true
        decline.value = true
        val token = LocalStorageUtils.getUserAndToken(getApplication()).second.idToken!!
        if (!caregiverUid.isNullOrEmpty() && !patientUid.isNullOrEmpty()) {
            firebaseDatabase.respondInvitation(
                caregiverUid!!,
                patientUid!!,
                token,
                InvitationResponse.DECLINE,
                ::onSuccessDeclineInvitation,
                ::onErrorDeclineInvitation
            )
        }
    }

    fun setCaregiverUid(senderUid: String) {
        this.caregiverUid = senderUid
    }

    fun setPatientUid(receiverUid: String) {
        this.patientUid = receiverUid
    }

    private fun onSuccessAcceptInvitation() {
        Log.i("Success", "Accept invitation with success!")
        onLoadProgressBar.value = false
        onResponseSent.value = true
        responseTitle.value = "Accept"
        responseText.value = "You have accepted the invitation. From now on, $caregiverEmail will be your caregiver."
    }

    private fun onErrorAcceptInvitation(error: Int) {
        Log.e("Error", "Accept invitation with error. Error code: $error")
        onLoadProgressBar.value = false
        onResponseSent.value = true
        responseText.value = "An error has occurred."
    }

    private fun onSuccessDeclineInvitation() {
        Log.i("Success", "Decline invitation with success!")
        onLoadProgressBar.value = false
        onResponseSent.value = true
        responseTitle.value = "Decline"
        responseText.value = "You have declined the invitation sent by \n$caregiverEmail."
    }

    private fun onErrorDeclineInvitation(error: Int) {
        Log.e("Error", "Decline invitation with error. Error code: $error")
        onLoadProgressBar.value = false
        onResponseSent.value = true
        responseText.value = "An error has occurred."
    }
}