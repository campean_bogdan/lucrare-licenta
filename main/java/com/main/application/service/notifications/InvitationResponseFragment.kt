package com.main.application.service.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.messaging.RemoteMessage
import com.main.application.R
import com.main.application.constants.NotificationType
import com.main.application.databinding.FragmentInvitationResponseBinding

class InvitationResponseFragment: Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[InvitationResponseViewModel::class.java]
    }

    // datele din firebase; in acest caz: cel care trimite invitatia e caregiver, iar cel care primeste este patient
    companion object {
        private const val CAREGIVER_UID = "senderUID"
        private const val PATIENT_UID = "receiverUID"
        private const val SENDER_EMAIL = "senderEmail"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentInvitationResponseBinding>(
            inflater,
            R.layout.fragment_invitation_response,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val bundle = activity!!.intent!!.extras
        val message = bundle!![NotificationType.INVITATION] as RemoteMessage

        if (!message.data[SENDER_EMAIL].isNullOrEmpty()) {
            viewModel.setInvitationText(message.data[SENDER_EMAIL]!!)
        }
        if (!message.data[CAREGIVER_UID].isNullOrEmpty()) {
            viewModel.setCaregiverUid(message.data[CAREGIVER_UID]!!)
        }
        if (!message.data[PATIENT_UID].isNullOrEmpty()) {
            viewModel.setPatientUid(message.data[PATIENT_UID]!!)
        }

        viewModel.accept.observe(this, Observer { shouldDrawGreen ->
            if (shouldDrawGreen) {
                val drawable = activity!!.resources.getDrawable(R.drawable.card_view_confirm)
                binding.invitationResponseLayout.background = drawable
            }
        })

        viewModel.decline.observe(this, Observer { shouldDrawRed ->
            if (shouldDrawRed) {
                val drawable = activity!!.resources.getDrawable(R.drawable.card_view_decline)
                binding.invitationResponseLayout.background = drawable
            }
        })

        return binding.root
    }
}