package com.main.application.service.notifications

import android.app.Notification
import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.main.application.CaregiverMapActivity
import com.main.application.InvitationResponseActivity
import com.main.application.R
import com.main.application.constants.NotificationType
import com.main.application.utils.LocalStorageUtils


class NotificationService: FirebaseMessagingService() {

    // se primeste un token nou doar cand aplicatia este instalata prima data
    override fun onNewToken(deviceRegisterToken: String) {
        super.onNewToken(deviceRegisterToken)
        LocalStorageUtils.saveDeviceRegisterToken(deviceRegisterToken, applicationContext)
    }

    override fun onMessageReceived(receivedMessage: RemoteMessage) {
        super.onMessageReceived(receivedMessage)

        val type = receivedMessage.data["type"]
        var invitationIntent: Intent? = null
        var helpIntent: Intent? = null
        var invitationPendingIntent: PendingIntent? = null
        var helpPendingIntent: PendingIntent? = null

        // cand primesc notificarea => din notificare iau caregiverUID si patientUID si le pun intr-un Bundle inainte sa porneasca activitatea de raspuns
        // cand activitatea porneste apasandu-se pe Yes sau No => in activitate se afla cheile celor 2 din tabelul caregivers_patients
        when (type) {
            NotificationType.INVITATION -> {
                // in acest caz => senderUID = caregiverUID; receiverUID = patientUID
                invitationIntent = Intent(applicationContext, InvitationResponseActivity::class.java)
                invitationIntent.putExtra(NotificationType.INVITATION, receivedMessage)
                invitationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                invitationPendingIntent = PendingIntent.getActivity(applicationContext, 0, invitationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                if (invitationPendingIntent != null) {
                    val notification = createInvitationNotification(invitationPendingIntent, receivedMessage)
                    with(NotificationManagerCompat.from(this)) {
                        notify(1, notification)
                    }
                }
            }
            NotificationType.HELP -> {
                helpIntent = Intent(applicationContext, CaregiverMapActivity::class.java)
                helpIntent.putExtra(NotificationType.HELP, receivedMessage)
                helpIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                helpPendingIntent = PendingIntent.getActivity(applicationContext, 0, helpIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                if (helpPendingIntent != null) {
                    val notification = createHelpNotification(helpPendingIntent, receivedMessage)
                    with(NotificationManagerCompat.from(this)) {
                        notify(1, notification)
                    }
                }
            }
        }
    }

    private fun createInvitationNotification(invitationPendingIntent: PendingIntent, receivedMessage: RemoteMessage): Notification {
        return NotificationCompat.Builder(this, "NOTIFICATION_CHANNEL_ID")
            .setSmallIcon(R.drawable.email_icon)
            .setContentTitle(receivedMessage.notification!!.title)
            .setContentText(receivedMessage.notification!!.body)
            .setOngoing(true)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(invitationPendingIntent)
            .build()
    }

    private fun createHelpNotification(helpPendingIntent: PendingIntent, receivedMessage: RemoteMessage): Notification {
        return NotificationCompat.Builder(this, "NOTIFICATION_CHANNEL_ID")
            .setSmallIcon(R.drawable.icon_error)
            .setContentTitle(receivedMessage.notification!!.title)
            .setContentText(receivedMessage.notification!!.body)
            .setOngoing(true)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(helpPendingIntent)
            .build()
    }
}