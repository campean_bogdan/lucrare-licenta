package com.main.application.service.firebase_services

import com.main.application.model.auth.*

// firebase service which handles the registration, login, delete account, etc - predefined APIs for those actions
interface FirebaseAuthentication {

    fun registerUserService(authRequest: AuthRequest, onSuccess: (AuthResponse) -> Unit, onError: (Int) -> Unit)
    fun loginUserService(authRequest: AuthRequest, onSuccess: (AuthResponse) -> Unit, onError: (Int) -> Unit)

    /**
     * Method is used to refresh the actual token.
     * @exception 1 = response was not successfully
     */
    fun refreshToken(refreshRequest: RefreshRequest, onSuccess: (RefreshResponse) -> Unit, onError: (Int) -> Unit)

    /**
     * Error: 1 = email not found
     */
    fun changePassword(resetPasswordRequest: ResetPasswordRequest, onSuccess: () -> Unit, onError: (Int) -> Unit)
}