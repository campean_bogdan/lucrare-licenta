package com.main.application.service.firebase_services

import android.content.Context
import com.main.application.R
import com.main.application.api.FirebaseServiceAPI
import com.main.application.model.auth.*
import com.main.application.retrofit.BaseUrls
import com.main.application.retrofit.RetrofitFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.HttpException

class FirebaseAuthenticationImpl(val context: Context): FirebaseAuthentication {

    // firebase services url
    private val firebaseServiceRetrofit = RetrofitFactory.createFirebaseRetrofit<FirebaseServiceAPI>(BaseUrls.FIREBASE_SERVICE_URL)
    private val firebaseRefreshToken = RetrofitFactory.createFirebaseRetrofit<FirebaseServiceAPI>(BaseUrls.FIREBASE_REFRESH_TOKEN_URL)
    private val firebaseKey = context.resources.getString(R.string.firebase_key)

    private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + job)


    override fun registerUserService(
        authRequest: AuthRequest,
        onSuccess: (AuthResponse) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response =
                    firebaseServiceRetrofit.registerUserInFirebase(firebaseKey, authRequest).await()
                if (response.isSuccessful) {
                    if (response.code() in 200..299) {
                        onSuccess(response.body()!!)
                    } else {
                        onError(response.code())
                    }
                }
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun loginUserService(
        authRequest: AuthRequest,
        onSuccess: (AuthResponse) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseServiceRetrofit.loginUser(firebaseKey, authRequest).await()
                if (response.isSuccessful) {
                    when (response.code()) {
                        in 200..299 -> onSuccess(response.body()!!)

                        else -> onError(response.code())
                    }
                } else
                    onError(response.code())
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun refreshToken(
        refreshRequest: RefreshRequest,
        onSuccess: (RefreshResponse) -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseRefreshToken.changeRefreshTokenForIDToken(firebaseKey, refreshRequest).await()
                if (response.isSuccessful) {
                    if (response.code() in 200..299) {
                        onSuccess(response.body()!!)
                    } else {
                        onError(response.code())
                    }
                } else
                    onError(response.code())
            }
        } catch (httpException: HttpException) {
            httpException.printStackTrace()
            onError(-1)
        }
    }

    override fun changePassword(
        resetPasswordRequest: ResetPasswordRequest,
        onSuccess: () -> Unit,
        onError: (Int) -> Unit
    ) {
        try {
            coroutineScope.launch {
                val response = firebaseServiceRetrofit.resetPassword(firebaseKey, resetPasswordRequest).await()
                if (response.isSuccessful) {
                    onSuccess()
                } else {
                    onError(1)
                }
            }
        } catch (httpEcveption: HttpException) {
            httpEcveption.printStackTrace()
            onError(-1)
        }
    }
}