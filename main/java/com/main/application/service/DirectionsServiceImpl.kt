package com.main.application.service

import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.main.application.api.MapAPI
import com.main.application.retrofit.RetrofitFactory
import com.main.application.utils.JsonUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DirectionsServiceImpl : DirectionsService {

    private val retrofit = RetrofitFactory.createDirectionsRetrofit<MapAPI>()
    private val gson = Gson()
    private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + job)


    override fun getDirections(
        options: Map<String, String>,
        onSuccess: (MutableList<List<LatLng>>) -> Unit,
        onError: (Int) -> Unit
    ) {
        retrofit.getDirections(options).enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                //onSuccess(response.body()!!)
                if (response.isSuccessful) {
                    if (response.code() in 200..299) {
                        if (response.body()!!.isJsonNull)
                            onError(1)
                        else {
                            val jsonObject = response.body()!!.asJsonObject
                            if (isStatusNull(jsonObject)) {
                                onError(1)
                            } else {
                                val pathPoints = JsonUtils.parseMapPoints(jsonObject)
                                onSuccess(pathPoints)
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                t.printStackTrace()
                onError(-1)
            }
        })
    }

    override fun getCurrentLocation(
        options: Map<String, String>,
        onSuccess: (JsonObject) -> Unit,
        onError: () -> Unit
    ) {
        retrofit.getCurrentLocation(options).enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                onSuccess(response.body()!!)
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                onError()
            }
        })
    }

    override fun osmGetLocationDetails(
        query: String,
        onSuccess: (String) -> Unit,
        onError: () -> Unit
    ) {
        retrofit.osmGetLocationDetails(query).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                //onSuccess(response.body()!!)
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                t.printStackTrace()
                onError()
            }
        })
    }

    private fun isStatusNull(jsonObject: JsonObject) =
        jsonObject["status"].asString == "ZERO_RESULTS" ||
        jsonObject["status"].asString == "NOT_FOUND"
}