package com.main.application.service

import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject

/**
 * This class refers to Directions API Google's service.
 */
interface DirectionsService {

    fun getDirections(options: Map<String, String>, onSuccess: (MutableList<List<LatLng>>) -> Unit, onError: (Int) -> Unit)
    fun getCurrentLocation(options: Map<String, String>, onSuccess: (JsonObject) -> Unit, onError: () -> Unit)
    fun osmGetLocationDetails(query: String, onSuccess: (String) -> Unit, onError: () -> Unit)
}