package com.main.application.constants

object Roles {

    const val ROLE_PATIENT = "PATIENT"
    const val ROLE_CAREGIVER = "CAREGIVER"
}