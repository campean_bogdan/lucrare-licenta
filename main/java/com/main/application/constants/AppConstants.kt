package com.main.application.constants

object AppConstants {

    const val DEVIATION_DISTANCE = 20.0
    const val NUMBER_OF_GPS_READINGS = 50
    const val ANGLE_CURVE = 155.0
    const val LIGHT_TURNING_POINT = 120.0
    const val LIMIT_DISTANCE_FROM_OBJECT = 10
    const val CIRCLE_DISTANCE = 13
    const val ROAD_DISTANCE = 0.00015       // distance between a road segment and perpendicular points on it; it is used to surround the path
    const val SHORT_DISTANCE = 13       // announcing the distance from 10 to 10 meters
    const val LONG_DISTANCE = 10

    val ANNOUNCE_DISTANCES = listOf(25, 15, 10, 5, 0)      // it must be in descending order

    const val EMAIL = "email"
    const val NAME = "name"
    const val PATIENT_KEY = "patient_key"
    const val DEFINED_ROUTE_KEY = "defined_route_key"

    const val TIMER_VALUE_LOW: Long = 10000
    const val TIMER_VALUE_MEDIUM: Long = 15000

    enum class Type {
        ROAD_EVENT, POINT
    }

    enum class RoadEventsCategory {
        SIMPLE_CROSSING,
        TRAFFIC_LIGHT_CROSSING,
        TRAFFIC_LIGHT_GROUP_CROSSING
    }
}