package com.main.application.constants

object NotificationType {

    const val INVITATION = "INVITATION"
    const val HELP = "HELP"
}