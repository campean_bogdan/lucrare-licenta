package com.main.application.constants

object InvitationResponse {

    const val PENDING = "PENDING"
    const val ACCEPT = "ACCEPT"
    const val DECLINE = "DECLINE"
    const val NOT_CONNECTED = "NOT CONNECTED"
}