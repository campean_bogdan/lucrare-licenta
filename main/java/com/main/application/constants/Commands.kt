package com.main.application.constants

object Commands {
    const val SET_NEW_POINT_OF_INTEREST_RO = "setează"
    const val DELETE_POINT_OF_INTEREST_RO = "șterge"
    const val GET_TO_POINT_OF_INTEREST_RO = "ajung la"     // request a route to user's interest point; dynamic path
    const val GET_TO_DESTINATION_RO = "ajung pe"            // request a route to an address; dynamic path
    const val GET_ROUTE_RO = "traseu"                       // request a predefined path
    const val GET_CURRENT_LOCATION_INFORMATION_RO = "unde sunt"         // finding the current address
    const val NEED_HELP_RO = "ajutor"                       // send notification
    const val LOGOUT = "logout"

    const val LEFT_EN = "Left"
    const val RIGHT_EN = "Right"
    const val FORWARD_EN = "Forward"
    const val BACK_EN = "Back"

    const val LEFT_RO = "Stânga"
    const val RIGHT_RO = "Dreapta"
    const val SOURCE_RO = "sursă"
    const val DESTINATION_RO = "destinație"
    const val BACK_RO = "înapoi"
}