package com.main.application

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import com.main.application.constants.Roles
import com.main.application.di.applicationModule
import com.main.application.model.auth.AuthResponse
import com.main.application.model.auth.RefreshRequest
import com.main.application.model.auth.RefreshResponse
import com.main.application.model.firebase.User
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.service.firebase_services.FirebaseAuthentication
import com.main.application.service.firebase_services.FirebaseAuthenticationImpl
import com.main.application.utils.LocalStorageUtils
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin

class MainActivity : AppCompatActivity(), KoinComponent {

    private lateinit var firebaseAuthentication: FirebaseAuthentication
    private lateinit var firebaseDatabase: FirebaseDatabase
    private val REQUEST_CODE_ACCESS_FINE_LOCATION = 1
    private val REQUEST_CODE_ACCESS_INTERNET = 2
    private val REQUEST_CODE_ACCESS_RECORD_AUDIO = 3
    private val REFRESH_TOKEN_CODE = 401
    private val INVALID_REFRESH_TOKEN_CODE = 400
    private val USER_NOT_FOUND = 1

    // values that are stored in local storage
    private var token = ""
    private var refreshToken = ""
    private var localId = ""
    private var email = ""
    private lateinit var progressBar: View
    lateinit var internetConnectionBanner: View

    private lateinit var tm: TelephonyManager

    // TODO: findNavController(R.id.navHostId) rezolva eroarea cu "ID does not reference a View inside this Activity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // set toolbar on this activity
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    override fun onStop() {
        super.onStop()
        stopKoin()
    }

    override fun onStart() {
        super.onStart()
        startKoin {
            androidLogger()
            androidContext(applicationContext)
            modules(applicationModule)
        }

        internetConnectionBanner = findViewById(R.id.appBanner)
        progressBar = findViewById(R.id.progressBarId)
        internetConnectionBanner.visibility = View.GONE
        init()
        if (checkInternetConnection())
            tryToAutoConnect()
        else {
            internetConnectionBanner.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    fun setToolbarTitle(text: String) {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = text
    }

    fun init() {
        token = LocalStorageUtils.getUserAndToken(this).second.idToken!!
        refreshToken = LocalStorageUtils.getUserAndToken(this).second.refreshToken!!
        email = LocalStorageUtils.getUserAndToken(this).first
        localId = LocalStorageUtils.getUserAndToken(this).second.uuid!!
    }

    fun showBanner(text: String, backgroundColor: Int, visibility: Int) {
        internetConnectionBanner = findViewById(R.id.appBanner)
        (internetConnectionBanner as TextView).text = text
        internetConnectionBanner.setBackgroundColor(backgroundColor)
        internetConnectionBanner.visibility = visibility
    }

    fun bannerGone() {
        internetConnectionBanner = findViewById(R.id.appBanner)
        internetConnectionBanner.visibility = View.GONE
    }

    private fun tryToAutoConnect() {
        progressBar.visibility = View.VISIBLE
        firebaseDatabase.getUserByEmail(token, email, ::onSuccessGetUser, ::onError)
    }

    private fun onSuccessGetUser(user: User) {
        Log.i("Success", "MainActivity. Get user with success!")
        when (user.role) {
            Roles.ROLE_PATIENT -> {
                progressBar.visibility = View.GONE
                findNavController(R.id.navHostId).navigate(R.id.mapFragment)
            }
            Roles.ROLE_CAREGIVER -> {
                progressBar.visibility = View.GONE
                findNavController(R.id.navHostId).navigate(R.id.caregiverFragment)
            }
        }
    }

    private fun onError(error: Int) {
        Log.e("Error", "MainActivity. Failed to get user. Error code: $error")
        when (error) {
            // token is expired
            REFRESH_TOKEN_CODE -> {
                val refreshRequest = RefreshRequest(refreshToken)
                firebaseAuthentication.refreshToken(refreshRequest, ::onSuccessRefreshToken, ::onErrorRefreshToken)
            }
            USER_NOT_FOUND -> {
                progressBar.visibility = View.GONE
            }
            // refresh token was deleted from local storage
            INVALID_REFRESH_TOKEN_CODE -> {
                progressBar.visibility = View.GONE
                findNavController(R.id.navHostId).navigate(R.id.loginFragment)
            }
        }
    }

    private fun onSuccessRefreshToken(refreshResponse: RefreshResponse) {
        Log.i("Success", "MainActivity. On success refresh token!")
        val authResponse = AuthResponse(localId, refreshResponse.idToken, refreshResponse.refreshToken)
        LocalStorageUtils.saveUserAndToken(email, authResponse, this)
        progressBar.visibility = View.GONE
        firebaseDatabase.getUserByEmail(refreshResponse.idToken, email, ::onSuccessGetUser, ::onError2)
    }

    private fun onError2(error: Int) {
        Log.e("Error", "MainActivity. Get user with error (second). Error code: $error")
        progressBar.visibility = View.GONE
    }

    private fun onErrorRefreshToken(error: Int) {
        Log.e("Error", "MainActivity. On error refresh token. Error code: $error")
        progressBar.visibility = View.GONE
        findNavController(R.id.navHostId).navigate(R.id.loginFragment)
    }
}
