package com.main.application.menu.auth.forgot_password

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.main.application.model.auth.ResetPasswordRequest
import com.main.application.service.firebase_services.FirebaseAuthentication
import com.main.application.service.firebase_services.FirebaseAuthenticationImpl
import com.main.application.utils.LocalStorageUtils

class ChangePasswordViewModel(application: Application): AndroidViewModel(application) {

    val email = MutableLiveData<String>()
    val onLoadProgressBar = MutableLiveData<Boolean>()
    val error = MutableLiveData<Boolean>()
    val success = MutableLiveData<Boolean>()
    val errorText = MutableLiveData<String>()
    val successText = MutableLiveData<String>()
    private val token = LocalStorageUtils.getUserAndToken(application).second.idToken!!
    private val firebaseService: FirebaseAuthentication = FirebaseAuthenticationImpl(application)

    init {
        error.value = false
        success.value = false
        onLoadProgressBar.value = false
    }

    fun changePassword() {
        success.value = false
        error.value = false
        onLoadProgressBar.value = true
        val resetPassword = ResetPasswordRequest(email.value!!)
        firebaseService.changePassword(resetPassword, ::onSuccessChangePassword, ::onErrorChangePassword)
    }

    private fun onSuccessChangePassword() {
        Log.i("Success", "ChangePasswordVM. Password changed!")
        onLoadProgressBar.value = false
        successText.value = "You will receive shortly an email to change the password."
        success.value = true
    }

    private fun onErrorChangePassword(error: Int) {
        Log.e("Error", "ChangePasswordVM. Error changing password. Error code: $error")
        onLoadProgressBar.value = false
        when (error) {
            1 -> {
                errorText.value = "Email does not exist!"
                this.error.value = true
            }
        }
    }
}