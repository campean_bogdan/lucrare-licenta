package com.main.application.menu.auth.login

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.main.application.constants.Roles
import com.main.application.menu.auth.SharedViewModel
import com.main.application.model.auth.AuthRequest
import com.main.application.model.auth.AuthResponse
import com.main.application.model.firebase.User
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.service.firebase_services.FirebaseAuthentication
import com.main.application.service.firebase_services.FirebaseAuthenticationImpl
import com.main.application.utils.LocalStorageUtils

class LoginViewModel(application: Application): AndroidViewModel(application) {

    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val emailError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    val globalError = MutableLiveData<String>()
    val onLoadProgressBar = MutableLiveData<Boolean>()
    val onNavigateToHomeMenu = MutableLiveData<Boolean>()
    val onNavigateToCaregiverMenu = MutableLiveData<Boolean>()
    val internetConnection = MutableLiveData<Boolean>()
    val checkInternetConnection = MutableLiveData<Boolean>()

    //private val firebaseService: FirebaseService by inject()
    private val firebaseAuthentication: FirebaseAuthentication = FirebaseAuthenticationImpl(application.applicationContext)
    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()

    init {
        onLoadProgressBar.value = false
        onNavigateToHomeMenu.value = false
        onNavigateToCaregiverMenu.value = false
        email.value = ""
        password.value = ""
        internetConnection.value = false
        checkInternetConnection.value = false
    }



    fun checkFields(): Boolean {
        globalError.value = null
        checkInternetConnection.value = true
        if (password.value!!.length < 6) {
            passwordError.value = "Password too short!"
            return false
        } else {
            passwordError.value = null
        }
        onLoadProgressBar.value = true
        if (internetConnection.value!!)
            firebaseAuthentication.loginUserService(AuthRequest(email.value!!, password.value!!), ::onSuccessLogin, ::onErrorGetUser)
        else
            onLoadProgressBar.value = false
        return true
    }

    fun accountCreated() {
//        banner.value = "Account created with success!\nYou can login now."
//        bannerVisibility.value = true
        SharedViewModel.accountCreated = false
    }

    private fun onSuccessLogin(response: AuthResponse) {
        Log.i("SUCCESS", "User logged in successfully - firebase service.")
        LocalStorageUtils.saveUserAndToken(email.value!!, response, getApplication())
        firebaseDatabase.getUserByEmail(response.idToken!!, email.value!!, ::onSuccessGetUser, ::onErrorGetUser)
    }

    private fun onSuccessGetUser(user: User) {
        Log.i("Success", "Get user with success!")
        onLoadProgressBar.value = false
        onNavigateToCaregiverMenu.value = user.role == Roles.ROLE_CAREGIVER
        onNavigateToHomeMenu.value = user.role == Roles.ROLE_PATIENT
    }

    private fun onErrorGetUser(code: Int) {
        Log.e("ERROR", "Failed to get user. Error code: $code")
        globalError.value = "User does not exist!"
        onLoadProgressBar.value = false

//        banner.value = "Salut"
//        bannerVisibility.value = true
//        errorColor.value = true
    }
}