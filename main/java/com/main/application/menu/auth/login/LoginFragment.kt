package com.main.application.menu.auth.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.main.application.MainActivity
import com.main.application.PrivacyPolicyActivity
import com.main.application.R
import com.main.application.databinding.FragmentLoginBinding
import com.main.application.menu.auth.SharedViewModel

class LoginFragment: Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[LoginViewModel(activity!!.application)::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentLoginBinding>(inflater, R.layout.fragment_login, container, false)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val title = context!!.resources.getString(R.string.login)
        (activity!! as MainActivity).setToolbarTitle(title)
        val icc = (activity!! as MainActivity).checkInternetConnection()
        if (icc)
            (activity!! as MainActivity).bannerGone()

        // observer
        viewModel.onNavigateToHomeMenu.observe(this, Observer { shouldNavigateToMenu ->
            if (shouldNavigateToMenu == true) {
                findNavController().navigate(R.id.mapFragment)
            }
        })

        viewModel.onNavigateToCaregiverMenu.observe(this, Observer { shouldNavigateToCaregiverMenu ->
            if (shouldNavigateToCaregiverMenu) {
                findNavController().navigate(R.id.caregiverFragment)
            }
        })

        // on login press, check if there is internet connection; if true => banner gone, otherwise show internet connection error banner
        viewModel.checkInternetConnection.observe(this, Observer { shouldCheckInternetConnection ->
            if (shouldCheckInternetConnection) {
                val ic = (activity!! as MainActivity).checkInternetConnection()
                viewModel.internetConnection.value = ic
                val color = resources.getColor(R.color.errorColor)
                if (!ic) {
                    (activity!! as MainActivity).showBanner("There is no Internet connection!", color, View.VISIBLE)
                } else {
                    (activity!! as MainActivity).bannerGone()
                }
            }
        })

        binding.privacyPolicyText.setOnClickListener {
            val intent = Intent(context, PrivacyPolicyActivity::class.java)
            startActivity(intent)
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        if (SharedViewModel.accountCreated) {
            viewModel.accountCreated()
            val color = this.resources.getColor(R.color.successColor)
            (activity!! as MainActivity).showBanner("Account created with success!\nYou can login with new account.",  color, View.VISIBLE)
            SharedViewModel.accountCreated = false
        }
    }
}