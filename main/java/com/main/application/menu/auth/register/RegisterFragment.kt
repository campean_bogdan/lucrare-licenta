package com.main.application.menu.auth.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.databinding.FragmentRegisterBinding

class RegisterFragment: Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[RegisterViewModel(activity!!.application)::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentRegisterBinding>(
            inflater,
            R.layout.fragment_register,
            container,
            false
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val title = context!!.resources.getString(R.string.create_new_account)
        (activity!! as MainActivity).setToolbarTitle(title)

        binding.spinnerRole.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.role = parent!!.getItemAtPosition(position)!! as String
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        viewModel.onNavigateToLogin.observe(this, Observer { shouldNavigateToPatient ->
            if (shouldNavigateToPatient) {
                findNavController().navigate(R.id.loginFragment)
            }
        })

        // on login press, check if there is internet connection; if true => banner gone, otherwise show internet connection error banner
        viewModel.checkInternetConnection.observe(this, Observer { shouldCheckInternetConnection ->
            if (shouldCheckInternetConnection) {
                val ic = (activity!! as MainActivity).checkInternetConnection()
                viewModel.internetConnection.value = ic
                val color = resources.getColor(R.color.errorColor)
                if (!ic) {
                    (activity!! as MainActivity).showBanner("There is no Internet connection!", color, View.VISIBLE)
                } else {
                    (activity!! as MainActivity).bannerGone()
                }
            }
        })

        return binding.root
    }
}