package com.main.application.menu.auth.register

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.main.application.menu.auth.SharedViewModel
import com.main.application.model.auth.AuthRequest
import com.main.application.model.auth.AuthResponse
import com.main.application.model.firebase.User
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.service.firebase_services.FirebaseAuthentication
import com.main.application.service.firebase_services.FirebaseAuthenticationImpl
import com.main.application.utils.LocalStorageUtils

class RegisterViewModel(application: Application): AndroidViewModel(application) {

    val name = ObservableField<String>()
    val email = ObservableField<String>()
    val password = ObservableField<String>()
    val confirmPassword = ObservableField<String>()
    val city = ObservableField<String>()

    val nameError = MutableLiveData<String>()
    val emailError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    val confirmPasswordError = MutableLiveData<String>()
    val globalError = MutableLiveData<String>()

    val enabledRegisterButton = MutableLiveData<Boolean>()
    val onProgressbarLoad = MutableLiveData<Boolean>()
    val onNavigateToLogin = MutableLiveData<Boolean>()
    val internetConnection = MutableLiveData<Boolean>()
    val checkInternetConnection = MutableLiveData<Boolean>()

    var role = ""

    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()
    private val firebaseAuthentication: FirebaseAuthentication = FirebaseAuthenticationImpl(application.applicationContext)

    init {
        enabledRegisterButton.value = true
        onProgressbarLoad.value = false
        name.set("")
        email.set("")
        password.set("")
        confirmPassword.set("")
        city.set("")
        onNavigateToLogin.value = false
        internetConnection.value = false
        checkInternetConnection.value = false
        SharedViewModel.accountCreated = false
    }


    fun confirmFields(): Boolean {
        checkInternetConnection.value = true
        if (password.get()!!.length < 6) {
            passwordError.value = "Password must be minimum 6 characters!"
            return false
        } else {
            passwordError.value = null
        }
        if (!password.get().equals(confirmPassword.get())) {
            confirmPasswordError.value = "Password doesn't match!"
            return false
        } else {
            confirmPasswordError.value = null
        }

        // register new user in firebase servcie
        if (internetConnection.value!!) {
            enabledRegisterButton.value = false
            onProgressbarLoad.value = true
            firebaseAuthentication.registerUserService(AuthRequest(email.get()!!, password.get()!!), ::onSuccessRegister, ::onError)
        } else {
            onProgressbarLoad.value = false
            enabledRegisterButton.value = true
        }
        return true
    }

    private fun onError(code: Int) {
        Log.e("ERROR", "Failed to register user. Error code: $code")
        if (code == 400) {
            globalError.value = "Email already exists!"
        }
        enabledRegisterButton.value = true
        onProgressbarLoad.value = false
    }

    private fun onSuccessRegister() {
        Log.i("SUCCESS", "User registered with success - firebase database.")
        globalError.value = null
        enabledRegisterButton.value = true
        onProgressbarLoad.value = false
        onNavigateToLogin.value = true
        SharedViewModel.accountCreated = true
    }

    private fun onSuccessRegister(response: AuthResponse) {
        Log.i("SUCCESS", "User registered with success - firebase service.")
        // insert new user in my firebase database only if register on firebase was successful
        val deviceToken = LocalStorageUtils.getDeviceRegistrationToken(getApplication())
        firebaseDatabase.registerUserInDatabase(
            response.uuid!!,
            User(
                name.get()!!,
                email.get()!!,
                password.get()!!,
                city.get()!!,
                deviceToken,
                role.toUpperCase()
            ), ::onSuccessRegister, ::onError)
    }
}