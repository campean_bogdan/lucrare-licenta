package com.main.application.menu.caregiver.routes.defined_routes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.constants.AppConstants
import com.main.application.databinding.FragmentViewDefinedRoutesBinding
import com.main.application.menu.caregiver.routes.defined_routes.recycler_view.DefinedRoutesAdapter

class DefinedRoutesFragment: Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[DefinedRoutesViewModel::class.java]
    }
//    private val viewModel by lazy {
//        ViewModelProviders.of(this)[CaregiverPatientsViewModel::class.java]
//    }
    private lateinit var adapter: DefinedRoutesAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentViewDefinedRoutesBinding>(
            inflater,
            R.layout.fragment_view_defined_routes,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.patientEmail.value = arguments!![AppConstants.EMAIL] as String
        viewModel.patientName.value = "Patient name: ${arguments!![AppConstants.NAME] as String}"

        val title = context!!.resources.getString(R.string.defined_routes)
        (activity!! as MainActivity).setToolbarTitle(title)
        setRecyclerView(binding.patientDefinedRoutesRecyclerView)

        viewModel.existData.observe(this, Observer { shouldNotifyAdapter ->
            if (shouldNotifyAdapter) {
                adapter.setDefinedRoutes(viewModel.patientDefinedRoutes)
            }
        })

        DefinedRoutesViewModel.readyToDelete.observe(this, Observer { shouldDeleteDefinedRoute ->
            if (shouldDeleteDefinedRoute) {
                viewModel.deleteDefinedRoute(DefinedRoutesViewModel.whichToDeleteText.value!!)
                adapter.updateList(DefinedRoutesViewModel.whichToDeleteText.value!!)
                DefinedRoutesViewModel.readyToDelete.value = false
            }
        })

        DefinedRoutesViewModel.readyToNavigate.observe(this, Observer { shouldNavigateToDetails ->
            if (shouldNavigateToDetails) {
                val bundle = bundleOf(
                    AppConstants.PATIENT_KEY to viewModel.patientLocalId,
                    AppConstants.DEFINED_ROUTE_KEY to DefinedRoutesViewModel.whichToShowDetails.value
                )
                findNavController().navigate(R.id.action_definedRoutesFragment_to_definedRoutesDetailsFragment, bundle)
                DefinedRoutesViewModel.readyToNavigate.value = false
            }
        })

        viewModel.getDefinedRoutes()

        return binding.root
    }

    private fun setRecyclerView(recyclerView: RecyclerView) {
        adapter =
            DefinedRoutesAdapter(
                context!!,
                mutableListOf()
            )
        recyclerView.layoutManager = LinearLayoutManager(context!!)
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }
}