package com.main.application.menu.caregiver.routes.define_route

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.databinding.FragmentCaregiverDefineRouteBinding

class DefineRouteFragment: Fragment(), OnMapReadyCallback {

    val viewModel by lazy {
        ViewModelProviders.of(this)[DefineRouteViewModel::class.java]
    }

    private var mapFragment = SupportMapFragment.newInstance()
    private lateinit var map: GoogleMap
    private var marker: Marker? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel.reset()

        val binding = DataBindingUtil.inflate<FragmentCaregiverDefineRouteBinding>(
            inflater,
            R.layout.fragment_caregiver_define_route,
            container,
            false
        )

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        mapFragment = childFragmentManager.findFragmentById(R.id.map_define_route) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val title = context!!.resources.getString(R.string.define_route)
        (activity!! as MainActivity).setToolbarTitle(title)

        return binding.root
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map!!

        val locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val locationListener = object: LocationListener {
            override fun onLocationChanged(location: Location?) {
                setCurrentLocation(location!!)
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onProviderEnabled(provider: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onProviderDisabled(provider: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        if (ContextCompat.checkSelfPermission(context!!, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                500,
                2.0f,
                locationListener
            )
        }

        mapListeners()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_next_icon_id) {
            findNavController().navigate(R.id.action_defineRouteFragment_to_defineRouteNameFragment)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_next_item, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setCurrentLocation(location: Location) {
        if (marker != null)
            marker!!.remove()
        marker = map.addMarker(MarkerOptions()
            .title("Your Location")
            .position(LatLng(location.latitude, location.longitude))
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
        )
        if (!viewModel.firstGpsReading.value!!) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 18.0f))
            viewModel.firstGpsReading.value = true
        }
    }

    override fun onDestroyView() {
        SharedDefinedViewModel.clearAndSetList(
            viewModel.markers
        )
        super.onDestroyView()
    }

    private fun mapListeners() {
        map.setOnMapClickListener {location ->
            viewModel.addNewPoint(location, ::addRouteMarker, ::drawLine)
        }
    }

    private fun addRouteMarker(location: LatLng, title: String) =
        map.addMarker(MarkerOptions()
            .title(title)
            .position(location)
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
        )

    private fun drawLine(firstPoint: LatLng, lastPoint: LatLng) =
        map.addPolyline(PolylineOptions().addAll(listOf(firstPoint, lastPoint)).color(Color.RED))
}