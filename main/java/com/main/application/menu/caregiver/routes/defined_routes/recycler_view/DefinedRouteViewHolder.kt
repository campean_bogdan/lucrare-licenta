package com.main.application.menu.caregiver.routes.defined_routes.recycler_view

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.model.LatLng
import com.main.application.R
import com.main.application.menu.caregiver.routes.defined_routes.DefinedRoutesViewModel
import com.main.application.model.firebase.DefinedRoute
import com.main.application.utils.RequestUtils
import java.text.SimpleDateFormat

class DefinedRouteViewHolder(
    itemView: View
): RecyclerView.ViewHolder(itemView) {

    private lateinit var context: Context
    private lateinit var cardView: CardView
    private lateinit var name: TextView
    private lateinit var date: TextView
    private lateinit var numberOfPoints: TextView
    private lateinit var routeLength: TextView
    private lateinit var deleteButton: Button
    private lateinit var mapImage: ImageView


    fun bindViewHolder(context: Context, definedRoute: DefinedRoute) {
        this.context = context
        cardView = itemView.findViewById(R.id.card_view_defined_route)
        name = itemView.findViewById(R.id.defined_route_name)
        date = itemView.findViewById(R.id.defined_route_date)
        numberOfPoints = itemView.findViewById(R.id.defined_route_number_of_points)
        routeLength = itemView.findViewById(R.id.defined_route_length)
        deleteButton = itemView.findViewById(R.id.defined_route_delete_btn)
        mapImage = itemView.findViewById(R.id.imageView3)

        this.insertRouteSnapshot(definedRoute.points)

        name.text = definedRoute.name
        date.text = SimpleDateFormat("dd/MM/yyyy").format(definedRoute.date)
        if (!definedRoute.points.isNullOrEmpty())
            numberOfPoints.text = definedRoute.points!!.size.toString()
        else
            numberOfPoints.text = "0"
        val str = "${definedRoute.totalLength} m"
        routeLength.text = str

        // when user wants to delete a defined route from a patient
        deleteButton.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("Delete Defined Route")
                .setMessage("Are you sure you want to delete this defined route?")
                .setPositiveButton(R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                    DefinedRoutesViewModel.whichToDeleteText.value = definedRoute.key
                    DefinedRoutesViewModel.readyToDelete.value = true
                })
                .setNegativeButton(R.string.no, null)
                .show()
        }

        // navigate to details
        cardView.setOnClickListener {
            DefinedRoutesViewModel.whichToShowDetails.value = definedRoute.key
            DefinedRoutesViewModel.readyToNavigate.value = true
        }
    }

    private fun insertRouteSnapshot(points: List<LatLng>?) {
        if (points == null) {
            Glide.with(context)
                .load(R.drawable.image_not_found_5)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(20)))
                .into(mapImage)
        } else {
            val url = RequestUtils.getStaticImage(points)
            Glide.with(context)
                .load(url)
                .placeholder(R.drawable.image_not_found)
                .error(R.drawable.image_not_found_5)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(20)))
                .into(mapImage)
        }
    }
}