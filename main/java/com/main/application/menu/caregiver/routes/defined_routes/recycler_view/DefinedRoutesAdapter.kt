package com.main.application.menu.caregiver.routes.defined_routes.recycler_view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.main.application.R
import com.main.application.model.firebase.DefinedRoute

class DefinedRoutesAdapter(
    private val context: Context,
    private var definedRoutes: MutableList<DefinedRoute>
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun setDefinedRoutes(definedRoutes: MutableList<DefinedRoute>) {
        this.definedRoutes = definedRoutes
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return DefinedRouteViewHolder(
            inflater.inflate(R.layout.defined_route_view_holder, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return definedRoutes.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as DefinedRouteViewHolder).bindViewHolder(context, definedRoutes[position])
    }

    fun updateList(key: String) {
        var index = -1
        for (i in definedRoutes.indices) {
            if (definedRoutes[i].key == key)
                index = i
        }
        definedRoutes.removeAt(index)
        notifyItemRemoved(index)
    }
}