package com.main.application.menu.caregiver.routes.define_route

import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

class SharedDefinedViewModel: ViewModel() {

    companion object {
        var definedRoutePoints = mutableListOf<LatLng>()

        fun clearAndSetList(list: List<Marker>) {
            for (i in 0 until definedRoutePoints.size)
                definedRoutePoints.removeAt(0)
            for (i in list.indices)
                definedRoutePoints.add(list[i].position)
        }
    }
}