package com.main.application.menu.caregiver.routes.defined_routes

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.constants.AppConstants
import com.main.application.databinding.FragmentDefinedRouteDetailsBinding

class DefinedRoutesDetailsFragment: Fragment(), OnMapReadyCallback {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[DefinedRoutesDetailsViewModel::class.java]
    }
    private var mapFragment = SupportMapFragment.newInstance()
    private lateinit var map: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentDefinedRouteDetailsBinding>(
            inflater,
            R.layout.fragment_defined_route_details,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val title = context!!.resources.getString(R.string.defined_route_details)
        (activity!! as MainActivity).setToolbarTitle(title)

        mapFragment = childFragmentManager.findFragmentById(R.id.defined_route_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        viewModel.definedRouteKey.value = arguments!![AppConstants.DEFINED_ROUTE_KEY] as String
        viewModel.patientLocalId.value = arguments!![AppConstants.PATIENT_KEY] as String

        viewModel.existsPoints.observe(this, Observer { shouldDrawPath ->
            if (shouldDrawPath) {
                this.drawPath(viewModel.definedRoutePoints)
            }
        })

        return binding.root
    }

    override fun onMapReady(p0: GoogleMap?) {
        this.map = p0!!
        viewModel.getDefinedRouteDetails(viewModel.patientLocalId.value!!, viewModel.definedRouteKey.value!!)
    }

    private fun drawPath(points: List<LatLng>) {
        map.addPolyline(PolylineOptions().addAll(points).color(Color.BLUE))
        var averageLat = 0.0
        var averageLon = 0.0
        for (i in points.indices) {
            averageLat += points[i].latitude
            averageLon += points[i].longitude
            map.addMarker(MarkerOptions().position(points[i]).title("$i").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)))
            map.addPolyline(PolylineOptions())
        }
        averageLat /= points.size
        averageLon /= points.size
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(averageLat, averageLon), 15F))
        map.addMarker(MarkerOptions().position(LatLng(averageLat, averageLon)).title("Average route point").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)))
    }
}