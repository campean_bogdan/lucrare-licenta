package com.main.application.menu.caregiver.routes.defined_routes

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.main.application.model.firebase.DefinedRoute
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils

class DefinedRoutesDetailsViewModel(application: Application): AndroidViewModel(application) {

    val patientLocalId = MutableLiveData<String>()
    val definedRouteKey = MutableLiveData<String>()
    val onLoadProgressBar = MutableLiveData<Boolean>()
    val existsPoints = MutableLiveData<Boolean>()
    var definedRoutePoints = mutableListOf<LatLng>()
    val definedRouteName = MutableLiveData<String>()
    val definedRouteNumberOfPoints = MutableLiveData<String>()
    val definedRouteLength = MutableLiveData<String>()
    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()
    private val token = LocalStorageUtils.getUserAndToken(application).second.idToken!!


    fun getDefinedRouteDetails(patientLocalId: String, definedRotueKey: String) {
        onLoadProgressBar.value = true
        existsPoints.value = false
        firebaseDatabase.getDefinedRouteDetails(token, patientLocalId, definedRotueKey, ::onSuccessGetDetails, ::onErrorGetDetails)
    }

    private fun onSuccessGetDetails(definedRoute: DefinedRoute) {
        Log.i("Success", "DefinedRouteDetailsViewModel. Get defined route details with success!")
        onLoadProgressBar.value = false
        definedRouteName.value = definedRoute.name
        definedRouteLength.value = definedRoute.totalLength.toString() + "m"
        if (!definedRoute.points.isNullOrEmpty()) {
            definedRoutePoints = definedRoute.points!!.toMutableList()
            definedRouteNumberOfPoints.value = definedRoute.points!!.size.toString()
            existsPoints.value = true
        } else {
            definedRouteNumberOfPoints.value = "0"
        }
    }

    private fun onErrorGetDetails(error: Int) {
        Log.e("Error", "DefinedRouteDetailsViewModel. Get defined route details with error. Error code: $error")
        onLoadProgressBar.value = false
    }
}