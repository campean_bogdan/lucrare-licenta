package com.main.application.menu.caregiver.routes.define_route

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.databinding.FragmentDefineRouteNameBinding

class DefineRouteDataFragment: Fragment() {

    val viewModel by lazy {
        ViewModelProviders.of(this)[DefineRouteDataViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentDefineRouteNameBinding>(
            inflater,
            R.layout.fragment_define_route_name,
            container,
            false
        )

        val icc = (activity!! as MainActivity).checkInternetConnection()
        if (icc)
            (activity!! as MainActivity).bannerGone()

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val title = context!!.resources.getString(R.string.define_route_data)
        (activity!! as MainActivity).setToolbarTitle(title)

        viewModel.reset()
        viewModel.definedPoints =
            SharedDefinedViewModel.definedRoutePoints

        // on login press, check if there is internet connection; if true => banner gone, otherwise show internet connection error banner
        viewModel.checkInternetConnection.observe(this, Observer { shouldCheckInternetConnection ->
            if (shouldCheckInternetConnection) {
                val ic = (activity!! as MainActivity).checkInternetConnection()
                viewModel.internetConnection.value = ic
                val color = resources.getColor(R.color.errorColor)
                if (!ic) {
                    (activity!! as MainActivity).showBanner("There is no Internet connection!", color, View.VISIBLE)
                } else {
                    (activity!! as MainActivity).bannerGone()
                }
            }
        })

        return binding.root
    }
}