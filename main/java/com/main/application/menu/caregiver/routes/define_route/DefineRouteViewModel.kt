package com.main.application.menu.caregiver.routes.define_route

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.Polyline

class DefineRouteViewModel: ViewModel() {

    val polyline = mutableListOf<Polyline>()
    val markers = mutableListOf<Marker>()
    val firstGpsReading = MutableLiveData<Boolean>()

    init {
        firstGpsReading.value = false
    }

    fun reset() {
        for (i in 0 until polyline.size)
            polyline[i].remove()
        for (i in 0 until polyline.size)
            polyline.removeAt(0)
        for (i in 0 until markers.size)
            markers[i].remove()
        for (i in 0 until markers.size)
            markers.removeAt(0)
    }

    fun addNewPoint(newPoint: LatLng, drawPoint: (LatLng, String) -> Marker, drawLine: (LatLng, LatLng) -> Polyline) {
        val newMarker = drawPoint(newPoint, (markers.size).toString())
        markers.add(newMarker)
        if (markers.size > 1) {
            val lastPoint = markers[markers.size - 2].position
            val newLine = drawLine(lastPoint, newPoint)
            polyline.add(newLine)
        }
    }

    fun deleteLastPoint() {
        if (markers.size > 0) {
            val markersSize = markers.size - 1
            markers[markersSize].remove()
            markers.removeAt(markersSize)
            if (polyline.size > 0) {
                val lineSize = polyline.size - 1
                polyline[lineSize].remove()
                polyline.removeAt(lineSize)
            }
        }
    }
}