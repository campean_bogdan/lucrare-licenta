package com.main.application.menu.caregiver.routes.defined_routes

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.main.application.model.firebase.DefinedRoute
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils

class DefinedRoutesViewModel(application: Application): AndroidViewModel(application) {

    val patientEmail = MutableLiveData<String>()
    val patientName = MutableLiveData<String>()
    var patientDefinedRoutes = mutableListOf<DefinedRoute>()
    val existData = MutableLiveData<Boolean>()
    val onSwipeRefresh = MutableLiveData<Boolean>()
    var patientLocalId: String = ""

    private var token: String = LocalStorageUtils.getUserAndToken(getApplication()).second.idToken!!
    private var localId: String = LocalStorageUtils.getUserAndToken(getApplication()).second.uuid!!
    private var refreshToken: String = LocalStorageUtils.getUserAndToken(getApplication()).second.refreshToken!!
    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()

    companion object {
        val readyToDelete = MutableLiveData<Boolean>()
        val whichToDeleteText = MutableLiveData<String>()
        val readyToNavigate = MutableLiveData<Boolean>()
        val whichToShowDetails = MutableLiveData<String>()
    }

    fun deleteDefinedRoute(key: String) {
        onSwipeRefresh.value = true
        firebaseDatabase.deleteDefinedRoute(token, patientLocalId, key, ::onSuccessDeleteDefinedRoute, ::onErrorDeleteDefinedRoute)
    }

    fun getDefinedRoutes() {
        existData.value = false
        onSwipeRefresh.value = true
        readyToDelete.value = false
        firebaseDatabase.getUserLocalId(token, patientEmail.value!!, ::onSuccessGetUserData, ::onErrorGetUserData)
    }

    private fun onSuccessGetUserData(localId: String) {
        Log.i("Success", "DefinedRoutesViewModel. Get user with success!")
        patientLocalId = localId
        firebaseDatabase.getPatientDefinedRoutes(token, localId, ::onSuccessGetDefinedRoutes, ::onErrorGetDefinedRoutes)
    }

    private fun onErrorGetUserData(error: Int) {
        Log.e("Error", "DefinedRoutesViewModel. Get user with error. Error code: $error!")
        onSwipeRefresh.value = false
    }

    private fun onSuccessGetDefinedRoutes(definedRoutes: List<DefinedRoute>) {
        Log.i("Success", "DefinedRoutesViewModel. Get defined routes with success!")
        patientDefinedRoutes = definedRoutes as MutableList<DefinedRoute>
        existData.value = true
        onSwipeRefresh.value = false
    }

    private fun onErrorGetDefinedRoutes(error: Int) {
        Log.e("Error", "DefinedRoutesViewModel. Get defined routes with error. Error code: $error!")
        onSwipeRefresh.value = false
    }

    private fun onSuccessDeleteDefinedRoute() {
        Log.i("Success", "DefinedRoutesViewModel. Delete defined route with success!")
        onSwipeRefresh.value = false
    }

    private fun onErrorDeleteDefinedRoute(error: Int) {
        Log.e("Error", "DefinedRoutesViewModel. Delete defined route with error. Error code: $error!")
        onSwipeRefresh.value = false
    }

}