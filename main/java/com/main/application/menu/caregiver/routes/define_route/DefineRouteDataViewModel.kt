package com.main.application.menu.caregiver.routes.define_route

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.main.application.model.firebase.DefinedRoute
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils
import com.main.application.utils.TransformationsUtils

class DefineRouteDataViewModel(application: Application): AndroidViewModel(application) {

    val patientEmail = ObservableField<String>()
    val routeName = ObservableField<String>()
    val routeNameError = MutableLiveData<String>()
    val emailError = MutableLiveData<String>()
    var definedPoints = mutableListOf<LatLng>()
    val success = MutableLiveData<Boolean>()
    val successText = MutableLiveData<String>()
    val onLoadProgressBar = MutableLiveData<Boolean>()
    private val firebaseDatabaseService = FirebaseDatabaseImpl()
    private val transformationsUtils = TransformationsUtils(application)
    private lateinit var token: String
    private lateinit var localId: String
    val internetConnection = MutableLiveData<Boolean>()
    val checkInternetConnection = MutableLiveData<Boolean>()

    init {
        val user = LocalStorageUtils.getUserAndToken(getApplication())
        token = user.second.idToken!!
        localId = user.second.uuid!!
        internetConnection.value = false
        checkInternetConnection.value = false
    }

    fun reset() {
        patientEmail.set("")
        routeName.set("")
    }

    fun checkFields(): Boolean {
        success.value = false
        checkInternetConnection.value = true
        if (!internetConnection.value!!)
            return false
        onLoadProgressBar.value = true
        emailError.value = ""
        routeNameError.value = ""
        if (routeName.get()!!.isNullOrEmpty()) {
            routeNameError.value = "Field must not be empty!"
            return false
        } else
            routeNameError.value = null
        if (patientEmail.get()!!.isNullOrEmpty()) {
            emailError.value = "Field must not be empty!"
            return false
        } else
            emailError.value = null
        return true
    }

    fun insertDefinedRoute() {
        if (checkFields()) {
            val definedRouteLength = this.computeTotalDistance(definedPoints)
            val definedRoute = DefinedRoute(routeName.get()!!, definedPoints, definedRouteLength)
            firebaseDatabaseService.addDefinedRoute(token, localId, patientEmail.get()!!, definedRoute, ::onSuccessInsertDefinedRoute, ::onErrorInsertDefinedRoute)
        }
    }

    private fun computeTotalDistance(list: List<LatLng>): Int {
        var totalDistance = 0
        for (i in 0 until list.size - 1) {
            totalDistance += transformationsUtils.haversineDistance(list[i], list[i + 1]).toInt()
        }
        return totalDistance
    }

    // TODO: introducere mesaj de confirmare adaugare traseu nou pt nevazator
    private fun onSuccessInsertDefinedRoute() {
        Log.i("Success", "Insert new defined route with success!")
        successText.value = "Route added with success!"
        success.value = true
        onLoadProgressBar.value = false
    }

    private fun onErrorInsertDefinedRoute(error: Int) {
        Log.e("Error", "Insert new defined route with error. Error code: $error")
        success.value = false
        when (error) {
            1 -> {
                emailError.value = "User does not exist!"
            }
            4 -> {
                routeNameError.value = "This route name already exists!"
            }
            5 -> {
                emailError.value = "Patient with this email does not belong to your care!"
            }
        }
        onLoadProgressBar.value = false
    }
}