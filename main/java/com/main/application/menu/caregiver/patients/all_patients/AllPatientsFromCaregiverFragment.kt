package com.main.application.menu.caregiver.patients.all_patients

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.databinding.FragmentAllPatientsFromCaregiverBinding
import com.main.application.menu.caregiver.patients.all_patients.recycler_view.PatientAdapter

class AllPatientsFromCaregiverFragment: Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[AllPatientsFromCaregiverViewModel::class.java]
    }
    private lateinit var adapter: PatientAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentAllPatientsFromCaregiverBinding>(
            inflater,
            R.layout.fragment_all_patients_from_caregiver,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val title = context!!.resources.getString(R.string.all_patients)
        (activity!! as MainActivity).setToolbarTitle(title)
        setRecyclerView(binding.allPatientsList)

        viewModel.existsUsers.observe(this, Observer { shouldNotifyAdapter ->
            if (shouldNotifyAdapter) {
                adapter.setPatientList(viewModel.allPatients)
            }
        })

        AllPatientsFromCaregiverViewModel.invite.observe(this, Observer { shouldSendInvitation ->
            if (shouldSendInvitation) {
                AllPatientsFromCaregiverViewModel.invite.value = false
                viewModel.invitePatient()
            }
        })

        AllPatientsFromCaregiverViewModel.removeInvitation.observe(this, Observer { shouldRemoveInvitation ->
            if (shouldRemoveInvitation) {
                AllPatientsFromCaregiverViewModel.removeInvitation.value = false
                viewModel.removeInvitation()
            }
        })

        viewModel.readyToUpdate.observe(this, Observer { shouldUpdateAdapter ->
            if (shouldUpdateAdapter) {
                viewModel.readyToUpdate.value = false
                adapter.setPatientList(viewModel.newPatients)
            }
        })

        return binding.root
    }

    private fun setRecyclerView(recyclerView: RecyclerView) {
        adapter = PatientAdapter(context!!, mutableListOf())
        recyclerView.layoutManager = LinearLayoutManager(context!!)
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }
}