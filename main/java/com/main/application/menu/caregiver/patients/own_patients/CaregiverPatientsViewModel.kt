package com.main.application.menu.caregiver.patients.own_patients

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.main.application.model.firebase.User
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils

class CaregiverPatientsViewModel(application: Application): AndroidViewModel(application) {

    var patients = mutableListOf<Pair<User, String>>()
    val existsUsers = MutableLiveData<Boolean>()
    val onSwipeToRefresh = MutableLiveData<Boolean>()

    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()


    init {
        existsUsers.value = false
        onSwipeToRefresh.value = true
        getMyPatients()
    }

    fun getMyPatients() {
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication()).second
        val token = userAndToken.idToken!!
        val userUID = userAndToken.uuid!!
        firebaseDatabase.getCaregiverPatients(userUID, token, ::onSuccessGetPatients, ::onErrorGetPatients)
    }

    private fun onSuccessGetPatients(response: MutableList<Pair<User, String>>) {
        Log.i("Success", "Get patients with success!")
        patients = response
        existsUsers.value = true
        onSwipeToRefresh.value = false
    }

    private fun onErrorGetPatients(error: Int) {
        Log.e("Error", "Get patients with error. Error code: $error")
        existsUsers.value = true
        onSwipeToRefresh.value = false
    }
}