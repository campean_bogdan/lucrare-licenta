package com.main.application.menu.caregiver.patients.own_patients.recycler_view

import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.main.application.R
import com.main.application.constants.AppConstants
import com.main.application.model.firebase.User

class OwnPatientViewHolder(
    itemView: View
): RecyclerView.ViewHolder(itemView) {

    private var cardView: CardView = itemView.findViewById(R.id.id_cardView_viewHolder_patient)
    private var patientName: TextView = itemView.findViewById(R.id.patient_name)
    private var patientEmail: TextView = itemView.findViewById(R.id.patient_email)
    private var patientCity: TextView = itemView.findViewById(R.id.patient_city)
    private var patientStatus: TextView = itemView.findViewById(R.id.patient_status)


    fun bindViewHolder(patient: Pair<User, String>) {
        patientName.typeface = Typeface.DEFAULT_BOLD
        patientEmail.typeface = Typeface.DEFAULT_BOLD
        patientCity.typeface = Typeface.DEFAULT_BOLD
        patientStatus.typeface = Typeface.DEFAULT_BOLD

        patientName.text = patient.first.name
        patientEmail.text = patient.first.email
        patientCity.text = patient.first.city
        patientStatus.text = patient.second

        // used when on click => view all defined routes about patient
        cardView.setOnClickListener { cardView ->
            val activity = cardView.context as AppCompatActivity
            val bundle = bundleOf(
                AppConstants.EMAIL to patient.first.email,
                AppConstants.NAME to patient.first.name
            )
            cardView.findNavController().navigate(R.id.action_caregiverPatientsFragment_to_definedRoutesFragment, bundle)
        }
    }
}