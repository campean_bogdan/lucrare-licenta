package com.main.application.menu.caregiver.patients.all_patients.recycler_view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.main.application.R
import com.main.application.model.firebase.User

class PatientAdapter(
    private val context: Context,
    private var patients: List<Pair<User, String>>
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun setPatientList(patients: List<Pair<User, String>>) {
        this.patients = patients
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return PatientViewHolder(
            inflater.inflate(R.layout.patient_view_holder, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return patients.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PatientViewHolder).bindViewHolder(context, patients[position])
    }
}