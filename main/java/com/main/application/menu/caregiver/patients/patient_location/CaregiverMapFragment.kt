package com.main.application.menu.caregiver.patients.patient_location

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.messaging.RemoteMessage
import com.main.application.R
import com.main.application.constants.NotificationType
import com.main.application.databinding.FragmentCaregiverMapBinding

class CaregiverMapFragment: Fragment(), OnMapReadyCallback {

    companion object {
        const val MESSAGE_TYPE = "HELP"
        const val PATIENT_UID = "senderUID"
    }

    private var mapFragment = SupportMapFragment.newInstance()
    private lateinit var map: GoogleMap

    private val viewModel by lazy {
        ViewModelProviders.of(this)[CaregiverMapViewModel::class.java]
    }
    private var patientUID = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentCaregiverMapBinding>(
            inflater,
            R.layout.fragment_caregiver_map,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val title = context!!.resources.getString(R.string.patient_location)
        //(activity!! as MapsActivity).setToolbarTitle(title)
        setToolbarTitle(title)


        val bundle = activity!!.intent!!.extras
        if (bundle != null) {
            val message = bundle[NotificationType.HELP] as RemoteMessage
            patientUID = message.data[PATIENT_UID]!!
        }

        mapFragment = childFragmentManager.findFragmentById(R.id.map_caregiver) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return binding.root
    }

    fun setToolbarTitle(text: String) {
        val toolbar = activity!!.findViewById<Toolbar>(R.id.toolbar_1)
        toolbar.title = text
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap!!

        // If need some value/calculation from View Model, use a lambda function from Fragment to get it
        // Ex: this method goes as parameter through view model, service; when the computation from service is over, function 'onSuccess' is called
        // Method 'patientLocation' is called from 'onSuccess' method
        viewModel.placePatientPosition(patientUID, ::patientLocation)

        // Delete 'help' from database. It is used to let blind user to call for future help (because it won't rewrite 'help' field in database)
        viewModel.deleteHelp(patientUID)
    }

    private fun patientLocation(location: LatLng) {
        placeMarker(map, location, "Patient")
    }

    private fun placeMarker(map: GoogleMap, location: LatLng, title: String) {
        map.addMarker(MarkerOptions().position(location).title(title))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15.0f))
    }
}