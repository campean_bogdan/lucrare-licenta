package com.main.application.menu.caregiver.patients.own_patients

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.databinding.FragmentCaregiverPatientsBinding
import com.main.application.menu.caregiver.patients.own_patients.recycler_view.OwnPatientAdapter

class CaregiverPatientsFragment: Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[CaregiverPatientsViewModel::class.java]
    }
    private lateinit var adapterOwn: OwnPatientAdapter
    private lateinit var binding: FragmentCaregiverPatientsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_caregiver_patients,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val title = context!!.resources.getString(R.string.my_patients)
        (activity!! as MainActivity).setToolbarTitle(title)

        setRecyclerView(binding.myPatientsList)

        viewModel.existsUsers.observe(this, Observer { shouldUpdatePatientList ->
            if (shouldUpdatePatientList) {
                adapterOwn.setPatientList(viewModel.patients)
            }
        })

        return binding.root
    }

    private fun setRecyclerView(recyclerView: RecyclerView) {
        adapterOwn =
            OwnPatientAdapter(
                context!!,
                mutableListOf()
            )
        recyclerView.layoutManager = LinearLayoutManager(context!!)
        recyclerView.adapter = adapterOwn
        adapterOwn.notifyDataSetChanged()
    }
}