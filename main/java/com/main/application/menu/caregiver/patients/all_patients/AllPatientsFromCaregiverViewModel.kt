package com.main.application.menu.caregiver.patients.all_patients

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.main.application.constants.InvitationResponse
import com.main.application.model.firebase.User
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils

class AllPatientsFromCaregiverViewModel(application: Application): AndroidViewModel(application) {

    val onSwipeRefresh = MutableLiveData<Boolean>()
    val existsUsers = MutableLiveData<Boolean>()
    var allPatients = mutableListOf<Pair<User, String>>()
    var newPatients = mutableListOf<Pair<User, String>>()
    var readyToUpdate = MutableLiveData<Boolean>()
    val searchNameField = MutableLiveData<String>()
    private val token = LocalStorageUtils.getUserAndToken(application).second.idToken!!
    private val uuid = LocalStorageUtils.getUserAndToken(application).second.uuid!!
    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()

    companion object {
        val invite = MutableLiveData<Boolean>()
        val emailInvite = MutableLiveData<String>()
        val removeInvitation = MutableLiveData<Boolean>()
        val removeKey = MutableLiveData<String>()
    }

    init {
        existsUsers.value = false
        onSwipeRefresh.value = true
        getPatients()
    }

    fun getPatients() {
        onSwipeRefresh.value = true
        firebaseDatabase.getAllPatients(uuid, token, ::onSuccessGetAllPatients, ::onErrorGetAllPatients)
    }

    fun invitePatient() {
        onSwipeRefresh.value = true
        firebaseDatabase.sendInvitationToEmail(uuid, emailInvite.value!!, token, InvitationResponse.PENDING, ::onSuccessSendInvitation, ::onErrorSendInvitation)
    }

    fun removeInvitation() {
        onSwipeRefresh.value = true
        firebaseDatabase.deleteInvitation(uuid, removeKey.value!!, token, ::onSuccessRemoveInvitation, ::onErrorRemoveInvitation)
    }

    fun searchByNameAndEmail() {
        if (searchNameField.value == null) {
            newPatients = allPatients.toMutableList()
            readyToUpdate.value = true
        } else {
            newPatients.clear()
            for (patient in allPatients) {
                if (patient.first.name.contains(searchNameField.value!!, true) ||
                    patient.first.email.contains(searchNameField.value!!, true)
                ) {
                    newPatients.add(patient)
                }
            }
            readyToUpdate.value = true
        }
    }

    private fun onSuccessGetAllPatients(patients: List<Pair<User, String>>) {
        Log.i("Success", "AllPatientsFromCaregiverViewModel. Success get all patients!")
        this.allPatients = patients.toMutableList()
        existsUsers.value = true
        onSwipeRefresh.value = false
    }

    private fun onErrorGetAllPatients(error: Int) {
        Log.e("Error", "AllPatientsFromCaregiverViewModel. Error get all patients. Error code: $error")
        existsUsers.value = true
        onSwipeRefresh.value = false
    }

    private fun onSuccessSendInvitation() {
        Log.i("Success", "AllPatientsFromCaregiverViewModel. Success invite patient!")
        onSwipeRefresh.value = false
    }

    private fun onErrorSendInvitation(error: Int) {
        Log.e("Error", "AllPatientsFromCaregiverViewModel. Send invitation with error. Error code: $error")
        onSwipeRefresh.value = false
    }

    private fun onSuccessRemoveInvitation() {
        Log.i("Success", "AllPatientsFromCaregiverViewModel. Success remove invitation!")
        onSwipeRefresh.value = false
    }

    private fun onErrorRemoveInvitation(error: Int) {
        Log.e("Error", "AllPatientsFromCaregiverViewModel. Remove invitation with error. Error code: $error")
        onSwipeRefresh.value = false
    }
}