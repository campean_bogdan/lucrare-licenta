package com.main.application.menu.caregiver.patients.patient_location

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils

class CaregiverMapViewModel(application: Application): AndroidViewModel(application) {

    val onLoadProgressBar = MutableLiveData<Boolean>()

    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()

    init {
        onLoadProgressBar.value = true
    }

    /**
     * Place marker where the patient is
     */
    fun placePatientPosition(patientUID: String, execute: (LatLng) -> Unit) {
        val token = LocalStorageUtils.getUserAndToken(getApplication()).second.idToken!!
        firebaseDatabase.getHelpedPatientLocation(patientUID, token, execute, ::onSuccessGetPatientLocation, ::onErrorGetPatientLocation)
    }

    /**
     * Delete 'help' from database.
     */
    fun deleteHelp(patientUID: String) {
        val token = LocalStorageUtils.getUserAndToken(getApplication()).second.idToken!!
        firebaseDatabase.deleteHelp(patientUID, token, ::onSuccessDeleteHelp, ::onErrorDeleteHelp)
    }

    private fun onSuccessDeleteHelp() {
        Log.i("Success", "Delete help with success!")
    }

    private fun onErrorDeleteHelp(error: Int) {
        Log.i("Error", "Delete help with error. Error code: $error")
    }

    private fun onSuccessGetPatientLocation(execute: (LatLng) -> Unit, response: LatLng) {
        Log.i("Success", "Get patient's position with success!")
        onLoadProgressBar.value = false
        execute(response)
    }

    private fun onErrorGetPatientLocation(error: Int) {
        Log.e("Error", "Get patient's position with error. Error code: $error")
        onLoadProgressBar.value = false
    }
}