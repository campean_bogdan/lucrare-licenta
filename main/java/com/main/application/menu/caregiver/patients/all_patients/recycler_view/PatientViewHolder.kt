package com.main.application.menu.caregiver.patients.all_patients.recycler_view

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Typeface
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.main.application.R
import com.main.application.constants.AppConstants
import com.main.application.constants.InvitationResponse
import com.main.application.menu.caregiver.patients.all_patients.AllPatientsFromCaregiverViewModel
import com.main.application.model.firebase.User

class PatientViewHolder(
    itemView: View
): RecyclerView.ViewHolder(itemView) {

    private lateinit var context: Context
    private var cardView: CardView = itemView.findViewById(R.id.id_all_patients_cardView)
    private var patientName: TextView = itemView.findViewById(R.id.all_patient_name)
    private var patientEmail: TextView = itemView.findViewById(R.id.all_patient_email)
    private var patientCity: TextView = itemView.findViewById(R.id.all_patient_city)
    private var patientStatus: TextView = itemView.findViewById(R.id.all_patient_status)
    private var inviteButton: Button = itemView.findViewById(R.id.patient_invite_button)
    private var removeButton: Button = itemView.findViewById(R.id.patient_remove_button)


    fun bindViewHolder(context: Context, patient: Pair<User, String>) {
        patientName.typeface = Typeface.DEFAULT_BOLD
        patientEmail.typeface = Typeface.DEFAULT_BOLD
        patientCity.typeface = Typeface.DEFAULT_BOLD
        patientStatus.typeface = Typeface.DEFAULT_BOLD

        this.context = context
        patientName.text = patient.first.name
        patientEmail.text = patient.first.email
        patientCity.text = patient.first.city
        patientStatus.text = patient.second

        if (patientStatus.text == InvitationResponse.PENDING || patientStatus.text == InvitationResponse.ACCEPT) {
            inviteButton.visibility = View.INVISIBLE
            removeButton.visibility = View.VISIBLE
        }
        if (patientStatus.text == InvitationResponse.NOT_CONNECTED || patientStatus.text == InvitationResponse.DECLINE) {
            inviteButton.visibility = View.VISIBLE
            removeButton.visibility = View.INVISIBLE
        }

        // used when on click => view all defined routes about patient
        cardView.setOnClickListener { cardView ->
            val activity = cardView.context as AppCompatActivity
            val bundle = bundleOf(
                AppConstants.EMAIL to patient.first.email,
                AppConstants.NAME to patient.first.name
            )
            // TODO
            //cardView.findNavController().navigate(R.id.action_caregiverPatientsFragment_to_definedRoutesFragment, bundle)
        }

        // when press on invite/decline => do action
        inviteButton.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("Invite Patient")
                .setMessage("Are you sure you want to connect with this patient?")
                .setPositiveButton(R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                    AllPatientsFromCaregiverViewModel.emailInvite.value = patientEmail.text.toString()
                    AllPatientsFromCaregiverViewModel.invite.value = true
                    removeButton.visibility = View.VISIBLE
                    inviteButton.visibility = View.INVISIBLE
                    patientStatus.text = InvitationResponse.PENDING
                })
                .setNegativeButton(R.string.no, null)
                .show()
        }

        removeButton.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("Remove Patient")
                .setMessage("Are you sure you want to delete this patient from care list?")
                .setPositiveButton(R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                    AllPatientsFromCaregiverViewModel.removeKey.value = patient.first.key
                    AllPatientsFromCaregiverViewModel.removeInvitation.value = true
                    removeButton.visibility = View.INVISIBLE
                    inviteButton.visibility = View.VISIBLE
                    patientStatus.text = InvitationResponse.NOT_CONNECTED
                })
                .setNegativeButton(R.string.no, null)
                .show()
        }
    }
}