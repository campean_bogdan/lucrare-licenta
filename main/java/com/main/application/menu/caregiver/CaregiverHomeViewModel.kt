package com.main.application.menu.caregiver

import android.app.Application
import android.util.Log
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.main.application.constants.InvitationResponse
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils

class CaregiverHomeViewModel(application: Application): AndroidViewModel(application) {

    val email = MutableLiveData<String>()
    val emailError = MutableLiveData<Boolean>()
    val emailErrorMessage = MutableLiveData<String>()
    val onLoadProgressBar = MutableLiveData<Boolean>()
    val successSendMessage = MutableLiveData<String>()
    val successSend = MutableLiveData<Boolean>()
    val navigateHome = MutableLiveData<Boolean>()

    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()

    init {
        emailError.value = false
        onLoadProgressBar.value = false
        successSend.value = false
        navigateHome.value = false
    }


    fun checkEmail(): Boolean {
        onLoadProgressBar.value = true
        if (!Patterns.EMAIL_ADDRESS.matcher(email.value!!).matches()) {
            emailErrorMessage.value = "This is not a valid email address."
            emailError.value = true
            onLoadProgressBar.value = false
            return false
        }
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication())
        val token = userAndToken.second.idToken!!
        val userUID = userAndToken.second.uuid!!
        // trimit o invitatie spre user-ul cu emailul de mai sus => initial invitatia se salveaza in baza de date ca si PENDING
        firebaseDatabase.sendInvitationToEmail(userUID, email.value!!, token, InvitationResponse.PENDING, ::onSuccessSendInvitation, ::onErrorSendInvitation)

        return true
    }

    fun logout() {
        LocalStorageUtils.removeAll(getApplication())
        navigateHome.value = true
    }

    private fun onSuccessSendInvitation() {
        Log.i("Success", "Send invitation with success!")
        successSendMessage.value = "Sent invitation to ${email.value} with success!"
        emailError.value = false
        onLoadProgressBar.value = false
        successSend.value = true
    }

    private fun onErrorSendInvitation(error: Int) {
        Log.e("Error", "Send invitation with error. Error code: $error")
        when (error) {
            0 -> emailErrorMessage.value = "Email address does not exist!"
            1 -> emailErrorMessage.value = "Email address does not correspond to a patient!"
        }
        emailError.value = true
        successSend.value = false
        onLoadProgressBar.value = false
    }
}