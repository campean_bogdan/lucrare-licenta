package com.main.application.menu.caregiver.road_events

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.databinding.FragmentAddRoadEventMapBinding
import com.main.application.model.firebase.RoadEvent

class AddRoadEventsMapFragment : Fragment(), OnMapReadyCallback {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[AddRoadEventsViewModel::class.java]
    }

    private var mapFragment = SupportMapFragment.newInstance()
    private lateinit var map: GoogleMap
    private var currentPositionMarker: Marker? = null
    private var markers = mutableMapOf<String, Marker>()
    private val RECENTLY_ADDED = "Recently added"


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentAddRoadEventMapBinding>(
            inflater,
            R.layout.fragment_add_road_event_map,
            container,
            false
        )

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val title = context!!.resources.getString(R.string.add_road_event)
        (activity!! as MainActivity).setToolbarTitle(title)

        binding.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.roadEvent = parent!!.getItemAtPosition(position)!! as String
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        mapFragment = childFragmentManager.findFragmentById(R.id.map_add_road_event) as SupportMapFragment
        mapFragment.getMapAsync(this)

        viewModel.addMarker.observe(this, Observer { shouldAddMarkerOnLocation ->
            if (shouldAddMarkerOnLocation) {
                val marker = map.addMarker(MarkerOptions()
                    .position(LatLng(viewModel.latitude, viewModel.longitude))
                    .title(RECENTLY_ADDED)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                )
                markers[viewModel.key] = marker
                viewModel.addMarker.value = false
            }
        })

        viewModel.canRemoveMarkers.observe(this, Observer { shouldRemoveMarker ->
            if (shouldRemoveMarker) {
                val marker = markers[viewModel.key]
                marker?.remove()
                viewModel.canRemoveMarkers.value = false
                markers.remove(viewModel.key)
            }
        })

        return binding.root
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap!!

        // set move listener on map when the map is ready
        map.setOnCameraMoveListener {
            viewModel.setCenterLocation(map.cameraPosition.target!!)
        }

        map.setOnMarkerClickListener{
            viewModel.key = this.getKey(it.id)
            viewModel.deleteEnabled.value = it.title != RECENTLY_ADDED
            false
        }

        viewModel.fetchAllRoadEvents(::setMarkersOnRoadEvents)

        val locationManager =
            activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location?) {
                setMarker(location!!)
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onProviderEnabled(provider: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onProviderDisabled(provider: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        if (ContextCompat.checkSelfPermission(
                context!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                5000,
                10.0f,
                locationListener
            )
        }
    }

    private fun setMarker(location: Location) {
        val locationAsLatLng = LatLng(location.latitude, location.longitude)
        currentPositionMarker?.remove()
        currentPositionMarker =
            map.addMarker(MarkerOptions().position(locationAsLatLng).title("You Are Here"))
        if (viewModel.firstLocation) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(locationAsLatLng, 18.0f))
            viewModel.firstLocation = false
        }
    }

    private fun setMarkersOnRoadEvents(listOfRoadEvents: Map<String, RoadEvent>) {
        for (entry in listOfRoadEvents) {
            val marker = map.addMarker(
                MarkerOptions()
                    .position(LatLng(entry.value.latitude, entry.value.longitude))
                    .title("${entry.value.category.name} - ${entry.value.email}")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                )
            markers[entry.key] = marker
        }
    }

    private fun getKey(id: String): String {
        for (entry in markers) {
            if (id == entry.value.id)
                return entry.key
        }
        return "-"
    }
}