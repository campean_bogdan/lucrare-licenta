package com.main.application.menu.caregiver.road_events

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.main.application.constants.AppConstants
import com.main.application.model.firebase.RoadEvent
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.LocalStorageUtils
import java.util.*

class AddRoadEventsViewModel(application: Application): AndroidViewModel(application) {

    val onLoadProgressBar = MutableLiveData<Boolean>()
    val mapCenterLocation = MutableLiveData<String>()
    val addMarker = MutableLiveData<Boolean>()
    val canRemoveMarkers = MutableLiveData<Boolean>()
    var roadEvent = ""
    var firstLocation = true
    val email: String = ""
    var key = "-"
    val deleteEnabled = MutableLiveData<Boolean>()

    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()
    var latitude = 0.0
    var longitude = 0.0
    private lateinit var token: String


    init {
        deleteEnabled.value = true
        onLoadProgressBar.value = false
        addMarker.value = false
        mapCenterLocation.value = "Lat: 0.0\nLon: 0.0"
        token = LocalStorageUtils.getUserAndToken(getApplication()).second.idToken!!
    }

    /**
     * Get coordinates in the middle of the map, where the pin image is shown.
     */
    fun setCenterLocation(location: LatLng) {
        this.latitude = location.latitude
        this.longitude = location.longitude
        val latitude = "%.5f".format(location.latitude).toDouble()
        val longitude = "%.5f".format(location.longitude).toDouble()
        mapCenterLocation.value = "Lat: $latitude\nLon: $longitude"
    }

    fun addRoadEvent() {
        var category: AppConstants.RoadEventsCategory? = null
        when (roadEvent) {
            "Simple Crossing" -> category = AppConstants.RoadEventsCategory.SIMPLE_CROSSING
            "Traffic Light Crossing" -> category = AppConstants.RoadEventsCategory.TRAFFIC_LIGHT_CROSSING
            "Crossing Group" -> category = AppConstants.RoadEventsCategory.TRAFFIC_LIGHT_GROUP_CROSSING
        }

        if (category != null) {
            val email = LocalStorageUtils.getUserAndToken(getApplication()).first
            onLoadProgressBar.value = true
            val newRoadEvent = RoadEvent(category, latitude, longitude, email, Date())
            firebaseDatabase.addRoadEvent(token, newRoadEvent, ::onSuccessAddRoadEvent, ::onErrorAddRoadEvent)
        }
    }

    fun fetchAllRoadEvents(execute: (Map<String, RoadEvent>) -> Unit) {
        onLoadProgressBar.value = true
        firebaseDatabase.getAllRoadEvents(token, execute, ::onSuccessFetchAllRoadEvents, ::onErrorFetchAllRoadEvents)
    }

    fun deleteRoadEvent() {
        onLoadProgressBar.value = true
        firebaseDatabase.deleteRoadEvent(key, token, ::onSuccessDeleteRE, ::onErrorDeleteRE)
    }

    private fun onSuccessDeleteRE() {
        Log.i("Success", "AddRoadEventsVM. Delete RE with success.")
        onLoadProgressBar.value = false
        canRemoveMarkers.value = true
    }

    private fun onErrorDeleteRE(error: Int) {
        Log.e("Error", "AddRoadEventsVM. Delete RE with error. Error code: $error")
        onLoadProgressBar.value = false
    }

    private fun onSuccessFetchAllRoadEvents(execute: (Map<String, RoadEvent>) -> Unit, roadEvents: Map<String, RoadEvent>) {
        Log.i("Success", "Fetch all road events with success!")
        onLoadProgressBar.value = false
        execute(roadEvents)
    }

    private fun onErrorFetchAllRoadEvents(error: Int) {
        Log.e("Error", "Fetch all road events with error. Error code: $error")
        onLoadProgressBar.value = false
    }

    private fun onSuccessAddRoadEvent(key: String) {
        Log.i("Success", "Add road event with success!")
        onLoadProgressBar.value = false
        addMarker.value = true
        this.key = key
    }

    private fun onErrorAddRoadEvent(error: Int) {
        Log.e("Error", "Add road event with error. Error code: $error")
        onLoadProgressBar.value = false
        addMarker.value = true
    }
}