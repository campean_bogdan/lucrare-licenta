package com.main.application.menu.caregiver

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.databinding.FragmentCaregiverBinding

class CaregiverHomeFragment: Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[CaregiverHomeViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentCaregiverBinding>(
            inflater,
            R.layout.fragment_caregiver,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val title = context!!.resources.getString(R.string.caregiver_menu)
        (activity!! as MainActivity).setToolbarTitle(title)

        viewModel.navigateHome.observe(this, Observer { shouldNavigateToLogin ->
            if (shouldNavigateToLogin) {
                findNavController().navigate(R.id.loginFragment)
                viewModel.navigateHome.value = false
            }
        })

        return binding.root
    }


}