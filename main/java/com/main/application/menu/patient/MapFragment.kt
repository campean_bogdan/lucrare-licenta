package com.main.application.menu.patient


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.hardware.GeomagneticField
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.speech.RecognizerIntent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.main.application.MainActivity
import com.main.application.R
import com.main.application.constants.AppConstants
import com.main.application.databinding.FragmentMapBinding
import com.main.application.di.applicationModule
import com.main.application.model.firebase.RoadEvent
import com.main.application.sensor.OrientationSensorActivity
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin

/**
 * A simple [Fragment] subclass.
 */
class MapFragment : Fragment(), OnMapReadyCallback {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[MapsFragmentViewModel::class.java]
    }

    private var mapFragment = SupportMapFragment.newInstance()
    private lateinit var map: GoogleMap
    private var currentPositionMarker: Marker? = null
    private var firstView = true
    private lateinit var orientationSensor: OrientationSensorActivity
    private var bearing = 0.0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentMapBinding>(
            inflater,
            R.layout.fragment_map,
            container,
            false
        )

        val title = context!!.resources.getString(R.string.navigate)
        (activity!! as MainActivity).setToolbarTitle(title)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        orientationSensor = OrientationSensorActivity(activity!!)

//        viewModel.currentOrientation.observe(this, Observer { newMarkerOrientation ->
//            currentPositionMarker!!.rotation = newMarkerOrientation.toFloat()
//            println("AICI: ROTATE   $newMarkerOrientation ${currentPositionMarker!!.rotation}")
//        })

        // render path from Google service
        viewModel.readyToDrawLineGoogle.observe(this, Observer { shouldDrawLinesOnMap ->
            if (shouldDrawLinesOnMap) {
                viewModel.drawPathFromJson(::drawPolyline, ::drawPoint, ::drawPolyline2)
            }
        })

        // draw path from defined route points
        viewModel.readyToDrawLineDefinedPath.observe(this, Observer { shouldDrawPath ->
            if (shouldDrawPath) {
                viewModel.drawPathFromPoints(::drawPolyline, ::drawPoint, ::drawPolyline2)
            }
        })

        // when exists a route, we get all road events
        viewModel.existsPath.observe(this, Observer { shouldGetRoadEvents ->
            if (shouldGetRoadEvents) {
                viewModel.getRoadEvents(::drawRouteLimits, ::setMarkerOnRoadEvents)
            }
        })

        viewModel.canClearMap.observe(this, Observer { shouldDeleteAllMapData ->
            if (shouldDeleteAllMapData) {
                map.clear()
                viewModel.canClearMap.value = false
            }
        })

        viewModel.canLogout.observe(this, Observer { shouldLogout ->
            if (shouldLogout) {
                findNavController().navigate(R.id.action_mapFragment_to_loginFragment)
                viewModel.canLogout.value = false
            }
        })

        // draw path from Google Directions
        viewModel.readyToDrawPathLines.observe(this, Observer { shouldDrawPathLines ->
            if (shouldDrawPathLines) {
                this.drawPolyline3(viewModel.polylineCenter.first.points, Color.RED)

                // draw first and last points
                val firstP = viewModel.roadList[0]
                val lastP = viewModel.roadList[viewModel.roadList.size - 1]
                map.addMarker(
                    MarkerOptions()
                        .position(firstP.first)
                        .title("Start Point")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                )

                map.addMarker(
                    MarkerOptions()
                        .position(lastP.first)
                        .title("End Point")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                )

                // draw turning points
                for (i in 0 until viewModel.turningPoints.size) {
                    val turningPoint = viewModel.turningPoints[i]
                    map.addMarker(
                        MarkerOptions()
                            .position(turningPoint.location)
                            .title("${turningPoint.direction} - ${turningPoint.angle}")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                    )
                }

                // draw road event points
                for (i in 0 until viewModel.roadObjects.size) {
                    val obj = viewModel.roadObjects[i]
                    if (obj.second == AppConstants.Type.ROAD_EVENT) {
                        val point = (obj.first as RoadEvent)
                        map.addMarker(
                            MarkerOptions()
                                .position(point.toLatLng())
                                .title("${point.category} - ${point.email}")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                        )
                    }
                }
            }
        })

        //android:onClick="@{() -> viewModel.searchGoogleMapsDestinationWithSimpleOptions()}"

        // cand se apasa pentru a asculta o comanda; text to speech

//        binding.map.setOnClickListener {
//            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
//            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ro-RO")
//            startActivityForResult(intent, 10)
//        }

        return binding.root
    }

    override fun onPause() {
        super.onPause()
        viewModel.unregisterSensors()
        stopKoin()
    }

    override fun onResume() {
        super.onResume()
        viewModel.registerSensors()
    }

    private fun setMarkerLocation(location: Location) {
        viewModel.lastKnownLocation = location
        this.setCurrentPositionMarker(location)

        // daca exista o cerere de traseu => request la osm
        if (viewModel.existsPath.value!!) {
            // TODO: uncomment next lines
            val locationAsLatLng = LatLng(location.latitude, location.longitude)
            viewModel.navigate(map, locationAsLatLng)
        }

        map.setOnMapClickListener {locationAsLatLng ->
            // TODO: se apasa pe harta pentru a folosi text-to-speech
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ro-RO")
            startActivityForResult(intent, 10)

            // TODO: se apasa pentru a simula traseul
//            if (viewModel.existsPath.value!!) {
//                viewModel.addMarkerOnLocation(map, locationAsLatLng!!)
//                viewModel.navigate(map, locationAsLatLng)
//            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.destroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        startKoin {
            androidLogger()
            androidContext(activity!!.applicationContext)
            modules(applicationModule)
        }
    }

    /**
     * Draw road events on map. It is given a list of road events used in MapsFragmentViewModel as parameter
     */
    private fun setMarkerOnRoadEvents(roadEvents: List<RoadEvent>) {
        for (roadEvent in roadEvents) {
            map.addMarker(MarkerOptions()
                .title("${roadEvent.category.name} - ${roadEvent.email}")
                .position(LatLng(roadEvent.latitude, roadEvent.longitude))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
            )
        }
    }

    private fun drawPolyline(listOfPoints: List<List<LatLng>>, polylineOptions: PolylineOptions) {
        for (element in listOfPoints) {
            map.addPolyline(polylineOptions.addAll(element).color(Color.RED))
            var i = 0
            for (e in element) {
                //map.addMarker(MarkerOptions().title(i.toString()).position(e).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)))
                i++
            }
        }
        //return polylineOptions
    }

    private fun drawPolyline2(listOfPoints: List<LatLng>, polylineOptions: PolylineOptions) {
        map.addPolyline(polylineOptions.addAll(listOfPoints).color(Color.BLACK))
    }

    private fun drawPolyline3(listOfPoints: List<LatLng>, color: Int) {
        map.addPolyline(PolylineOptions().addAll(listOfPoints).color(color))
    }

    /**
     * This method draw the 4 extreme points of route. Those points are making a square
     */
    private fun drawRouteLimits(posA: LatLng, posB: LatLng, posC: LatLng, posD: LatLng) {
        map.addMarker(MarkerOptions().position(posA).title("${posA.latitude}\n${posA.longitude}\nA").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
        map.addMarker(MarkerOptions().position(posB).title("${posB.latitude} ${posB.longitude}\nB").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
        map.addMarker(MarkerOptions().position(posC).title("${posC.latitude} ${posC.longitude}\nC").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
        map.addMarker(MarkerOptions().position(posD).title("${posD.latitude} ${posD.longitude}\nD").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
    }

    /**
     * Set current user's location on map.
     */
    private fun setCurrentPositionMarker(location: Location) {
        val marker = LatLng(location.latitude, location.longitude)
        currentPositionMarker?.remove()
        currentPositionMarker = map.addMarker(MarkerOptions().position(marker).title("You Are Here"))
        if (currentPositionMarker != null) {
            currentPositionMarker!!.showInfoWindow()
            // set marker orientation
            //currentPositionMarker!!.rotation = orientationSensor.mAzimuth.toFloat() - 180.0f
            //println("AICI: ROTATE   ${orientationSensor.mAzimuth.toFloat()} ${currentPositionMarker!!.rotation}")
            //currentPositionMarker!!.rotation = 90.0f
            //currentPositionMarker!!.snippet = "${currentPositionMarker!!.rotation}"
        }
        // TODO: DELETE firstView after tests, because you always want to center camera on current position
        if (firstView) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 19F))
            firstView = false
        }
        orientationSensor.marker = marker
        val oldPos = map.cameraPosition
        val newPos = CameraPosition.builder(oldPos).bearing(bearing.toFloat()).build()
        map.moveCamera(CameraUpdateFactory.newCameraPosition(newPos))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 19F))
    }

    // TODO: delete after test
    private fun drawPoint(point: LatLng, title: String) {
        map.addMarker(MarkerOptions().position(point).title(title).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)))
    }

    // TODO: delete after test
//    private fun drawPolyline2(point1: LatLng, point2: LatLng) {
//        val list = mutableListOf<LatLng>()
//        list.addAll(0, listOf(point1, point2))
//        map.addPolyline(PolylineOptions().addAll(list).color(Color.BLACK))
//    }
}
