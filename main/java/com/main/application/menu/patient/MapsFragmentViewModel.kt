package com.main.application.menu.patient

import android.app.Application
import android.location.Location
import android.os.CountDownTimer
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.main.application.constants.AppConstants
import com.main.application.constants.Commands
import com.main.application.constants.Commands.BACK_RO
import com.main.application.constants.Commands.DELETE_POINT_OF_INTEREST_RO
import com.main.application.constants.Commands.GET_CURRENT_LOCATION_INFORMATION_RO
import com.main.application.constants.Commands.GET_ROUTE_RO
import com.main.application.constants.Commands.GET_TO_DESTINATION_RO
import com.main.application.constants.Commands.GET_TO_POINT_OF_INTEREST_RO
import com.main.application.constants.Commands.LOGOUT
import com.main.application.constants.Commands.NEED_HELP_RO
import com.main.application.constants.Commands.SET_NEW_POINT_OF_INTEREST_RO
import com.main.application.model.firebase.PointOfInterest
import com.main.application.model.firebase.RoadEvent
import com.main.application.model.firebase.Route
import com.main.application.model.firebase.RouteUpdate
import com.main.application.model.local.TurningPoint
import com.main.application.sensor.OrientationSensorActivity
import com.main.application.service.DirectionsServiceImpl
import com.main.application.service.firebase_database.FirebaseDatabase
import com.main.application.service.firebase_database.FirebaseDatabaseImpl
import com.main.application.utils.*
import org.koin.core.KoinComponent
import java.util.*

class MapsFragmentViewModel(application: Application) : AndroidViewModel(application), KoinComponent {

    val onShowSearchField = ObservableField<Boolean>(false)

    // todo: delete next line with map
    lateinit var map: GoogleMap

    // lastKnownLocation va fi locatia primita de la gps in MapFragment => cand se muta codul pe locatia de tip Location
    var lastKnownLocation = Location("default")
    var lastKnownLocationAsLatLng = LatLng(0.0, 0.0)
    private var currentRoadSide = ""
    private var traveledDistance = 0
    private var lastDistanceAnnouncedCurves = 0
    private var lastDistanceAnnouncedRoadEvents = 0
    private var lastDistanceAnnouncedCurveAndRoadEvent = 0
    var canClearMap = MutableLiveData<Boolean>()
    val canLogout = MutableLiveData<Boolean>()

    var polylineFirst = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var polylineLast = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var polylineRightPoints = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var polylineLeftPoints = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var polylineCenter = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var timerValue = true

    var roadEvents = mutableListOf<RoadEvent>()
    var nearestPointPerpendicular: LatLng? = null
    val destinationAsString = ObservableField<String>()
    val readyToDrawLineGoogle = MutableLiveData<Boolean>()
    val readyToDrawLineDefinedPath = MutableLiveData<Boolean>()
    val angleAtNextIntersection = MutableLiveData<Double>()
    val currentOrientation = MutableLiveData<Double>()
    var currentCurvePointIndex = 0
    var currentLocationIndex = 0
    var numberOfGpsReadings = 0
    var currentRoadEventIndex = 0
    var currentCurveDirection = "NCP"
    var currentCurveAngle = -1
    val readyToDrawPathLines = MutableLiveData<Boolean>()

    private var askForDistanceToNextCurvePoint = true
    val announceTotalDistance = MutableLiveData<Boolean>()
    var willArriveAtDestination = false
    var hasArrivedAtDestination = false
    var onceArriveAtDestination = true
    var roadList = mutableListOf<Pair<LatLng, AppConstants.Type>>()     // toate punctele (road events translatate pe perpendiculara)
    var turningPoints = mutableListOf<TurningPoint>()
    var roadObjects = mutableListOf<Pair<Any, AppConstants.Type>>()     // toate punctele (road events la locul lor initial)
    var currentMapResult = mutableListOf<List<LatLng>>()
    var currentDefinedPoints = listOf<LatLng>()
    // save points where request have been made
    val requestsMade = mutableListOf<LatLng>()
    var existsPath = MutableLiveData<Boolean>()
    val deviated: MutableLiveData<Boolean> = MutableLiveData()
    val announcedDistances = mutableListOf<Int>()

    private lateinit var startTime: Date
    private lateinit var endTime: Date
    private var totalDistance = 0.0
    private var currentRouteKey = ""
    private var pointOfInterestAsString = ""
    private lateinit var token: String
    private lateinit var userUID: String
    val mapService = DirectionsServiceImpl()
    private val requestUtils = RequestUtils(application.applicationContext)
    private val mapUtils = MapUtils(application)
    //private val mapUtils: MapUtils by inject()
    private val transformUtils = TransformationsUtils(application.applicationContext)
    private val orientationUtils = OrientationSensorActivity(application.applicationContext)
    private val textToSpeechUtils = TextToSpeechUtilsRO(application.applicationContext)
    private val commandsUtils = CommandsUtils()
    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabaseImpl()
    val loadProgressBar = MutableLiveData<Boolean>()


    init {
        loadProgressBar.value = true
        canLogout.value = false
        canClearMap.value = false
        angleAtNextIntersection.value = 0.0
        existsPath.value = false
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication()).second
        token = userAndToken.idToken!!
        userUID = userAndToken.uuid!!
    }

    private fun roundDistance(distance: Int): Int {
        when (distance.rem(10)) {
            1 -> return distance - 1
            2 -> return distance - 2
            3 -> return distance - 3
            4 -> return distance - 4
            5 -> return distance - 5
            6 -> return distance + 4
            7 -> return distance + 3
            8 -> return distance + 2
            9 -> return distance + 1
        }
        return distance
    }

    fun addMarkerOnLocation(map: GoogleMap, location: LatLng) {
        mapUtils.addMarkerOnLocation(map, location)
    }

    /**
     * When receives a command, this method will make the proper action
     */
    fun parseCommand(text: String) {
        val result = commandsUtils.parseCommand(text)
        if (result != null) {
            when (result.first) {
                GET_TO_DESTINATION_RO -> {
                    destinationAsString.set(result.second[0])
                    searchGoogleMapsDestinationWithSimpleOptions()
                }
                GET_ROUTE_RO -> {
                    getDefinedRoute(result.second[0], result.second[1])
                }
                SET_NEW_POINT_OF_INTEREST_RO -> {
                    pointOfInterestAsString = result.second[0]
                    checkPointOfInterest(pointOfInterestAsString)
                }
                DELETE_POINT_OF_INTEREST_RO -> {
                    pointOfInterestAsString = result.second[0]
                    deletePointOfInterest(pointOfInterestAsString)
                }
                GET_TO_POINT_OF_INTEREST_RO -> {
                    pointOfInterestAsString = result.second[0]
                    searchDestinationFromPOI()
                }
                GET_CURRENT_LOCATION_INFORMATION_RO -> {
                    getCurrentLocationInformation()
                }
                NEED_HELP_RO -> {
                    addHelpForPatient()
                }
                LOGOUT -> {
                    logout()
                }
            }
        } else {
            announceInexistentCommand()
        }
    }

    fun destroy() {
        textToSpeechUtils.stop()
    }

    /**
     * Checks if the user is on the right road (when press on map)
     * @param currentLocation current user's position
     */
    fun navigate(map: GoogleMap, currentLocation: LatLng) {
        lastKnownLocationAsLatLng = currentLocation
        this.map = map

        // salvez in firebase ultima locatie a pacientului dupa ce s-au efectuat un numar de NUMBER_OF_GPS_READINGS de la ultima salvare
        if (numberOfGpsReadings == AppConstants.NUMBER_OF_GPS_READINGS) {
            numberOfGpsReadings = 0
            val userAndToken = LocalStorageUtils.getUserAndToken(getApplication())
            val token = userAndToken.second.idToken!!
            val patientUid = userAndToken.second.uuid!!
            firebaseDatabase.addPatientLastKnownLocation(patientUid, token, lastKnownLocationAsLatLng, ::onSuccessSaveLastKnownLocation, ::onErrorSaveLastKnownLocation)
        }

        val nearestPointFromPathPoints = mapUtils.getNearestPoint(currentLocation)
        val perpendicularPoint = mapUtils.getNearestPointOnPath(map, LatLng(currentLocation.latitude, currentLocation.longitude))
        deviated.value = transformUtils.haversineDistance(currentLocation, perpendicularPoint) > AppConstants.DEVIATION_DISTANCE

        // deviceOrientation = first - angle with azimuth, second - road side
        //val deviceOrientation = mapUtils.getOrientation(currentLocation, nearestPointFromPathPoints.second, mapUtils.pathPoints)
        //val deviceOrientation = mapUtils.getOrientation(currentLocation, currentLocationIndex, mapUtils.pathPoints)
        val list = mutableListOf<LatLng>()
        for (x in this.roadList) {
            list.add(x.first)
        }
        val deviceOrientation = mapUtils.getOrientation(currentLocation, currentLocationIndex, this.roadList.asLatLngList())
        // incercam si cu roadList

        currentRoadSide = deviceOrientation.second
        currentOrientation.value = deviceOrientation.first

        // deviare de la traseu fata de perpendiculara pozitiei pe traseu
        if (deviated.value == false) {
            nearestPointPerpendicular = perpendicularPoint
        }
        angleAtNextIntersection.value = mapUtils.getAngleBetweenPointsOnPath(map, nearestPointFromPathPoints.first)
        mapUtils.map = map

        val sizeLeft = mapUtils.leftPoints.first.points.size - 1
        if (transformUtils.getDeterminant(mapUtils.leftPoints.first.points[sizeLeft], mapUtils.rightPoints.first.points[sizeLeft], currentLocation) >= 0 &&
                transformUtils.haversineDistance(currentLocation, mapUtils.centerPoints.first.points[sizeLeft]) <= AppConstants.CIRCLE_DISTANCE) {
            hasArrivedAtDestination = true
        }

        if (!hasArrivedAtDestination) {
            // road orientation
            // announce if user direction is not correct
            if (currentLocationIndex < polylineLeftPoints.first.points.size) {
                currentLocationIndex = mapUtils.getCurrentIndexOnPath(currentLocation, currentLocationIndex, polylineLeftPoints.first.points, polylineRightPoints.first.points)
            }
            if (currentLocationIndex >= polylineLeftPoints.first.points.size) {
                currentLocationIndex = mapUtils.getCurrentIndexOnPath(currentLocation, 0, polylineLeftPoints.first.points, polylineRightPoints.first.points)
            }
            if (currentRoadEventIndex == -1 && currentCurvePointIndex == -1) {
                willArriveAtDestination = true
                val distance = mapUtils.checkDistance(currentLocation, roadList[roadList.size - 1].first, announcedDistances)
                if (distance != -1)
                    //textToSpeechUtils.announceTurningDirection(distance, currentCurveDirection, currentCurveAngle)
                    textToSpeechUtils.announceWillArriveAtDestination(distance)
            }
            if (currentCurvePointIndex != -1) {
                val leftP = mapUtils.leftPoints.first.points[currentCurvePointIndex]
                val rightP = mapUtils.rightPoints.first.points[currentCurvePointIndex]
                val det = transformUtils.getDeterminant(leftP, rightP, currentLocation)
                if (det >= 0) {
                    announcedDistances.clear()
                    val nextCurvePair = mapUtils.getNextTurningPointIndex(this.roadList.asLatLngList(), map, currentCurvePointIndex)
                    currentCurvePointIndex = nextCurvePair[0] as Int
                    currentCurveDirection = nextCurvePair[1] as String
                    currentCurveAngle = nextCurvePair[3] as Int
                    // cand se schiimba urmatorul punct de cotitura pun timer => sa nu intre in comanda de verificare a directiei cand se schimba segmentul de drum dupa o cotitura
                    val timer = object : CountDownTimer(AppConstants.TIMER_VALUE_MEDIUM, 1000) {
                        override fun onFinish() {
                            timerValue = true
                        }
                        override fun onTick(millisUntilFinished: Long) {
                            timerValue = false
                        }
                    }
                    timer.start()
                }
            }
            if (currentRoadEventIndex != -1) {
                val leftP = mapUtils.leftPoints.first.points[currentRoadEventIndex]
                val rightP = mapUtils.rightPoints.first.points[currentRoadEventIndex]
                val det = transformUtils.getDeterminant(leftP, rightP, currentLocation)
                val dist = transformUtils.haversineDistance(currentLocation, roadList[currentRoadEventIndex].first)
                if (det >= 0 && dist < AppConstants.LONG_DISTANCE) {
                    announcedDistances.clear()
                    currentRoadEventIndex = mapUtils.getNextRoadEventIndex(this.roadList, currentLocation, currentRoadEventIndex)
                }
            }
            if (currentRoadEventIndex != -1 && currentCurvePointIndex != -1) {
                val dist = transformUtils.haversineDistance(roadList[currentRoadEventIndex].first, roadList[currentCurvePointIndex].first)
                // daca o trecere se afla aproape de o cotitura => se anunta impreuna in acelasi timp
                if (dist < AppConstants.SHORT_DISTANCE) {
                    val roadEventCoordinates = this.roadList[currentRoadEventIndex].first
                    val nextRoadEventObject = mapUtils.getRoadEvent(roadEventCoordinates, roadObjects.asRoadEvent())
                    if (nextRoadEventObject != null) {
                        val roundedDistance = mapUtils.checkDistance(currentLocation, roadList[currentRoadEventIndex].first, announcedDistances).toDouble()
                        if (roundedDistance != -1.0) {
                            //textToSpeechUtils.announceRoadEventAndCurve(nextRoadEventObject, roundedDistance, currentRoadSide)
                            textToSpeechUtils.announceRoadEventAndCurve(nextRoadEventObject, roundedDistance, currentCurveDirection)
                            val nextCurvePair = mapUtils.getNextTurningPointIndex(this.roadList.asLatLngList(), map, currentCurvePointIndex)
                            currentCurvePointIndex = nextCurvePair[0] as Int
                            currentCurveDirection = nextCurvePair[1] as String
                            currentCurveAngle = nextCurvePair[3] as Int
                            currentRoadEventIndex = mapUtils.getNextRoadEventIndex(this.roadList, currentLocation, currentRoadEventIndex)
                            // cand se schiimba urmatorul punct de cotitura pun timer => sa nu intre in comanda de verificare a directiei cand se schimba segmentul de drum dupa o cotitura
                            val timer = object : CountDownTimer(AppConstants.TIMER_VALUE_MEDIUM, 1000) {
                                override fun onFinish() {
                                    timerValue = true
                                }
                                override fun onTick(millisUntilFinished: Long) {
                                    timerValue = false
                                }
                            }
                            timer.start()
                        }
                    }
                } else
                if (currentCurvePointIndex < currentRoadEventIndex) {
                    println("AICI: ${currentRoadEventIndex} $currentCurvePointIndex $currentLocationIndex PRIMA CURBA")
                    // anunta curve point
                    val curvePoint = this.roadList[currentCurvePointIndex].first
                    //val distance = transformUtils.haversineDistance(curvePoint, currentLocation).toInt()
                    val distance = mapUtils.checkDistance(currentLocation, curvePoint, announcedDistances)
                    if (distance != -1)
                        textToSpeechUtils.announceTurningDirection(distance, currentCurveDirection, currentCurveAngle)
                } else {
                    println("AICI: ${currentRoadEventIndex} $currentCurvePointIndex $currentLocationIndex ROAD EVENT")
                    val roadEventCoordinates = this.roadList[currentRoadEventIndex].first
                    val nextRoadEventObject = mapUtils.getRoadEvent(roadEventCoordinates, mapUtils.roadObjects.asRoadEvent())
                    if (nextRoadEventObject != null) {
                        //val distanceToNextRoadEvent = transformUtils.haversineDistance(currentLocation, next RoadEventObject.asLatLng())
                        val distanceToNextRoadEvent = mapUtils.checkDistance(currentLocation, nextRoadEventObject.toLatLng(), announcedDistances)
                        if (distanceToNextRoadEvent != -1)
                            textToSpeechUtils.announceDistanceToRoadEvent(nextRoadEventObject, distanceToNextRoadEvent)
                    }
                }
            } else {
                if (currentCurvePointIndex != -1) {
                    println("AICI: ${currentRoadEventIndex} $currentCurvePointIndex $currentLocationIndex PRIMA CURBA")
                    val curvePoint = this.roadList[currentCurvePointIndex].first
                    //val distance = transformUtils.haversineDistance(curvePoint, currentLocation).toInt()
                    val distance = mapUtils.checkDistance(currentLocation, curvePoint, announcedDistances)
                    if (distance != -1)
                        textToSpeechUtils.announceTurningDirection(distance, currentCurveDirection, currentCurveAngle)
                }
                if (currentRoadEventIndex != -1 ) {
                    println("ROAD EVENT")
                    val roadEventCoordinates = this.roadList[currentRoadEventIndex].first
                    val nextRoadEventObject = mapUtils.getRoadEvent(roadEventCoordinates, mapUtils.roadObjects.asRoadEvent())
                    if (nextRoadEventObject != null) {
                        //val distanceToNextRoadEvent = transformUtils.haversineDistance(currentLocation, nextRoadEventObject.asLatLng())
                        val distanceToNextRoadEvent = mapUtils.checkDistance(currentLocation, nextRoadEventObject.toLatLng(), announcedDistances)
                        if (distanceToNextRoadEvent != -1)
                            textToSpeechUtils.announceDistanceToRoadEvent(nextRoadEventObject, distanceToNextRoadEvent)
                    }
                }
            }

            val userTurningDirection = mapUtils.azimuthAngleOfNearestPosition(currentLocationIndex, deviceOrientation.first, deviceOrientation.second, this.roadList.asLatLngList())
            // se anunta deviarea de la traseu
            if (userTurningDirection.first != Commands.FORWARD_EN && timerValue) {
                textToSpeechUtils.startingOrientation(userTurningDirection)
                // start timer
                val timer = object : CountDownTimer(AppConstants.TIMER_VALUE_LOW, 1000) {
                    override fun onFinish() {
                        timerValue = true
                    }
                    override fun onTick(millisUntilFinished: Long) {
                        timerValue = false
                    }
                }
                timer.start()
            }
        } else if (onceArriveAtDestination) {
            onceArriveAtDestination = false
            textToSpeechUtils.announceHasArrivedAtDestination()
            hasArrivedAtDestination()
        }

        println("AICI: FINAL $hasArrivedAtDestination $willArriveAtDestination $onceArriveAtDestination")
    }

    fun logout() {
        textToSpeechUtils.logout()
        LocalStorageUtils.removeAll(getApplication())
        canLogout.value = true
    }

    private fun RoadEvent.asLatLng(): LatLng {
        return LatLng(this.latitude, this.longitude)
    }

    private fun announceTotalDistanceAndTurningDirection(userTurningDirection: Pair<String?, Int>, roadSide: String?) {
        // se anunta o singura data la inceput distanta totala a traseului
        totalDistance = mapUtils.getTotalPathDistance()
        val nextCurvePoint = mapUtils.getNextTurningPointIndex( 0)
        lastDistanceAnnouncedCurves = roundDistance(transformUtils.haversineDistance(mapUtils.pathPoints[0], nextCurvePoint.third).toInt())
        textToSpeechUtils.totalDistance(totalDistance.toInt())

        if (roadSide != null) {
            textToSpeechUtils.roadSide(roadSide)
        }
        // se anunta deviarea de la traseu
        if (userTurningDirection.first != null) {
            textToSpeechUtils.startingOrientation(userTurningDirection)
            // start timer
            val timer = object : CountDownTimer(AppConstants.TIMER_VALUE_LOW, 1000) {
                override fun onFinish() {
                    timerValue = true
                }
                override fun onTick(millisUntilFinished: Long) {
                    timerValue = false
                }
            }
            timer.start()
        }
    }

    private fun addHelpForPatient() {
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication())
        val token = userAndToken.second.idToken!!
        val userUID = userAndToken.second.uuid!!
        firebaseDatabase.addHelpForPatient(userUID, token, ::onSuccessAddHelp, ::onErrorAddHelp)
    }

    private fun deleteLastKnownLocation() {
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication())
        val token = userAndToken.second.idToken!!
        val userUID = userAndToken.second.uuid!!
        firebaseDatabase.deletePatientLastKnownLocation(userUID, token, ::onSuccessDeleteLastKnownLocation, ::onErrorDeleteLastKnownLocation)
    }

    private fun announceInexistentCommand() {
        textToSpeechUtils.inexistentCommand()
    }

    fun navigate(map: GoogleMap, currentLocation: Location) {
        val nearestLocation = mapUtils.getNearestLocationFromPathPoints(
            map,
            LatLng(currentLocation.latitude, currentLocation.longitude)
        )
        val perpendicularPoint = mapUtils.getNearestPointOnPath(
            map,
            LatLng(currentLocation.latitude, currentLocation.longitude)
        )
        deviated.value = transformUtils.haversineDistance(
            LatLng(
                currentLocation.latitude,
                currentLocation.longitude
            ), perpendicularPoint
        ) > AppConstants.DEVIATION_DISTANCE
        if (deviated.value == false) {
            nearestPointPerpendicular = nearestLocation
        }

        angleAtNextIntersection.value = mapUtils.getAngleBetweenPointsOnPath(map, nearestLocation)
    }

    /**
     * When user arrive at destination, this method will be called to save time, distance, etc
     */
    private fun hasArrivedAtDestination() {
        endTime = Date()
        val totalTimeInSeconds = ((endTime.time - startTime.time) / 1000).toInt()
        val route = RouteUpdate()
        route.totalTime = totalTimeInSeconds
        route.distance = totalDistance.toInt()
        route.averageSpeed = (totalDistance / totalTimeInSeconds).toFloat()
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication()).second
        val token = userAndToken.idToken!!
        val userUID = userAndToken.uuid!!
        firebaseDatabase.updateRoute(userUID, route, currentRouteKey, token, ::onSuccessUpdateRoute, ::onErrorUpdateRoute)
    }

    private fun onSuccessUpdateRoute(response: JsonObject) {
        Log.i("Success", "Update route with success!")
    }

    private fun onErrorUpdateRoute(error: Int) {
        Log.i("Error", "Update route with error. Code: $error")
    }

    fun drawPathFromJson(drawPolyline: (List<List<LatLng>>, PolylineOptions) -> Unit, drawPoint: (LatLng, String) -> Unit, draw: (List<LatLng>, PolylineOptions) -> Unit) {
        mapUtils.drawPathFromJson(currentMapResult, drawPolyline, drawPoint, draw)
    }

    fun drawPathFromPoints(drawPolyline: (List<List<LatLng>>, PolylineOptions) -> Unit, drawPoint: (LatLng, String) -> Unit, draw: (List<LatLng>, PolylineOptions) -> Unit) {
        mapUtils.drawPathFromList(currentDefinedPoints, drawPolyline, drawPoint, draw)
    }

    /**
     * Make a request for a route to Google Maps.
     */
    fun searchGoogleMapsDestinationWithSimpleOptions() {
        totalDistance = 0.0
        onceArriveAtDestination = true
        mapUtils.resetAll()
        currentCurvePointIndex = 0
        currentLocationIndex = 0
        canClearMap.value = true
        if (destinationAsString.get().isNullOrEmpty()) {
        } else {
            lastKnownLocationAsLatLng = LatLng(lastKnownLocation.latitude, lastKnownLocation.longitude)
            val options = requestUtils.getOptimizedDirectionsRequest(
                lastKnownLocation,
                destinationAsString.get()!!
            )
            mapService.getDirections(options, ::onSuccessSearchDestination, ::onErrorSearchDestination)
        }
    }

    /**
     * Search destination in data base
     */
    fun getDefinedRoute(back: String, name: String) {
        totalDistance = 0.0
        onceArriveAtDestination = true
        mapUtils.resetAll()
        currentCurvePointIndex = 0
        currentLocationIndex = 0
        canClearMap.value = true
        lastKnownLocationAsLatLng = LatLng(lastKnownLocation.latitude, lastKnownLocation.longitude)

        if (name.isNotEmpty()) {
            if (back == BACK_RO) {
//                val newName = name.substring(8, name.length - 1)
                firebaseDatabase.getPatientDefinedRouteByName(token, userUID, name, false, ::onSuccessGetDefinedRoute, ::onErrorGetDefinedRoute)
            } else
                firebaseDatabase.getPatientDefinedRouteByName(token, userUID, name, true, ::onSuccessGetDefinedRoute, ::onErrorGetDefinedRoute)
        }
    }

    /**
     * Search destination from data base
     */
    private fun onSuccessGetDefinedRoute(points: List<LatLng>) {
        Log.i("Success", "Search defined route with success!")
        currentDefinedPoints = points.toList()
        readyToDrawLineDefinedPath.value = true
        this.roadList.clear()
        this.turningPoints.clear()
        for (i in 0 until mapUtils.pathPoints.size) {
            this.roadList.add(Pair(mapUtils.pathPoints[i], AppConstants.Type.POINT))
        }

        // se anunta in ce parte sa o ia userul la inceput; userTurningDirection = directia in care trebuie sa o ia utilizatorul
        val nearestPointFromPathPoints = mapUtils.getNearestPoint(lastKnownLocationAsLatLng)
        val deviceOrientation = mapUtils.getOrientation(lastKnownLocationAsLatLng, nearestPointFromPathPoints.second, mapUtils.pathPoints)
        val userTurningDirection = mapUtils.azimuthAngleOfNearestPosition(nearestPointFromPathPoints.second, deviceOrientation.first, deviceOrientation.second)
        currentRoadSide = deviceOrientation.second
        announceTotalDistanceAndTurningDirection(userTurningDirection, currentRoadSide)
        existsPath.value = true
    }

    /**
     * Search destination from data base
     */
    private fun onErrorGetDefinedRoute(error: Int) {
        Log.e("Error", "Get defined route with error. Error code: $error")
    }

    /**
     * Search destination using Google service
     */
    private fun onSuccessSearchDestination(result: MutableList<List<LatLng>>) {
        Log.i("Success", "Search destination with success!")
        currentMapResult = result
        readyToDrawLineGoogle.value = true
        this.roadList.clear()
        this.turningPoints.clear()
        for (i in 0 until mapUtils.pathPoints.size) {
            this.roadList.add(Pair(mapUtils.pathPoints[i], AppConstants.Type.POINT))
        }

        // se anunta in ce parte sa o ia userul la inceput; userTurningDirection = directia in care trebuie sa o ia utilizatorul
        val nearestPointFromPathPoints = mapUtils.getNearestPoint(lastKnownLocationAsLatLng)
        val deviceOrientation = mapUtils.getOrientation(lastKnownLocationAsLatLng, nearestPointFromPathPoints.second, mapUtils.pathPoints)
        val userTurningDirection = mapUtils.azimuthAngleOfNearestPosition(nearestPointFromPathPoints.second, deviceOrientation.first, deviceOrientation.second)
        currentRoadSide = deviceOrientation.second
        announceTotalDistanceAndTurningDirection(userTurningDirection, currentRoadSide)
        existsPath.value = true

        // add new route in firebase
        val lastKnownLocationAsLatLng = LatLng(lastKnownLocation.latitude, lastKnownLocation.longitude)
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication())
        val userUID = userAndToken.second.uuid!!
        startTime = Date()
        val newRoute = Route(
            lastKnownLocationAsLatLng,
            mapUtils.getDestinationAsLatLng(),
            startTime
        )
        firebaseDatabase.addNewRoute(userUID, newRoute, userAndToken.second.idToken!!, ::onSuccessAddRoute, ::onErrorAddRoute)
    }

    private fun onSuccessAddRoute(routeKeyAsJson: JsonObject) {
        Log.i("Success", "Add route with success!")
        currentRouteKey = Gson().fromJson(routeKeyAsJson, RouteKey::class.java).name
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication())
        val token = userAndToken.second.idToken!!
        val patientUid = userAndToken.second.uuid!!
        // la inceputul fiecarui traseu salvez locatia initiala a pacientului
        firebaseDatabase.addPatientLastKnownLocation(patientUid, token, lastKnownLocationAsLatLng, ::onSuccessSaveLastKnownLocation, ::onErrorSaveLastKnownLocation)
    }

    private fun onErrorAddRoute(error: Int) {
        Log.e("Error", "Add route with error. Code: $error")
    }


    /**
     * Search destination from Google service
     */
    private fun onErrorSearchDestination(error: Int) {
        Log.i("Error", "Error search destination. Error code: $error")
        when (error) {
            1 -> textToSpeechUtils.locationIsNull()
        }
    }

    /**
     * Method will save in firebase database new point of interest
     * @param locationName the name of current location as String (ex. "home")
     */
    fun savePointOfInterest(locationName: String) {
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication()).second
        val userUID = userAndToken.uuid!!
        val lastKnownLocationAsLatLng = LatLng(lastKnownLocation.latitude, lastKnownLocation.longitude)
        val newPointOfInterest = PointOfInterest(
            lastKnownLocationAsLatLng,
            locationName.toLowerCase()
        )
        firebaseDatabase.addNewPointOfInterest(userUID, newPointOfInterest, userAndToken.idToken!!, ::onSuccessSavePOI, ::onErrorSavePOI)
    }

    private fun onSuccessSavePOI(response: JsonObject) {
        Log.i("Success", "Point of interest added with success!")
        textToSpeechUtils.poiSet(pointOfInterestAsString)
    }

    private fun onErrorSavePOI(error: Int) {
        Log.e("Error", "Point of interest added with error. Error code: $error")
    }

    /**
     * Method will check if asked location already exists in database. If it exists, it will not be saved, otherwise it will.
     * @param locationName the name of current location as String
     */
    fun checkPointOfInterest(locationName: String) {
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication()).second
        val token = userAndToken.idToken!!
        val userUID = userAndToken.uuid!!
        firebaseDatabase.getPointOfInterestByName(userUID, locationName.toLowerCase(), token, ::onSuccessGetPOI, ::onErrorGetPOI)
    }

    private fun onSuccessGetPOI(response: JsonObject) {
        Log.i("Success", "Get point of interest with success!")

        // daca POI nu exista deja in baza de date => il adaug
        // daca exista => anunt print-un mesaj ca exista deja
        if (response.entrySet().size == 0) {
            savePointOfInterest(pointOfInterestAsString)
        } else {
            textToSpeechUtils.poiAlreadyExists(pointOfInterestAsString)
        }
    }

    private fun onErrorGetPOI(error: Int) {
        Log.e("Error", "Get point of interest with Error. Error code: $error")
    }

    fun deletePointOfInterest(locationName: String) {
        val userAndToken = LocalStorageUtils.getUserAndToken(getApplication()).second
        val token = userAndToken.idToken!!
        val userUID = userAndToken.uuid!!
        firebaseDatabase.deletePointOfInterestByKey(userUID, locationName.toLowerCase(), token, ::onSuccessDeletePOI, ::onErrorDeletePOI)
    }

    private fun onSuccessDeletePOI() {
        Log.i("Success", "Delete point of interest with success!")
        textToSpeechUtils.poiDeleted(pointOfInterestAsString)
    }

    private fun onErrorDeletePOI(error: Int) {
        Log.e("Error", "Delete point of interest with error. Error code: $error")
        if (error == 1) {
            textToSpeechUtils.poiNotExist(pointOfInterestAsString)
        }
    }

    /**
     * Method will search a path from current location to a point of interest
     */
    fun searchDestinationFromPOI() {
        if (pointOfInterestAsString.isNotEmpty()) {
            val userAndToken = LocalStorageUtils.getUserAndToken(getApplication()).second
            val token = userAndToken.idToken!!
            val userUID = userAndToken.uuid!!
            firebaseDatabase.getPointOfInterestByName(userUID, pointOfInterestAsString.toLowerCase(), token, ::onSuccessFindPOI, ::onErrorFindPOI)
        }
    }

    fun getRoadEvents(drawRouteLimits: (LatLng, LatLng, LatLng, LatLng) -> Unit, setMarker: (List<RoadEvent>) -> Unit) {
        val roadSquare = mapUtils.getMinMaxLatLon(drawRouteLimits)
        firebaseDatabase.getRoadEventsFromInterval(token, roadSquare, setMarker, ::onSuccessGetRoadEvents, ::onErrorGetRoadEvents)
    }

    private fun onSuccessGetRoadEvents(setMarker: (List<RoadEvent>) -> Unit, roadEvents: List<RoadEvent>) {
        Log.i("Success", "Get road events with success!")
        startTime = Date()
        hasArrivedAtDestination = false
        willArriveAtDestination = false
        onceArriveAtDestination = true
        val filteredRoadEvents = mapUtils.filterPointsBySurroundingLines(roadEvents, mapUtils.centerPoints.first, mapUtils.leftPoints.first, mapUtils.rightPoints.first, setMarker) as MutableList
        val sortedFilteredRoadEvents = mapUtils.sortByDistance(filteredRoadEvents)
        roadList = mapUtils.addElements(roadList, sortedFilteredRoadEvents, AppConstants.Type.ROAD_EVENT).toMutableList()
        this.roadEvents = (sortedFilteredRoadEvents as MutableList).toMutableList()
        currentRoadEventIndex = 0
        this.polylineCenter = mapUtils.centerPoints
        this.polylineLeftPoints = mapUtils.leftPoints
        this.polylineRightPoints = mapUtils.rightPoints
        this.polylineFirst = mapUtils.firstPoints
        this.polylineLast = mapUtils.lastPoints
        this.turningPoints = mapUtils.getTurningPoints(mapUtils.pathPoints).toMutableList()
        this.roadObjects = mapUtils.roadObjects.toMutableList()
        readyToDrawPathLines. value = true
        if (mapUtils.roadEvents.size > 0) {
            val nextRoadEvent = LatLng(mapUtils.roadEvents[0].latitude, mapUtils.roadEvents[0].longitude)
            lastDistanceAnnouncedRoadEvents = roundDistance(transformUtils.haversineDistance(mapUtils.pathPoints[0], nextRoadEvent).toInt())
        }
    }

    private fun List<Pair<Any, AppConstants.Type>>.asRoadEvent(): List<RoadEvent> {
        val newList = mutableListOf<RoadEvent>()
        for (any in this) {
            if (any.second == AppConstants.Type.ROAD_EVENT) {
                val el = any.first as RoadEvent
                newList.add(el)
            }
        }
        return newList
    }

    private fun List<RoadEvent>.asLatLng(): MutableList<LatLng> {
        val newList = mutableListOf<LatLng>()
        for (i in this.indices) {
            newList.add(LatLng(this[i].latitude, this[i].longitude))
        }
        return newList
    }

//    private fun List<LatLng>.asRoadEvent(): MutableList<RoadEvent> {
//        val newList = mutableListOf<RoadEvent>()
//        for (x in this) {
//            newList.add(RoadEvent(AppConstants.RoadEventsCategory.SIMPLE_CROSSING, x.latitude, x.longitude, ""))
//        }
//        return newList
//    }

    private fun onErrorGetRoadEvents(error: Int) {
        Log.e("Error", "Get road events with error. Error code: $error")
    }

    private fun onSuccessFindPOI(response: JsonObject) {
        Log.i("Success", "Find point of interest with success!")
        if (response.entrySet().size > 0) {
            val poiAsJson = response.entrySet().first().value
            val pointOfInterest = Gson().fromJson<PointOfInterest>(poiAsJson, PointOfInterest::class.java)
            val destinationAsLatLng = pointOfInterest.location
            if (destinationAsLatLng != null) {
                val options = requestUtils.getOptimizedDirectionsRequest(lastKnownLocation, destinationAsLatLng)
                mapService.getDirections(options, ::onSuccessSearchDestination, ::onErrorFindPathFromPOI)
            }
        } else {
            textToSpeechUtils.poiNotExist(pointOfInterestAsString)
        }
    }

    private fun onErrorFindPOI(error: Int) {
        Log.e("Error", "Find point of interest with error. Error code: $error")
    }

    private fun onErrorFindPathFromPOI(error: Int) {
        Log.e("Error", "Error find path from point of interest. Error code: $error")
    }

    private fun getCurrentLocationInformation() {
        val options = requestUtils.getCurrentLocationRequest(lastKnownLocation)
        mapService.getCurrentLocation(options, ::onSuccessGetCurrentLocationInfo, ::onErrorGetCurrentLocationInfo)
    }

    private fun onSuccessGetCurrentLocationInfo(result: JsonObject) {
        Log.i("Success", "Get current location information with success!")
        // adresa curenta
        val currentAddress = JsonUtils.parseAddressRO(result)
        textToSpeechUtils.currentAddress(currentAddress, currentRoadSide)
        // partea in care sa se intoarca utilizatorul pentru a-si continua deplasarea
        if (existsPath.value!!) {
            val nearestPointFromPathPoints = mapUtils.getNearestPoint(lastKnownLocationAsLatLng)
            val deviceOrientation = mapUtils.getOrientation(lastKnownLocationAsLatLng, nearestPointFromPathPoints.second, mapUtils.pathPoints)
            val userTurningDirection = mapUtils.azimuthAngleOfNearestPosition(currentLocationIndex, deviceOrientation.first, deviceOrientation.second, this.roadList.asLatLngList())
            textToSpeechUtils.turningDirectionWhileOnRoad(userTurningDirection.first)
            //announceTurningPartWhileOnRoad(userTurningDirection.first)
        }
    }

    private fun List<Pair<LatLng, AppConstants.Type>>.asLatLngList(): List<LatLng> {
        val newList = mutableListOf<LatLng>()
        for (x in this) {
            newList.add(x.first)
        }
        return newList.toMutableList()
    }

    private fun onErrorGetCurrentLocationInfo() {
        Log.e("Error", "Get current location information with error!")
    }

    private fun onSuccessSaveLastKnownLocation() {
        Log.i("Success", "Save patient's last known location with success!")
    }

    private fun onErrorSaveLastKnownLocation(error: Int) {
        Log.e("Error", "Save patient's last known location with error. Error code: $error")
    }

    private fun onSuccessAddHelp() {
        Log.i("Success", "Add help for a patient with success!")
    }

    private fun onErrorAddHelp(error: Int) {
        Log.e("Error", "Add help for a patient with error. Error code: $error")
    }

    private fun onSuccessDeleteLastKnownLocation() {
        Log.i("Success", "Delete last known location with success!")
    }

    private fun onErrorDeleteLastKnownLocation(error: Int) {
        Log.e("Error", "Delete last known location with error. Error code; $error")
    }

    fun unregisterSensors() {
        orientationUtils.unregisterSensors()
    }

    fun registerSensors() {
        orientationUtils.registerSensors()
    }

    private fun onSuccessOsm(result: String) {
        println("RESPONSE: $result")
    }

    private inner class RouteKey {
        val name: String = ""
    }
}