package com.main.application.adapters

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.navigation.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

@BindingAdapter("onNavigate")
fun navigateToFragment(view: View, fragment: Int) {
    view.setOnClickListener {
        it.findNavController().navigate(fragment)
    }
}

@BindingAdapter(value = ["onSwipeToRefresh", "onSwipeToExecute"], requireAll = false)
fun onSwipeToRefresh(view: SwipeRefreshLayout, swipeToRefresh: Boolean, executeWhenSwipe: () -> Unit) {
    view.isRefreshing = swipeToRefresh
    if (view.isRefreshing) {
        executeWhenSwipe()
    }
}

@BindingAdapter("onFabTap")
fun onFabTap(view: View, execute: () -> Unit) {
    view.setOnClickListener {
        execute()
    }
}

@BindingAdapter("onActionTap")
fun onActionTap(view: View, action: () -> Unit) {
    view.setOnClickListener {
        action()
    }
}

@BindingAdapter(value = ["onActionLogout", "onNavigateToLogin"], requireAll = false)
fun onLogoutTap(view: View, action: () -> Unit, fragment: Int) {
    view.setOnClickListener {
        action()
        it.findNavController().navigate(fragment)
    }
}