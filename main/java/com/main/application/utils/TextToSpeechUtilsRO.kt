package com.main.application.utils

import android.content.Context
import android.speech.tts.TextToSpeech
import com.main.application.constants.AppConstants
import com.main.application.constants.Commands
import com.main.application.model.firebase.RoadEvent
import java.util.*

class TextToSpeechUtilsRO(context: Context) {

    private lateinit var textToSpeech: TextToSpeech

    init {
        textToSpeech = TextToSpeech(context, TextToSpeech.OnInitListener { status ->
            if (status != TextToSpeech.ERROR) {
                textToSpeech.language = Locale("ro-RO")
            }
        })
    }

    fun stop() {
        textToSpeech.shutdown()
    }

    fun announceHasArrivedAtDestination() {
        activateTextToSpeech("Ați ajuns la destinație. Se așteaptă alte comenzi.")
    }

    fun announceWillArriveAtDestination(meters: Int) {
        activateTextToSpeech("În $meters metrii veți ajunge la destinație.")
    }

    fun announceTurningDirection(meters: Int, direction: String, angle: Int) {
        //var text = "În $meters metrii trebuie să o luați"
        //var text = "În $meters metrii urmează o cotitură la $direction de $angle grade la care o luați"
        var text = "În $meters metrii urmează o cotitură" //la $direction de $angle grade la care o luați"
        if (angle in 0..AppConstants.LIGHT_TURNING_POINT.toInt())
            text += " spre $direction de $angle grade."
        if (angle in AppConstants.LIGHT_TURNING_POINT.toInt()..AppConstants.ANGLE_CURVE.toInt())
            text += " ușoară, spre $direction de $angle grade."
        activateTextToSpeech(text)
    }

    fun totalDistance(meters: Int) {
        activateTextToSpeech("Distanța traseului este de aproximativ $meters metrii.")
    }

    fun roadSide(roadSide: String) {
        activateTextToSpeech("Momentan vă aflați pe partea $roadSide a drumului.")
    }

    fun startingOrientation(userTurningDirection: Pair<String?, Int>) {
        var angle = userTurningDirection.second
        if (userTurningDirection.second > 180)
            angle = 360 - userTurningDirection.second
        if (userTurningDirection.first != null) {
            when (userTurningDirection.first) {
                Commands.LEFT_EN -> activateTextToSpeech("Direcție incorectă, întoarceți-vă spre stânga $angle grade.")
                Commands.RIGHT_EN -> activateTextToSpeech("Direcție incorectă, întoarceți-vă spre dreapta $angle grade.")
                Commands.FORWARD_EN -> activateTextToSpeech("Direcție corectă, mergeți înainte.")
                Commands.BACK_EN -> {
                    if (userTurningDirection.second > 180)
                        activateTextToSpeech("Direcție incorectă, întoarceți-vă înapoi prin partea dreaptă $angle grade.")
                    else
                        activateTextToSpeech("Direcție incorectă, întoarceți-vă înapoi prin partea stângă $angle grade.")
                }
            }
        }
    }

    fun turningDirectionWhileOnRoad(userTurningDirection: String?) {
        when (userTurningDirection) {
            Commands.LEFT_EN -> activateTextToSpeech("Pentru a continua deplasarea întoarceți-vă spre stânga.")
            Commands.RIGHT_EN -> activateTextToSpeech("Pentru a continua deplasarea întoarceți-vă spre dreapta.")
            Commands.FORWARD_EN -> activateTextToSpeech("Pentru a continua deplasarea mergeți înainte.")
            Commands.BACK_EN -> activateTextToSpeech("Pentru a continua deplasarea întoarceți-vă înapoi.")
        }
    }

    fun announceDistanceToRoadEvent(roadEvent: RoadEvent, distanceToNextRoadEvent: Int) {
        when (roadEvent.category) {
            AppConstants.RoadEventsCategory.SIMPLE_CROSSING -> activateTextToSpeech("În ${distanceToNextRoadEvent.toInt()} metrii urmează o trecere de pietoni nesemaforizată.")
            AppConstants.RoadEventsCategory.TRAFFIC_LIGHT_CROSSING-> activateTextToSpeech("În ${distanceToNextRoadEvent.toInt()} metrii urmează o trecere de pietoni semaforizată.")
            AppConstants.RoadEventsCategory.TRAFFIC_LIGHT_GROUP_CROSSING -> activateTextToSpeech("În ${distanceToNextRoadEvent.toInt()} metrii urmează o intersecție cu mai multe treceri de pietoni semaforizate.")
        }
    }

    fun announceRoadEventAndCurve(roadEvent: RoadEvent, distanceToNextCurve: Double, roadSide: String) {
        when (roadEvent.category) {
            AppConstants.RoadEventsCategory.SIMPLE_CROSSING -> activateTextToSpeech("În ${distanceToNextCurve.toInt()} metrii urmează o intersecție cu o trecere de pietoni nesemaforizată și o cotitură spre $roadSide.")
            AppConstants.RoadEventsCategory.TRAFFIC_LIGHT_CROSSING-> activateTextToSpeech("În ${distanceToNextCurve.toInt()} metrii urmează o trecere de pietoni semaforizată și o cotitură spre $roadSide.")
            AppConstants.RoadEventsCategory.TRAFFIC_LIGHT_GROUP_CROSSING -> activateTextToSpeech("În ${distanceToNextCurve.toInt()} metrii urmează mai multe treceri de pietoni semaforizate și o cotitură spre $roadSide.")
        }
    }

    fun locationIsNull() {
        activateTextToSpeech("Locația nu a fost găsită.")
    }

    // POINTS OF INTEREST

    fun poiSet(pointOfInterestAsString: String) {
        activateTextToSpeech("Punctul de interes, $pointOfInterestAsString, a fost salvat.")
    }

    fun poiAlreadyExists(pointOfInterestAsString: String) {
        activateTextToSpeech("Punctul de interes, $pointOfInterestAsString, există.")
    }

    fun poiNotExist(pointOfInterestAsString: String) {
        activateTextToSpeech("Punctul de interes, $pointOfInterestAsString, nu există.")
    }

    fun poiDeleted(pointOfInterestAsString: String) {
        activateTextToSpeech("Punctul de interes, $pointOfInterestAsString, a fost șters.")
    }

    // ADDRESS

    fun currentAddress(currentAddress: String, roadSide: String) {
        if (roadSide.isEmpty()) {
            activateTextToSpeech("Momentan vă aflați pe $currentAddress.")
        } else {
            activateTextToSpeech("Momentan vă aflați pe $currentAddress, pe partea $roadSide a drumului.")
        }
    }

    fun logout() {
        activateTextToSpeech("Proces logout.")
    }

    // INEXISTENT COMMAND

    fun inexistentCommand() {
        activateTextToSpeech("Această comandă nu există.")
    }
}