package com.main.application.utils

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.main.application.constants.AppConstants
import com.main.application.constants.Commands.BACK_EN
import com.main.application.constants.Commands.FORWARD_EN
import com.main.application.constants.Commands.LEFT_EN
import com.main.application.constants.Commands.LEFT_RO
import com.main.application.constants.Commands.RIGHT_EN
import com.main.application.constants.Commands.RIGHT_RO
import com.main.application.sensor.OrientationSensorActivity
import kotlin.math.*

class TransformationsUtils(val context: Context) {

    //private val orientationSensor: OrientationSensorActivity by inject()
    private val orientationSensor: OrientationSensorActivity = OrientationSensorActivity(context)
    private val earthRadius = 6378.137


    /**
     * Get road side relative to device orientation
     * @param currentPoint C
     * @param nearestPoint A
     * @param nextNearestPoint B - nearestPoint + 1
     * @return a pair with: azimuth angle, road side
     */
    fun getRoadSide(currentPoint: LatLng, nearestPoint: LatLng, nextNearestPoint: LatLng): Pair<Double, String> {
        val determinant = getDeterminant(nearestPoint, nextNearestPoint, currentPoint)
        return if (determinant > 0) {
            Pair(orientationSensor.mAzimuth, LEFT_RO)
        } else {
            Pair(orientationSensor.mAzimuth, RIGHT_RO)
        }
    }

    /**
     * This method will be used for the path of the user, using the last 2 coordinates from the path
     * @return true only if the points are on different lines and angle between lines is grater than 30 degrees
     */
    fun isCurvePointForUserLocation(
        pastPerpendicularPoint: LatLng,
        currentPerpendicularPoint: LatLng,
        nearestPoint: LatLng
    ): Boolean {
        val slopeCA = slope(currentPerpendicularPoint, nearestPoint)
        val slopeBA = slope(pastPerpendicularPoint, nearestPoint)
        return slopeBA != slopeCA && angleBetween(slopeBA, slopeCA) > 30.0
    }

    /**
     * This method will check if the middle point is a curve. If it is a curve point, it will return the side of the curve,
     * consistent to the direction of the segment [currentPoint, middlePoint]
     * @param currentPoint
     * @param middlePoint
     * @param nextPoint
     * @return angle and direction of the curve - left, right or NCP (not curve point)
     */
    fun isTurningPoint(
        currentPoint: LatLng,
        middlePoint: LatLng,
        nextPoint: LatLng
    ): Pair<Int, String> {
        // C - currentPoint, A - middlePoint, B - next point
        val azimuthCM = angleSegmentAzimuth(currentPoint, middlePoint, 0)
        val azimuthMN = angleSegmentAzimuth(middlePoint, nextPoint, 0)
        val angle = abs(180.0 - abs(azimuthCM - azimuthMN))
        //val angle = abs(azimuthCM - azimuthMN)

        if (angle > 0 && angle < AppConstants.ANGLE_CURVE) {
            val eq = segmentEquation(currentPoint, nextPoint)
            val distanceToSegment = abs(eq.first * middlePoint.longitude + eq.second * middlePoint.latitude - eq.third) / sqrt(eq.first * eq.first + eq.second * eq.second)
            //if (distanceToSegment >= AppConstants.ROAD_DISTANCE / 4) {
                val determinant = getDeterminant(currentPoint, middlePoint, nextPoint)
                return if (determinant < 0)
                    Pair(angle.toInt(), "Right")
                else
                    Pair(angle.toInt(), "Left")
            //}
        }
        return Pair(angle.toInt(), "NCP")
    }

    /**
     * @param currentPoint base point of the segment
     * @param nextPoint next point
     * @return the angle between azimuth and segment made of those 2 points
     */
    fun angleSegmentAzimuth(currentPoint: LatLng, nextPoint: LatLng, x: Int): Double {
        // will find the angle between Ox and segment made of those points =>
        // the angle will be 90 - alfa
        // angle of Ox = 0
        val slope = slope(currentPoint, nextPoint)

        // se translateaza urmatorul punct fata de punctul de baza pentru a afla in ce cadran este =>
        // se afla in ce directie e orientata dreapta pentru calcularea unghiului
        val translatedNextPoint = translateRelativeToPointInOrigin(nextPoint, currentPoint)

        when (inCadran(translatedNextPoint)) {
            1 -> {
                if (x == 1)
                return 90.0 - angleBetween(0.0, slope)
            }
            2 -> {
                if (x == 1)
                return 90.0 + angleBetween(0.0, slope)
            }
            3 -> {
                if (x == 1)
                return 270.0 - angleBetween(0.0, slope)
            }
            4 -> {
                if (x == 1)
                return 270.0 + angleBetween(0.0, slope)
            }
        }
        return angleBetween(0.0, slope)
    }

    /**
     * This method computes the orientation of device with respect to the road.
     * @param deviceOrientation - angle made by device with azimuth
     * @param segmentOrientation - angle made by road segment with azimuth
     * @param roadSide left or right
     * @return a String with device orientation (Left, Right, Back, Forward)
     */
    fun getOrientationWithRoad(deviceOrientation: Double, segmentOrientation: Double, roadSide: String, pos: Int): Pair<String, Int> {
        val angleBetween: Double    // unghiul mic (< 180) format intre directia segmentului si directia dispozitivului

        if (deviceOrientation <= segmentOrientation) {
            angleBetween = deviceOrientation + 360.0 - segmentOrientation
        } else {
            angleBetween = deviceOrientation - segmentOrientation
        }

        //println("ORIENTATION: ${deviceOrientation.toInt()}  ${segmentOrientation.toInt()}   ${angleBetween.toInt()}   ${roadSide} $pos")

        if (angleBetween >= 45.0 && angleBetween < 135.0) {
            //Toast.makeText(context, "${angleBetween.toInt()}\n${segmentOrientation.toInt()}\n$pos\n$roadSide-Left", Toast.LENGTH_SHORT).show()
            return Pair(LEFT_EN, angleBetween.toInt())
        }
        if (angleBetween >= 135.0 && angleBetween < 225.0) {
            //Toast.makeText(context, "${angleBetween.toInt()}\n${segmentOrientation.toInt()}\n$pos\n$roadSide-Back", Toast.LENGTH_SHORT).show()
            return Pair(BACK_EN, angleBetween.toInt())
        }
        if (angleBetween >= 225.0 && angleBetween < 315.0) {
            //Toast.makeText(context, "${angleBetween.toInt()}\n${segmentOrientation.toInt()}\n$pos\n$roadSide-Right", Toast.LENGTH_SHORT).show()
            return Pair(RIGHT_EN, angleBetween.toInt())
        }
        if (angleBetween >= 315.0 && angleBetween < 360.0 || angleBetween >= 0 && angleBetween < 45.0) {
            //Toast.makeText(context, "${angleBetween.toInt()}\n${segmentOrientation.toInt()}\n$pos\n$roadSide-Forward", Toast.LENGTH_SHORT).show()
            return Pair(FORWARD_EN, angleBetween.toInt())
        }
        return Pair("", -1)
    }

    /**
     * @return the perpendicular point on the path (D)
     */
    fun getPerpendicularPoint(
        currentPoint: LatLng,
        nearestPoint: LatLng,
        nextNearestPoint: LatLng
    ): LatLng {
        val slopeAB = slope(nearestPoint, nextNearestPoint)
        val slopeCD = -1 / slopeAB
        val equationAB = segmentEquation(nearestPoint, slopeAB)
        val equationCD = segmentEquation(currentPoint, slopeCD)
        val perpendicularPoint = getEquationResult(equationAB, equationCD)
        return LatLng(perpendicularPoint.second, perpendicularPoint.first)
    }

    /**
     * Calculate the result of 1st degree equation with 2 unknown terms;
     * Uses Cramer's Rule;
     * Parameters form: ax + by = c
     * @param equation1 triple with parameters of first equation
     * @param equation2 triple with parameters of second equation
     * @return a pair of (x, y)
     */
    fun getEquationResult(
        equation1: Triple<Double, Double, Double>,
        equation2: Triple<Double, Double, Double>
    ): Pair<Double, Double> {
        val globalDet = getDeterminant(
            Pair(equation1.first, equation1.second),
            Pair(equation2.first, equation2.second)
        )
        val detX = getDeterminant(
            Pair(equation1.third, equation1.second),
            Pair(equation2.third, equation2.second)
        )
        val detY = getDeterminant(
            Pair(equation1.first, equation1.third),
            Pair(equation2.first, equation2.third)
        )
        val x = detX / globalDet
        val y = detY / globalDet
        return Pair(x, y)
    }

    fun getDeterminant(A: Pair<Double, Double>, B: Pair<Double, Double>) =
        (A.first * B.second) - (A.second * B.first)

    /**
     * Calculate determinant of 3 points (used to find on which part is a point relative to a segment)
     * @param A BASE POINT of the segment
     * @param B END POINT of the segment
     * @param C MIDDLE POINT (user location) which is outside of the segment
     * @return the result of determinant
     */
    fun getDeterminant(A: LatLng, B: LatLng, C: LatLng): Double =
        C.longitude * A.latitude * 1.0 + B.latitude * A.longitude * 1.0 + C.latitude * B.longitude * 1.0 -
                B.longitude * A.latitude * 1.0 - A.longitude * C.latitude * 1.0 - B.latitude * C.longitude

    fun inCadran(point: LatLng): Int {
        if (point.latitude >= 0 && point.longitude >= 0)
            return 1
        if (point.latitude <= 0 && point.longitude >= 0)
            return 2
        if (point.latitude <= 0 && point.longitude <= 0)
            return 3
        if (point.latitude >= 0 && point.longitude <= 0)
            return 4
        return 0
    }

    /**
     * This method will translate one point to another, when reference point will be in origin (0, 0).
     * It will not depend on which part of the globe is the point.
     */
    fun translateRelativeToPointInOrigin(pointToTranslate: LatLng, referencePoint: LatLng): LatLng {
        return LatLng(
            pointToTranslate.latitude - referencePoint.latitude,
            pointToTranslate.longitude - referencePoint.longitude
        )
    }

    fun slope(point1: LatLng, point2: LatLng): Double {
        return (point2.latitude - point1.latitude) / (point2.longitude - point1.longitude)
    }

    /**
     * @param point point which is placed on the segment
     * @param slope the slope of the segment
     * @return a triple with segment equation as: ax + by = c
     */
    fun segmentEquation(point: LatLng, slope: Double): Triple<Double, Double, Double> {
        return Triple(-slope, 1.0, -slope * point.longitude + point.latitude)
    }

    /**
     * @param basePoint base point of the segment
     * @param endPoint end point of the segment
     * @return a triple with segment equation as: ax + by = c
     */
    fun segmentEquation(basePoint: LatLng, endPoint: LatLng): Triple<Double, Double, Double> {
        if (basePoint.latitude.equals(endPoint.latitude))
            return Triple(0.0, 1.0, endPoint.latitude)
        if (basePoint.longitude.equals(endPoint.longitude))
            return Triple(0.0, 1.0, endPoint.longitude)
        val A = (endPoint.latitude - basePoint.latitude) / (endPoint.longitude - basePoint.longitude)
        return Triple(A, -1.0, basePoint.longitude * A - basePoint.latitude)
    }

    /**
     * This method rotates a point with a given angle.
     * @param angle angle in degrees
     * @param point LatLng point to rotate
     * @return a LatLng point rotated with angle degrees
     */
    fun rotate(angle: Double, point: LatLng): LatLng =
        LatLng(
            point.longitude * sin(angle) + point.latitude * cos(angle),
            point.longitude * cos(angle) - point.latitude * sin(angle)
        )

    /**
     * This method translates a point with a reference point.
     * @param point point which is translated
     * @param referencePoint reference point
     * @return a LatLng point translated by the reference point
     */
    fun translate(point: LatLng, referencePoint: LatLng): LatLng =
        LatLng(point.latitude + referencePoint.latitude, point.longitude + referencePoint.longitude)

    /**
     * Method returns the points on perpendicular segment.
     * Aplica algoritmul care gaseste punctul C la distanta d fata de dreapta facuta de (basePoint, endPoint)
     * @param basePoint
     * @param endPoint
     * @param distance the distance between basePoint or endPoint and points to be found
     * @param k If k = 0 it will find perpendicular on basePoint. If k = 1 it will find perpendicular on endPoint.
     */
    fun getPerpendicularSegmentOnPoint(basePoint: LatLng, endPoint: LatLng, distance: Double, k: Int): Pair<LatLng, LatLng> {
        //val segmentSlope = slope(basePoint, endPoint)
        val segEq = segmentEquation(basePoint, endPoint)
        val segmentSlope = -(segEq.first.div(segEq.second)) // / segEq.second)
        //val slopePerpendicular = (-1.0) / segmentSlope
        var slopePerpendicular = (-1.0) / segmentSlope
        val perpendicularSegmentEqBP = segmentEquation(basePoint, slopePerpendicular)
        val perpendicularSegmentEqEP = segmentEquation(endPoint, slopePerpendicular)
        // aboveResult = punctul pe perpendiculara de deasupra dreptei (C1); belowResult = ... (C2)
        var aboveResult: LatLng
        var belowResult: LatLng

        if (k == 0) {
            // calcularea punctelor perpendicularei care trece prin basePoint
            var a = segEq.first
            var b = segEq.second
            var c = segEq.third

            val A = distance * sqrt(a * a + b * b)
            val aboveSegmentEquation = Triple(a, b, A + c)
            val belowSegmentEquation = Triple(-a, -b, A - c)
            val abovePoint = getEquationResult(aboveSegmentEquation, perpendicularSegmentEqBP)
            val belowPoint = getEquationResult(belowSegmentEquation, perpendicularSegmentEqBP)
            aboveResult = LatLng(abovePoint.second, abovePoint.first)
            belowResult = LatLng(belowPoint.second, belowPoint.first)
        } else {
            // calcularea punctelor perpendicularei care trece prin basePoint
            var a = segEq.first
            var b = segEq.second
            var c = segEq.third

            val A = distance * sqrt(a * a + b * b)
            val aboveSegmentEquation = Triple(a, b, A + c)
            val belowSegmentEquation = Triple(-a, -b, A - c)
            val abovePoint = getEquationResult(aboveSegmentEquation, perpendicularSegmentEqEP)
            val belowPoint = getEquationResult(belowSegmentEquation, perpendicularSegmentEqEP)
            belowResult = LatLng(abovePoint.second, abovePoint.first)
            aboveResult = LatLng(belowPoint.second, belowPoint.first)
        }
        return Pair(aboveResult, belowResult)
    }

    /**
     * This method calculates the angle between 2 slopes.
     * If 'atan' returns Infinity, the result is NaN. In this case, the result will be 90 degrees, because lim infinity(atan) = 90.
     * @return the angle in degrees between 2 segments with given slopes.
     */
    fun angleBetween(slope1: Double, slope2: Double): Double {
        var angleBetween = 0.0
        if  (slope1.isInfinite())
            angleBetween = atan(1 / slope2)
        else if (slope2.isInfinite())
            angleBetween = atan(1 / slope2)
        else
            angleBetween = atan(abs((slope1 - slope2) / (1 + slope1 * slope2)))
        if (angleBetween.isNaN())
            return 90.0
        return (angleBetween * 180) / PI
    }
}