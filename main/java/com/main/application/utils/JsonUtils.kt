package com.main.application.utils

import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.google.maps.android.PolyUtil

object JsonUtils {

    private val ACCEPTED_ADDRESS_FORMAT_RO = listOf("street_number", "route", "locality")

    /**
     * Parse address receive from Google Geocoding API into a readable address for Romania
     * @param json response from Google Geocoding API as JSON
     */
    fun parseAddressRO(json: JsonObject): String {
        val map = mutableMapOf<String, String>()

        val addressComponents = json["results"].asJsonArray[0].asJsonObject["address_components"].asJsonArray
        for (i in 0 until addressComponents.size()) {
            val address = addressComponents[i].asJsonObject
            val addressName = address["long_name"].asString
            val addressType = address["types"].asJsonArray
            for (j in 0 until addressType.size()) {
                val addType = addressType[j].asString
                if (ACCEPTED_ADDRESS_FORMAT_RO.contains(addType)) {
                    map[addType] = addressName
                }
            }
        }
        var finalAddress = "${map["route"]}, numărul ${map["street_number"]}, ${map["locality"]}"
        if (finalAddress.contains("Str.")) {
            finalAddress = finalAddress.replace("Str.", "Strada")
        }
        return finalAddress
    }

    fun parseMapPoints(resultAsJson: JsonObject): MutableList<List<LatLng>> {
        val path = mutableListOf<List<LatLng>>()
        val routes = resultAsJson.getAsJsonArray("routes")
        val legs = routes[0].asJsonObject["legs"].asJsonArray
        val steps = legs[0].asJsonObject["steps"].asJsonArray

        for (i in 0 until steps.size()) {
            val points = steps[i].asJsonObject["polyline"].asJsonObject["points"].asString
            val startPoint = steps[i].asJsonObject["start_location"].asJsonObject
            val endPoint = steps[i].asJsonObject["end_location"].asJsonObject
            var lat = startPoint["lat"].asDouble
            var lng = startPoint["lng"].asDouble
            val startPointAsLatLng = LatLng(lat, lng)
            lat = endPoint["lat"].asDouble
            lng = endPoint["lng"].asDouble
            val endPointAsLatLng = LatLng(lat, lng)
            val decode = PolyUtil.decode(points)
            path.add(decode)
        }
        return path
    }
}