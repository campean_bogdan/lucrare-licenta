package com.main.application.utils

import android.app.Application
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.main.application.constants.AppConstants
import com.main.application.constants.Commands
import com.main.application.model.firebase.RoadEvent
import com.main.application.model.local.Point
import com.main.application.model.local.TurningPoint

class MapUtils(application: Application) {

    val pathPoints = mutableListOf<LatLng>()
    var roadEvents = mutableListOf<RoadEvent>()
    var lastNearestLocation = LatLng(0.0, 0.0)

    var polylineRight = PolylineOptions()
    var polylineLeft = PolylineOptions()
    var polylineFirst = PolylineOptions()
    var polylineLast = PolylineOptions()
    var polylineCenter = PolylineOptions()

//    var polylineFirst = PolylineOptions()
//    var polylineLast = PolylineOptions()
//    var polylineRightPoints = PolylineOptions()
//    var polylineLeftPoints = PolylineOptions()
//    var polylineCenter = PolylineOptions()

    var firstPoints = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var lastPoints = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var rightPoints = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var leftPoints = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())
    var centerPoints = Pair(PolylineOptions(), mutableListOf<AppConstants.Type>())

    var roadObjects = mutableListOf<Pair<Any, AppConstants.Type>>()
    var curvePoints = mutableListOf<LatLng>()

    //private val transformationsUtils: TransformationsUtils by inject()
    private val transformationsUtils = TransformationsUtils(application.applicationContext)

    init {
        roadEvents.clear()
    }

    fun resetAll() {
        curvePoints.clear()
        roadObjects.clear()
        centerPoints.first.points.clear()
        centerPoints.second.clear()
        rightPoints.first.points.clear()
        rightPoints.second.clear()
        leftPoints.first.points.clear()
        leftPoints.second.clear()
        firstPoints.first.points.clear()
        firstPoints.second.clear()
        lastPoints.first.points.clear()
        lastPoints.second.clear()
        roadEvents.clear()
        pathPoints.clear()
        polylineRight.points.clear()
        polylineLeft.points.clear()
        polylineFirst.points.clear()
        polylineLast.points.clear()
    }

    fun getDestinationAsLatLng(): LatLng {
        return pathPoints[pathPoints.size - 1]
    }

    // TODO: delete this
    fun addMarkerOnLocation(map: GoogleMap, latLng: LatLng) {
//        map.addMarker(
//            MarkerOptions()
//                .position(latLng)
//                .title("${latLng.latitude}  ${latLng.longitude}")
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
//        )
        //map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19F))
    }

    // TODO: delete all params; only 'resultAsJson', 'drawPolyline' remains
    /**
     * This method decodes points received from Google Maps service. It will store those points in 'pathPoints'
     */
    fun drawPathFromJson(path: MutableList<List<LatLng>>, drawPolyline: (List<List<LatLng>>, PolylineOptions) -> Unit, drawPoint: (LatLng, String) -> Unit, draw: (List<LatLng>, PolylineOptions) -> Unit) {
        try {
            for (i in 0 until path.size) {
                for (j in 0 until path[i].size - 1) {
                    pathPoints.add(path[i][j])
                }
                if (i == path.size - 1)
                    pathPoints.add(path[i][path[i].size - 1])
            }

            // incercuirea traseului si desenarea
            val surroundingPaths = surroundPath(pathPoints)
            val firstP = surroundingPaths[0]
            val lastP = surroundingPaths[1]
            val leftP = surroundingPaths[2]
            val rightP = surroundingPaths[3]

            draw(firstP, polylineFirst)
            draw(lastP, polylineLast)
            draw(leftP, polylineLeft)
            draw(rightP, polylineRight)

            for (i in pathPoints.indices) {
                centerPoints.first.points.add(pathPoints[i])
                centerPoints.second.add(AppConstants.Type.POINT)
                this.roadObjects.add(Pair(
                    Point(
                        pathPoints[i]
                    ), AppConstants.Type.POINT))
            }
            for (i in firstP.indices) {
                firstPoints.first.points.add(firstP[i])
                firstPoints.second.add(AppConstants.Type.POINT)
            }
            for (i in lastP.indices) {
                lastPoints.first.points.add(lastP[i])
                lastPoints.second.add(AppConstants.Type.POINT)
            }
            for (i in leftP.indices) {
                leftPoints.first.points.add(leftP[i])
                leftPoints.second.add(AppConstants.Type.POINT)
            }
            for (i in rightP.indices) {
                rightPoints.first.points.add(rightP[i])
                rightPoints.second.add(AppConstants.Type.POINT)
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    fun drawPathFromList(points: List<LatLng>, drawPolyline: (List<List<LatLng>>, PolylineOptions) -> Unit, drawPoint: (LatLng, String) -> Unit, draw: (List<LatLng>, PolylineOptions) -> Unit) {
        val list = mutableListOf<List<LatLng>>()
        for (i in 0 until points.size - 1) {
            list.add(listOf(points[i], points[i + 1]))
        }
        //drawPolyline(list, polylineCenter)
        pathPoints.add(list[0][0])
        for ((i, p) in points.withIndex()) {
            if (i > 0) {
                if (transformationsUtils.haversineDistance(points[i - 1], p) != 0.0) {
                    pathPoints.add(p)
                }
            }
        }
        val surroundingPaths = surroundPath(pathPoints)
        val firstP = surroundingPaths[0]
        val lastP = surroundingPaths[1]
        val leftP = surroundingPaths[2]
        val rightP = surroundingPaths[3]

        draw(firstP, polylineFirst)
        draw(lastP, polylineLast)
        draw(leftP, polylineLeft)
        draw(rightP, polylineRight)

        for (i in pathPoints.indices) {
            centerPoints.first.points.add(pathPoints[i])
            centerPoints.second.add(AppConstants.Type.POINT)
            this.roadObjects.add(Pair(
                Point(
                    pathPoints[i]
                ), AppConstants.Type.POINT))
        }
        for (i in firstP.indices) {
            firstPoints.first.points.add(firstP[i])
            firstPoints.second.add(AppConstants.Type.POINT)
        }
        for (i in lastP.indices) {
            lastPoints.first.points.add(lastP[i])
            lastPoints.second.add(AppConstants.Type.POINT)
        }
        for (i in leftP.indices) {
            leftPoints.first.points.add(leftP[i])
            leftPoints.second.add(AppConstants.Type.POINT)
        }
        for (i in rightP.indices) {
            rightPoints.first.points.add(rightP[i])
            rightPoints.second.add(AppConstants.Type.POINT)
        }
    }

    /**
     * Method checks if a point is between 2 surrounding polylines of a path.
     * @param point point to check
     * @param leftPoints left polyline
     * @param rightPoints right polyline
     * @return a boolean value if the point is inside the path or not
     */
    fun isBetweenLines(point: LatLng, pathPoints: List<LatLng>, leftPoints: List<LatLng>, rightPoints: List<LatLng>): Boolean {
        val nearestLeft = getNearestPoint(point, leftPoints)
        val nearestIndex = nearestLeft.second
        val nearestCenterIndex = getNearestPoint(point, pathPoints).second

        if (nearestIndex > 0 && nearestIndex < leftPoints.size - 1) {
            val prevPointL = leftPoints[nearestIndex - 1]
            val middlePointL = leftPoints[nearestIndex]
            val nextPointL = leftPoints[nearestIndex + 1]
            val prevPointR = rightPoints[nearestIndex - 1]
            val middlePointR = rightPoints[nearestIndex]
            val nextPointR = rightPoints[nearestIndex + 1]
            val roadSide = transformationsUtils.isTurningPoint(pathPoints[nearestCenterIndex - 1], pathPoints[nearestCenterIndex], pathPoints[nearestCenterIndex + 1]).second
            val det1L = transformationsUtils.getDeterminant(prevPointL, middlePointL, point)
            val det2L = transformationsUtils.getDeterminant(middlePointL, nextPointL, point)
            val det1R = transformationsUtils.getDeterminant(prevPointR, middlePointR, point)
            val det2R = transformationsUtils.getDeterminant(middlePointR, nextPointR, point)

            // verific cazurile in care cel mai apropiat punct este curba st, curba dr sau punct normal
            when (roadSide) {
                "Left" -> {
                    // x4 || x5 || x6
                    return det1L < 0 && det1R > 0 && det2L > 0 && det2R > 0 ||
                            det1L < 0 && det1R > 0 && det2L < 0 && det2R > 0 ||
                            det1L > 0 && det1R > 0 && det2L < 0 && det2R > 0
                }
                "Right" -> {
                    // determinant > 0 == left
                    // x1 || x2 || x3
                    return det1L < 0 && det1R > 0 && det2L < 0 && det2R < 0 ||
                            det1L < 0 && det1R > 0 && det2L < 0 && det2R > 0 ||
                            det1L < 0 && det1R < 0 && det2L < 0 && det2R > 0
                }
                else -> {
                    return det1L < 0 && det1R > 0 || det2L < 0 && det2R > 0
                }
            }
        }
        return false
    }

    /**
     * This method rotate a pair of points by a reference point with a given angle.
     * @param angle angle in degrees
     * @param pair pair of LatLng points to be rotated
     * @param referencePoint the reference point
     * @return a pair of LatLng points rotated around a reference point with a given angle
     */
    fun rotate(angle: Double, pair: Pair<LatLng, LatLng>, referencePoint: LatLng): Pair<LatLng, LatLng> {
        val point1 = transformationsUtils.translate(pair.first, LatLng(-referencePoint.latitude, -referencePoint.longitude))
        val point2 = transformationsUtils.translate(pair.second, LatLng(-referencePoint.latitude, -referencePoint.longitude))
        val rotatedPoint1 = transformationsUtils.rotate(angle, point1)
        val rotatedPoint2 = transformationsUtils.rotate(angle, point2)
        val newPoint1 = transformationsUtils.translate(rotatedPoint1, referencePoint)
        val newPoint2 = transformationsUtils.translate(rotatedPoint2, referencePoint)
        return Pair(newPoint1, newPoint2)
    }

    /**
     * This method compute the points which surround the path. It applies the algorithm with L1, L2, R1 and R2. If, for a curve point, the surrounding points are on the same side,
     * it will try to change their orientation to move one on the side and the second on the other side.
     * This method computes all the surrounding points using the basePoint. If a point is curve and its surrounding points are on the same side, it will compute for
     * endPoint on the same curve point to change their orientation.
     * @param pathPoints a list of LatLng points
     * @param
     * @return a list of LatLng. 0 - firstPoints list, 1 - lastPoints list, 2 - leftPoints list, 3 - rightPoints list. FirstPoints & LastPoints are the lines between surrounding points of the
     * first point, respectively last point. LeftPoints & RightPoints are lists of points on the left/right side of the path.
     */
    private fun surroundPath(pathPoints: List<LatLng>): List<MutableList<LatLng>> {
        val toBeDeleted = mutableListOf<LatLng>()
        var ps: Pair<LatLng, LatLng>
        var roadSide_11: Pair<Double, String> = Pair(0.0, "")
        var roadSide_12: Pair<Double, String> = Pair(0.0, "")
        var roadSide_21: Pair<Double, String> = Pair(0.0, "")
        var roadSide_22: Pair<Double, String> = Pair(0.0, "")
        var curvePoint: Pair<Int, String> = Pair(0, "")
        val firstPoints = mutableListOf<LatLng>()
        val lastPoints = mutableListOf<LatLng>()
        val rightPoints = mutableListOf<LatLng>()
        val leftPoints = mutableListOf<LatLng>()

        // ADAUGAREA PRIMULUI PUNCT
        ps = transformationsUtils.getPerpendicularSegmentOnPoint(pathPoints[0], pathPoints[1], AppConstants.ROAD_DISTANCE, 0)
        roadSide_11 = transformationsUtils.getRoadSide(ps.first, pathPoints[0], pathPoints[1])
        roadSide_12 = transformationsUtils.getRoadSide(ps.first, pathPoints[0], pathPoints[1])
        roadSide_21 = transformationsUtils.getRoadSide(ps.second, pathPoints[0], pathPoints[1])
        roadSide_22 = transformationsUtils.getRoadSide(ps.second, pathPoints[0], pathPoints[1])

        // normal, P1
        if (roadSide_12.second == Commands.LEFT_RO) {
            leftPoints.add(ps.first)
        }
        // P2
        if (roadSide_22.second == Commands.RIGHT_RO) {
            rightPoints.add(ps.second)
        }
        // caz invers, P1
        if (roadSide_12.second == Commands.RIGHT_RO) {
            rightPoints.add(ps.first)
        }
        // P2
        if (roadSide_22.second == Commands.LEFT_RO) {
            leftPoints.add(ps.second)
        }
        firstPoints.add(ps.first)
        firstPoints.add(ps.second)

        // ADAUGAREA PUNCTELOR INTERMEDIARE
        for (i in 1 until pathPoints.size - 1) {
            curvePoint = transformationsUtils.isTurningPoint(pathPoints[i - 1], pathPoints[i], pathPoints[i + 1])
            // daca e curba se calculeaza pozitia punctului fata de dreapta in functie de dreapta precedenta si punctul endPoint
            // daca nu e curba se calculeaza pozitia punctului fata de dreapta in functie de dreapta curenta si punctul basePoint
            when (curvePoint.second) {
                "Right" -> {
                    ps = transformationsUtils.getPerpendicularSegmentOnPoint(pathPoints[i], pathPoints[i + 1], AppConstants.ROAD_DISTANCE, 0)
                    ps = rotate(-45.0, ps, pathPoints[i])
                    roadSide_11 = transformationsUtils.getRoadSide(ps.first, pathPoints[i - 1], pathPoints[i])
                    roadSide_12 = transformationsUtils.getRoadSide(ps.first, pathPoints[i], pathPoints[i + 1])
                    roadSide_21 = transformationsUtils.getRoadSide(ps.second, pathPoints[i - 1], pathPoints[i])
                    roadSide_22 = transformationsUtils.getRoadSide(ps.second, pathPoints[i], pathPoints[i + 1])


                    if (roadSide_11.second == Commands.LEFT_RO && roadSide_12.second == Commands.RIGHT_RO &&
                        roadSide_21.second == Commands.RIGHT_RO && roadSide_22.second == Commands.LEFT_RO ||
                        roadSide_21.second == Commands.LEFT_RO && roadSide_22.second == Commands.RIGHT_RO &&
                        roadSide_11.second == Commands.RIGHT_RO && roadSide_12.second == Commands.LEFT_RO
                    ) {
                        ps = transformationsUtils.getPerpendicularSegmentOnPoint(pathPoints[i - 1], pathPoints[i], AppConstants.ROAD_DISTANCE, 1)
                        ps = rotate(-45.0, ps, pathPoints[i])
                        roadSide_11 = transformationsUtils.getRoadSide(ps.first, pathPoints[i - 1], pathPoints[i])
                        roadSide_12 = transformationsUtils.getRoadSide(ps.first, pathPoints[i], pathPoints[i + 1])
                        roadSide_21 = transformationsUtils.getRoadSide(ps.second, pathPoints[i - 1], pathPoints[i])
                        roadSide_22 = transformationsUtils.getRoadSide(ps.second, pathPoints[i], pathPoints[i + 1])
                    }
                }
                "Left" -> {
                    ps = transformationsUtils.getPerpendicularSegmentOnPoint(pathPoints[i], pathPoints[i + 1], AppConstants.ROAD_DISTANCE, 0)
                    ps = rotate(-45.0, ps, pathPoints[i])
                    roadSide_11 = transformationsUtils.getRoadSide(ps.first, pathPoints[i - 1], pathPoints[i])
                    roadSide_12 = transformationsUtils.getRoadSide(ps.first, pathPoints[i], pathPoints[i + 1])
                    roadSide_21 = transformationsUtils.getRoadSide(ps.second, pathPoints[i - 1], pathPoints[i])
                    roadSide_22 = transformationsUtils.getRoadSide(ps.second, pathPoints[i], pathPoints[i + 1])

                    if (roadSide_11.second == Commands.RIGHT_RO && roadSide_12.second == Commands.LEFT_RO &&
                        roadSide_21.second == Commands.LEFT_RO && roadSide_22.second == Commands.RIGHT_RO ||
                        roadSide_21.second == Commands.RIGHT_RO && roadSide_22.second == Commands.LEFT_RO &&
                        roadSide_11.second == Commands.LEFT_RO && roadSide_12.second == Commands.RIGHT_RO
                    ) {
                        ps = transformationsUtils.getPerpendicularSegmentOnPoint(pathPoints[i - 1], pathPoints[i], AppConstants.ROAD_DISTANCE, 1)
                        ps = rotate(-45.0, ps, pathPoints[i])
                        roadSide_11 = transformationsUtils.getRoadSide(ps.first, pathPoints[i - 1], pathPoints[i])
                        roadSide_12 = transformationsUtils.getRoadSide(ps.first, pathPoints[i], pathPoints[i + 1])
                        roadSide_21 = transformationsUtils.getRoadSide(ps.second, pathPoints[i - 1], pathPoints[i])
                        roadSide_22 = transformationsUtils.getRoadSide(ps.second, pathPoints[i], pathPoints[i + 1])
                    }
                }
                else -> {
                    ps = transformationsUtils.getPerpendicularSegmentOnPoint(pathPoints[i], pathPoints[i + 1], AppConstants.ROAD_DISTANCE, 0)
                    roadSide_12 = transformationsUtils.getRoadSide(ps.first, pathPoints[i], pathPoints[i + 1])
                    roadSide_22 = transformationsUtils.getRoadSide(ps.second, pathPoints[i], pathPoints[i + 1])
                }
            }

            if (!ps.first.latitude.isNaN() && !ps.first.longitude.isNaN() && !ps.second.latitude.isNaN() && !ps.second.longitude.isNaN()) {
                // right, caz I, ps.first
                // R1
                if (curvePoint.second == "Right") {
                    if (roadSide_11.second == Commands.LEFT_RO && roadSide_12.second == Commands.RIGHT_RO) {
                        leftPoints.add(ps.first)
                    }
                    // caz II, ps.first
                    // R1
                    if (roadSide_11.second == Commands.RIGHT_RO && roadSide_12.second == Commands.RIGHT_RO) {
                        rightPoints.add(ps.first)
                    }
                    // right, caz I, ps.second
                    // R1
                    if (roadSide_21.second == Commands.LEFT_RO && roadSide_22.second == Commands.RIGHT_RO) {
                        leftPoints.add(ps.second)
                    }
                    // caz II, ps.second
                    // R1
                    if (roadSide_21.second == Commands.RIGHT_RO && roadSide_22.second == Commands.RIGHT_RO) {
                        rightPoints.add(ps.second)
                    }
                    // caz I, R2
                    if (roadSide_11.second == Commands.RIGHT_RO && roadSide_12.second == Commands.LEFT_RO) {
                        leftPoints.add(ps.first)
                    }
                    // cazII, R2
                    if (roadSide_11.second == Commands.LEFT_RO && roadSide_12.second == Commands.LEFT_RO) {
                        leftPoints.add(ps.first)
                    }
                    // caz I, ps.second, R2
                    if (roadSide_21.second == Commands.RIGHT_RO && roadSide_22.second == Commands.LEFT_RO) {
                        leftPoints.add(ps.second)
                    }
                    // cazII, ps.second, R2
                    if (roadSide_21.second == Commands.LEFT_RO && roadSide_22.second == Commands.LEFT_RO) {
                        leftPoints.add(ps.second)
                    }
                    // left, caz I, ps.first
                } else if (curvePoint.second == "Left") {
                    // L1
                    if (roadSide_11.second == Commands.RIGHT_RO && roadSide_12.second == Commands.LEFT_RO) {
                        rightPoints.add(ps.first)
                    }
                    // caz II, ps.first
                    // L1
                    if (roadSide_11.second == Commands.LEFT_RO && roadSide_12.second == Commands.LEFT_RO) {
                        leftPoints.add(ps.first)
                    }
                    // caz I, ps.second L1
                    if (roadSide_21.second == Commands.RIGHT_RO && roadSide_22.second == Commands.LEFT_RO) {
                        rightPoints.add(ps.second)
                    }
                    // caz II, ps.seconds
                    // L1
                    if (roadSide_21.second == Commands.LEFT_RO && roadSide_22.second == Commands.LEFT_RO) {
                        leftPoints.add(ps.second)
                    }

                    // cazI, ps.first, L2
                    if (roadSide_11.second == Commands.LEFT_RO && roadSide_12.second == Commands.RIGHT_RO) {
                        rightPoints.add(ps.first)
                    }

                    // caz II, ps.first L2
                    if (roadSide_11.second == Commands.RIGHT_RO && roadSide_12.second == Commands.RIGHT_RO) {
                        rightPoints.add(ps.first)
                    }
                    // caz I, ps.second, L2
                    if (roadSide_21.second == Commands.LEFT_RO && roadSide_22.second == Commands.RIGHT_RO) {
                        rightPoints.add(ps.second)
                    }

                    // caz II, ps.second, L2
                    if (roadSide_21.second == Commands.RIGHT_RO && roadSide_22.second == Commands.RIGHT_RO) {
                        rightPoints.add(ps.second)
                    }
                } else {
                    // normal, P1
                    if (roadSide_12.second == Commands.LEFT_RO) {
                        leftPoints.add(ps.first)
                    }
                    // P2
                    if (roadSide_22.second == Commands.RIGHT_RO) {
                        rightPoints.add(ps.second)
                    }
                    // caz invers, P1
                    if (roadSide_12.second == Commands.RIGHT_RO) {
                        rightPoints.add(ps.first)
                    }
                    // P2
                    if (roadSide_22.second == Commands.LEFT_RO) {
                        leftPoints.add(ps.second)
                    }
                }
            } else {
                toBeDeleted.add(this.pathPoints[i])
            }
        }

        // ADAUGAREA ULTIMULUI PUNCT
        ps = transformationsUtils.getPerpendicularSegmentOnPoint(pathPoints[pathPoints.size - 2], pathPoints[pathPoints.size - 1], AppConstants.ROAD_DISTANCE, 1)
        roadSide_12 = transformationsUtils.getRoadSide(ps.first, pathPoints[pathPoints.size - 2], pathPoints[pathPoints.size - 1])
        roadSide_22 = transformationsUtils.getRoadSide(ps.second, pathPoints[pathPoints.size - 2], pathPoints[pathPoints.size - 1])

        // normal, P1
        if (roadSide_12.second == Commands.LEFT_RO) {
            leftPoints.add(ps.first)
        }
        // P2
        if (roadSide_22.second == Commands.RIGHT_RO) {
            rightPoints.add(ps.second)
        }
        // caz invers, P1
        if (roadSide_12.second == Commands.RIGHT_RO) {
            rightPoints.add(ps.first)
        }
        // P2
        if (roadSide_22.second == Commands.LEFT_RO) {
            leftPoints.add(ps.second)
        }
        lastPoints.add(ps.first)
        lastPoints.add(ps.second)
        this.pathPoints.removeAll(toBeDeleted)

        return listOf(firstPoints, lastPoints, leftPoints, rightPoints)
    }

    /**
     * @param drawMarkerOnCurve
     * @param currentCurvePointIndex current index of the next curve point on map
     * @return a list which contains: [0] = the index of the next curve point, [1] = direction of the curve point, [2] = point coordinates, [3] = angle in degrees
     */
    fun getNextTurningPointIndex(
        pathPoints: List<LatLng>,
        map: GoogleMap,
        currentCurvePointIndex: Int
    ): List<Any> {
        var i = currentCurvePointIndex
        // pentru test: comment var i = currentCurvePointIndex; comment returns in if statements
        //var i = 0
        while (i < pathPoints.size - 2) {
            // se iau in considerare punctele care sunt mai departate unele de altele cu cel putin 5 m
            if (i >= 0 && i < pathPoints.size - 2) {
                val isCurvePoint = transformationsUtils.isTurningPoint(
                    pathPoints[i],
                    pathPoints[i + 1],
                    pathPoints[i + 2]
                )
                // i+1 = middle point - punctul de mijloc care e verificat daca este curba
                // se considera curba <=> e punct de curba si distanta dintre ultima curba si cea recenta e > 5 metri
                if (isCurvePoint.second == "Left" && transformationsUtils.haversineDistance(
                        pathPoints[currentCurvePointIndex],
                        pathPoints[i + 1]
                    ) > 0
                ) {
                    map.addMarker(
                        MarkerOptions().position(pathPoints[i + 1]).title("${isCurvePoint.first}  Left")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                    )
                    //return Triple(i + 1, "Stânga", pathPoints[i + 1])
                    return listOf(i+1, "Stânga", pathPoints[i + 1], isCurvePoint.first)
                }
                if (isCurvePoint.second == "Right" && transformationsUtils.haversineDistance(
                        pathPoints[currentCurvePointIndex],
                        pathPoints[i + 1]
                    ) > 0
                ) {
                    map.addMarker(
                        MarkerOptions().position(pathPoints[i + 1]).title("${isCurvePoint.first}  Right")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                    )
                    //return Triple(i + 1, "Dreapta", pathPoints[i + 1])
                    return listOf(i+1, "Dreapta", pathPoints[i + 1], isCurvePoint.first)
                }
            }
            i++
        }
        //return Triple(-1, "NCP", pathPoints[pathPoints.size - 1])
        return listOf(-1, "NCP", pathPoints[i + 1], -1)
    }

    fun getTurningPoints(list: List<LatLng>): List<TurningPoint> {
        val newList = mutableListOf<TurningPoint>()
        for (i in 1 until list.size - 2) {
            val isTurningPoint = transformationsUtils.isTurningPoint(list[i - 1], list[i], list[i + 1])
            if (isTurningPoint.second != "NCP") {
                val turningPoint = TurningPoint(list[i], isTurningPoint.second, isTurningPoint.first)
                newList.add(turningPoint)
            }
        }
        return newList
    }

    fun getNextTurningPointIndex(
        currentCurvePointIndex: Int
    ): Triple<Int, String, LatLng> {
        var i = currentCurvePointIndex
        //var i = 0
        while (i < pathPoints.size - 2) {
            // se iau in considerare punctele care sunt mai departate unele de altele cu cel putin 5 m
            if (i >= 0 && i < pathPoints.size - 2) {
                val isCurvePoint = transformationsUtils.isTurningPoint(
                    pathPoints[i],
                    pathPoints[i + 1],
                    pathPoints[i + 2]
                )
                // i+1 = middle point - punctul de mijloc care e verificat daca este curba
                // se considera curba <=> e punct de curba si distanta dintre ultima curba si cea recenta e > 5 metri
                if (isCurvePoint.second == "Left" && transformationsUtils.haversineDistance(
                        pathPoints[currentCurvePointIndex],
                        pathPoints[i + 1]
                    ) > 5
                ) {
                    return Triple(i + 1, "Stânga", pathPoints[i + 1])
                }
                if (isCurvePoint.second == "Right" && transformationsUtils.haversineDistance(
                        pathPoints[currentCurvePointIndex],
                        pathPoints[i + 1]
                    ) > 5
                ) {
                    return Triple(i + 1, "Dreapta", pathPoints[i + 1])
                }
            }
            i++
        }
        return Triple(-1, "NCP", pathPoints[pathPoints.size - 1])
    }

    /**
     * @param currentPosition
     * @param nearestPointIndex index of the nearest point from the path points
     * @return device orientation (in degrees - [0, 360]) with respect to azimuth, and device orientation (String: Left, Right, Forward, Back)
     */
    fun getOrientation(currentPosition: LatLng, nearestPointIndex: Int, pathPoints: List<LatLng>) =
        if (nearestPointIndex < pathPoints.size - 1)
            transformationsUtils.getRoadSide(
                currentPosition,
                pathPoints[nearestPointIndex],
                pathPoints[nearestPointIndex + 1]
        )
        else
            transformationsUtils.getRoadSide(
                currentPosition,
                pathPoints[nearestPointIndex],
                pathPoints[nearestPointIndex]
        )

    /**
     * @return pair with: string = turning direction in which user must turn, int = angle between user orientation and road orientation
     * @param nearestPositionIndex current location index
     * @param deviceOrientation device orientation in degrees
     * @param roadSide user's road side
     * @param pathPoints a list of points which is used to determine the relative orientation with road
     */
    fun azimuthAngleOfNearestPosition(
        nearestPositionIndex: Int,
        deviceOrientation: Double,
        roadSide: String,
        pathPoints: List<LatLng>
    ): Pair<String?, Int> {
        // nearestPositionAngle = unghiul facut de dreapta care are punctul de baza in nearestPosition
        if (nearestPositionIndex < pathPoints.size - 1) {
            val segmentOrientation = transformationsUtils.angleSegmentAzimuth(
                pathPoints[nearestPositionIndex],
                pathPoints[nearestPositionIndex + 1],
                1
            )
            return transformationsUtils.getOrientationWithRoad(
                deviceOrientation,
                segmentOrientation,
                roadSide,
                nearestPositionIndex
            )
        }
        return Pair(null, -1)
    }

    fun azimuthAngleOfNearestPosition(
        nearestPositionIndex: Int,
        deviceOrientation: Double,
        roadSide: String
    ): Pair<String?, Int> {
        // nearestPositionAngle = unghiul facut de dreapta care are punctul de baza in nearestPosition
        if (nearestPositionIndex < pathPoints.size - 1) {
            val segmentOrientation = transformationsUtils.angleSegmentAzimuth(
                pathPoints[nearestPositionIndex],
                pathPoints[nearestPositionIndex + 1],
                1
            )
            return transformationsUtils.getOrientationWithRoad(
                deviceOrientation,
                segmentOrientation,
                roadSide,
                nearestPositionIndex
            )
        }
        return Pair(null, -1)
    }

    fun getNextRoadEventIndex(list: List<Pair<LatLng, AppConstants.Type>>, currentPosition: LatLng, index: Int): Int {
        var min = Double.MAX_VALUE
        var reIndex = index
        if (list.isNullOrEmpty())
            return -1
        if (index < list.size - 1) {
            for (i in index + 1 until list.size) {
                if (list[i].second == AppConstants.Type.ROAD_EVENT) {
                    return i
                }
            }
        }
        return -1
    }

    // verific la fiecare curba & road event + reset la announcedDistances
    /**
     * Method checks which distance from AppConstants.ANNOUNCE_DISTANCES array is the closest from distance between current position and next critical point.
     * For example, if the distance between currentPosition and criticalPointPosition is 23, method will return 25.
     */
    fun checkDistance(currentPosition: LatLng, criticalPointPosition: LatLng, list: MutableList<Int>): Int {
        val distance = transformationsUtils.haversineDistance(currentPosition, criticalPointPosition).toInt()
        for (i in 0..AppConstants.ANNOUNCE_DISTANCES.size - 2) {
            if (distance >= AppConstants.ANNOUNCE_DISTANCES[i + 1] && distance <= AppConstants.ANNOUNCE_DISTANCES[i]) {
                val x = checkDist(AppConstants.ANNOUNCE_DISTANCES[i], list)
                return if (x) {
                    AppConstants.ANNOUNCE_DISTANCES[i]
                } else
                    -1
            }
        }
        return -1
    }

    private fun checkDist(distance: Int, list: MutableList<Int>): Boolean {
        for (i in list.indices) {
            if (distance == list[i])
                return false
        }
        if (distance != -1)
            list.add(distance)
        return true
    }

    /**
     * Method returns the road event from road events list, where the coordinates are equals to given coordinates.
     * @param coordinates given coordinates
     * @param roadEvents road events list where to search for object
     */
    fun getRoadEvent(coordinates: LatLng, roadEvents: List<RoadEvent>): RoadEvent? {
        for (x in roadEvents) {
            if (x.latitude == coordinates.latitude && x.longitude == coordinates.longitude)
                return x
        }
        return null
    }

    var map: GoogleMap? = null

    /**
     * Method sorts a list of elements with a new list of elements. Sorting rule is by route.
     * @param list existing list of elements
     * @param elements list of elements to be inserted in list
     * @return a sorted list made by 2 lists as parameter
     */
    fun addElements(list: List<Pair<LatLng, AppConstants.Type>>, elements: List<RoadEvent>, type: AppConstants.Type): MutableList<Pair<LatLng, AppConstants.Type>> {
        val newList = mutableListOf<Pair<LatLng, AppConstants.Type>>()
        for (x in list) {
            newList.add(x)
        }
        for (j in elements.indices) {
            var i = 0
            val element = elements[j]
            val elementToBeAdded = LatLng(elements[j].latitude, elements[j].longitude)
            var index = 0
            var min = Double.MAX_VALUE
            while (i < newList.size) {
                val dist = transformationsUtils.haversineDistance(elementToBeAdded, newList[i].first)
                if (dist < min && newList[i].second == AppConstants.Type.POINT) {
                    index = i
                    min = dist
                }
                i++
            }

            val leftP = leftPoints.first.points[index]
            val rightP = rightPoints.first.points[index]
            val det = transformationsUtils.getDeterminant(leftP, rightP, elementToBeAdded)
            val ps = transformationsUtils.getPerpendicularSegmentOnPoint(elementToBeAdded, newList[index + 1].first, AppConstants.ROAD_DISTANCE, 0)
            val det2 = transformationsUtils.getDeterminant(elementToBeAdded, newList[index + 1].first, ps.first)
            if (det >= 0) {
                val correctPosition = transformationsUtils.getPerpendicularPoint(elementToBeAdded, newList[index].first, newList[index + 1].first)
                newList.add(index + 1, Pair(correctPosition, type))
                centerPoints.first.points.add(index + 1, correctPosition)
                centerPoints.second.add(index + 1, AppConstants.Type.ROAD_EVENT)
                leftPoints.second.add(index + 1, AppConstants.Type.ROAD_EVENT)
                rightPoints.second.add(index + 1, AppConstants.Type.ROAD_EVENT)

                val newRoadEvent = RoadEvent(
                    element.category,
                    correctPosition.latitude,
                    correctPosition.longitude,
                    element.email,
                    element.date
                )
                this.roadObjects.add(index + 1, Pair(newRoadEvent, AppConstants.Type.ROAD_EVENT))
                if (det2 >= 0) {
                    leftPoints.first.points.add(index + 1, ps.first)
                    rightPoints.first.points.add(index + 1, ps.second)
                } else {
                    leftPoints.first.points.add(index + 1, ps.second)
                    rightPoints.first.points.add(index + 1, ps.first)
                }
            }
            else {
                val correctPosition = transformationsUtils.getPerpendicularPoint(elementToBeAdded, newList[index - 1].first, newList[index].first)
                newList.add(index, Pair(correctPosition, type))
                centerPoints.first.points.add(index, correctPosition)
                centerPoints.second.add(index, AppConstants.Type.ROAD_EVENT)
                leftPoints.second.add(index, AppConstants.Type.ROAD_EVENT)
                rightPoints.second.add(index, AppConstants.Type.ROAD_EVENT)

                val newRoadEvent = RoadEvent(
                    element.category,
                    correctPosition.latitude,
                    correctPosition.longitude,
                    element.email,
                    element.date
                )
                this.roadObjects.add(index, Pair(newRoadEvent, AppConstants.Type.ROAD_EVENT))
                if (det2 >= 0) {
                    leftPoints.first.points.add(index, ps.first)
                    rightPoints.first.points.add(index, ps.second)
                } else {
                    leftPoints.first.points.add(index, ps.second)
                    rightPoints.first.points.add(index, ps.first)
                }
            }
        }

        for (i in roadObjects.indices) {
            if (roadObjects[i].second == AppConstants.Type.ROAD_EVENT) {
                val el = (roadObjects[i].first as RoadEvent)
            }
        }
        return newList
    }

    /**
     * Method compute the index point of center path, where the user just passed.
     * @param currentPosition user position
     * @param currentIndex current index of the path
     * @param leftPoints
     * @param rightPoints
     * @return the passed index from path points
     */
    fun getCurrentIndexOnPath(currentPosition: LatLng, currentIndex: Int, leftPoints: List<LatLng>, rightPoints: List<LatLng>): Int {
        var i = currentIndex
            while (i < leftPoints.size - 1) {
                val spbl = leftPoints[i]
                val spal = leftPoints[i + 1]
                val spbr = rightPoints[i]
                val spar = rightPoints[i + 1]
                val detB = transformationsUtils.getDeterminant(spbr, spbl, currentPosition)
                val detA = transformationsUtils.getDeterminant(spar, spal, currentPosition)
                val detC = transformationsUtils.getDeterminant(spbl, spal, currentPosition)
                val detD = transformationsUtils.getDeterminant(spbr, spar, currentPosition)
                if (detB < 0 && detA > 0 && detC < 0 && detD > 0) {
                    return i
                }
                i++
            }
        return 0
    }

    /**
     * @return the nearest position (from every point on the path) from current position
     */
    fun getNearestLocationFromPathPoints(map: GoogleMap, currentPosition: LatLng): LatLng {
        val nearest = getNearestPoint(currentPosition)
        val nearestPosition = nearest.first
        val index = nearest.second
        addMarkerOnLocation(map, nearestPosition)
        if (index + 1 < pathPoints.size) {
            transformationsUtils.getRoadSide(
                currentPosition,
                nearestPosition,
                pathPoints[index + 1]
            )
        }
        lastNearestLocation = nearestPosition
        return nearestPosition
    }

    /**
     * @return the nearest position on the road (perpendicular point)
     */
    fun getNearestPointOnPath(map: GoogleMap, currentPosition: LatLng): LatLng {
        val nearest = getNearestPoint(currentPosition)
        val nearestPosition = nearest.first
        val index = nearest.second
        //addMarkerOnLocation(map, nearestPosition)
        val perpendicularPoint: LatLng
        if (index + 1 < pathPoints.size) {
            perpendicularPoint = transformationsUtils.getPerpendicularPoint(
                currentPosition,
                nearestPosition,
                pathPoints[index + 1]
            )
            //addMarkerOnLocation(map, perpendicularPoint)
            return perpendicularPoint
        }
        lastNearestLocation = nearestPosition
        return LatLng(0.0, 0.0)
    }

    fun getNearestPerpendicularPointOnPath(currentPosition: LatLng, k: Int): LatLng {
        val nearest = getNearestPoint(currentPosition)
        val nearestPosition = nearest.first
        val index = nearest.second
        val perpendicularPoint: LatLng
        if (k == 0) {
            if (index + 1 < pathPoints.size) {
                perpendicularPoint = transformationsUtils.getPerpendicularPoint(
                    currentPosition,
                    nearestPosition,
                    pathPoints[index + 1]
                )
                return perpendicularPoint
            }
        }
        else
            if (index > 1 && index < pathPoints.size) {
                perpendicularPoint = transformationsUtils.getPerpendicularPoint(
                    currentPosition,
                    pathPoints[index - 1],
                    nearestPosition
                )
                return perpendicularPoint
            }
        lastNearestLocation = nearestPosition
        return LatLng(0.0, 0.0)
    }

    fun getAngleBetweenPointsOnPath(map: GoogleMap, currentPosition: LatLng): Double {
        val nearestPair = getNearestPoint(currentPosition)
        val nearestPoint = nearestPair.first
        var index = nearestPair.second

        if (index + 2 < pathPoints.size) {
            val azim1 =
                transformationsUtils.angleSegmentAzimuth(nearestPoint, pathPoints[index + 1], 0)
            val azim2 = transformationsUtils.angleSegmentAzimuth(
                pathPoints[index + 1],
                pathPoints[index + 2],
                0
            )
            return kotlin.math.abs(180.0 - kotlin.math.abs(azim1 - azim2))
        }
        return 0.0
    }

    /**
     * @return the total distance of the trip
     */
    fun getTotalPathDistance(): Double {
        var totalDistance = 0.0
        for (i in 1 until pathPoints.size) {
            totalDistance += transformationsUtils.haversineDistance(
                pathPoints[i - 1],
                pathPoints[i]
            )
        }
        return totalDistance
    }

    /**
     * Find the nearest point from path points with the min haversine distance
     * @param currentPosition user's current position
     * @return first = nearestPoint, second = index of nearestPoint
     */
    fun getNearestPoint(currentPosition: LatLng): Pair<LatLng, Int> {
        var minDistance = Double.MAX_VALUE
        var nearestPosition = LatLng(0.0, 0.0)
        var i = 0
        var index = 0
        // nearestPosition = cel mai apropiat punct
        // index = indexul celui mai apropiat punct pentru a afla urmatorul punct
        for (position in pathPoints) {
            val dist = transformationsUtils.haversineDistance(currentPosition, position)
            if (dist < minDistance) {
                minDistance = dist
                nearestPosition = position
                index = i
            }
            i++
        }
        return Pair(nearestPosition, index)
    }

    /**
     * Method will find the nearest point of current position from a list of points.
     * @param currentPosition user's current position
     * @param points a list of points
     * @return a pair which contains the point and its index in the list
     */
    fun getNearestPoint(currentPosition: LatLng, points: List<LatLng>): Pair<LatLng, Int> {
        var minDistance = Double.MAX_VALUE
        var nearestPosition = LatLng(0.0, 0.0)
        var i = 0
        var index = 0
        // nearestPosition = cel mai apropiat punct
        // index = indexul celui mai apropiat punct pentru a afla urmatorul punct
        for (position in points) {
            val dist = transformationsUtils.haversineDistance(currentPosition, position)
            if (dist < minDistance) {
                minDistance = dist
                nearestPosition = position
                index = i
            }
            i++
        }
        return Pair(nearestPosition, index)
    }

    /**
     * Return 4 points which represent the area of all route. This method is doing the first filtering -> all points which are in the route area (square)
     * @return first parameter is min, max Latitude; second parameter is min, max Longitude
     */
    fun getMinMaxLatLon(drawRouteLimits: (LatLng, LatLng, LatLng, LatLng) -> Unit): Pair<Pair<Double, Double>, Pair<Double, Double>> {
        var minLat = 90.0
        var maxLat = 0.0
        var minLon = 180.0
        var maxLon = 0.0
        // epsilon = pt a incarda traseul intr-un patrat mai lat decat patratul facut de traseu
        val epsilon = 0.0001
        for (pathPoint in pathPoints) {
            val lat = pathPoint.latitude
            val lon = pathPoint.longitude
            if (lat < minLat) minLat = lat
            if (lat > maxLat) maxLat = lat
            if (lon < minLon) minLon = lon
            if (lon > maxLon) maxLon = lon
        }
        val posA = LatLng(minLat - epsilon, minLon - epsilon)
        val posB = LatLng(minLat - epsilon, maxLon + epsilon)
        val posC = LatLng(maxLat + epsilon, maxLon + epsilon)
        val posD = LatLng(maxLat + epsilon, minLon - epsilon)
        drawRouteLimits(posA, posB, posC, posD)
        return Pair(Pair(minLat - epsilon, maxLat + epsilon), Pair(minLon - epsilon, maxLon + epsilon))
    }

    /**
     * Method filters a list of road events by surrounding lines. If a point is  outside the path made by lines, it will be removed from list.
     * @param roadEvents a list of road events
     * @param polylineCenter a polyline which contains route points with points given by Google services
     * @param polylineLeft a polyline which contains left computed points of route points
     * @param polylineRight a polyline which contains right computed points of route points
     * @return a filtered list of road events. It will return only points which are between left and right polylines.
     */
    fun filterPointsBySurroundingLines(roadEvents: List<RoadEvent>, polylineCenter: PolylineOptions, polylineLeft: PolylineOptions, polylineRight: PolylineOptions, setMarker: (List<RoadEvent>) -> Unit): List<RoadEvent> {
        val filteredRoadEvents = mutableListOf<RoadEvent>()
        for (i in roadEvents.indices) {
            val re = roadEvents[i]
            val point = LatLng(re.latitude, re.longitude)
            if (isBetweenLines(point, pathPoints, polylineLeft.points, polylineRight.points)) {
                val roadEvent = RoadEvent(re.category, re.latitude, re.longitude, re.email, re.date)
                filteredRoadEvents.add(roadEvent)
            }
        }
        return filteredRoadEvents
    }

    /**
     * Method sorts a given list with selection algorithm. It places the minimum distances in front of the list.
     * @param list a list of road events
     * @return a sorted list by distance; reference distance is the first point of the route (pathPoints[0])
     */
    fun sortByDistance(list: MutableList<RoadEvent>): List<RoadEvent> {
        for (i in 0 until list.size - 1) {
            var min = Double.MAX_VALUE
            var index = 0
            for (j in i + 1 until list.size) {
                val sum = transformationsUtils.haversineDistance(pathPoints[0], LatLng(list[j].latitude, list[j].longitude))
                if (sum < min) {
                    min = sum
                    index = j
                }
            }
            if (transformationsUtils.haversineDistance(pathPoints[0], LatLng(list[i].latitude, list[i].longitude)) > min) {
                val aux = list[i]
                list[i] = list[index]
                list[index] = aux
            }
        }
        this.roadEvents = list
        return list
    }

    /**
     * Return points which are the closest to the route. This method is called once when the app starts.
     */
    fun filterPointsByDistance(roadEvents: List<RoadEvent>): MutableList<RoadEvent> {
        val listOfRoadEvents = mutableListOf<RoadEvent>()
        for (roadEvent in roadEvents) {
            val roadEventAsLatLng = LatLng(roadEvent.latitude, roadEvent.longitude)
            // perpendicularele punctului de interes pe cele 2 drepte
            val perpendicularPoint_1 = this.getNearestPerpendicularPointOnPath(roadEventAsLatLng, 0)
            val perpendicularPoint_2 = this.getNearestPerpendicularPointOnPath(roadEventAsLatLng, 1)
            // cele mai apropiate puncte
            val nearestPoint = this.getNearestPoint(roadEventAsLatLng)
            if (nearestPoint.second > 0 && nearestPoint.second < pathPoints.size - 1) {
                val nextNearestPoint = pathPoints[nearestPoint.second + 1]
                val prevNearestPoint = pathPoints[nearestPoint.second - 1]
                val nearestWithNextSegment = transformationsUtils.segmentEquation(nearestPoint.first, nextNearestPoint)
            }
            // adaug pe harta road events care sunt la 13 metri fata de traseu
            // dist_1, dist_2 = lungimea perpendicularei (perpendiculara facuta de punctul de interes pe traseu)
            val dist_1 = transformationsUtils.haversineDistance(LatLng(roadEvent.latitude, roadEvent.longitude), perpendicularPoint_1)
            val dist_2 = transformationsUtils.haversineDistance(LatLng(roadEvent.latitude, roadEvent.longitude), perpendicularPoint_2)
            if (dist_1 < AppConstants.CIRCLE_DISTANCE && dist_2 < AppConstants.CIRCLE_DISTANCE) {
                if (checkPoint(roadEvent))
                    listOfRoadEvents.add(roadEvent)
            }
        }
        for (j in 0 until listOfRoadEvents.size - 1) {
            var min = Double.MAX_VALUE
            var index = 0
            for (i in j + 1 until listOfRoadEvents.size) {
                val sum = transformationsUtils.haversineDistance(LatLng(listOfRoadEvents[i].latitude, listOfRoadEvents[i].longitude), pathPoints[0])
                if (sum < min) {
                    min = sum
                    index = i
                }
            }
            // sortez lista cu road events in functie de distanta initiala (la pornirea aplicatiei) dintre pozitia curenta si fiecare road event
            if (transformationsUtils.haversineDistance(LatLng(listOfRoadEvents[j].latitude, listOfRoadEvents[j].longitude), pathPoints[0]) > min) {
                val aux = listOfRoadEvents[j]
                listOfRoadEvents[j] = listOfRoadEvents[index]
                listOfRoadEvents[index] = aux
            }
        }
        this.roadEvents = listOfRoadEvents
        return listOfRoadEvents
    }

    fun checkPoint(roadEvent: RoadEvent): Boolean {
        var ok_1 = true
        var ok_2 = true
        val point = LatLng(roadEvent.latitude, roadEvent.longitude)
        val nearestPoint = getNearestPoint(point)
        if (nearestPoint.second > 0 && nearestPoint.second < pathPoints.size - 1) {
            val nextNearestPoint = pathPoints[nearestPoint.second + 1]
            val prevNearestPoint = pathPoints[nearestPoint.second - 1]

//            if (point.latitude > nearestPoint.first.latitude) {
//                // punctul se afla intre nearest si nextNearest
//                if (point.latitude > nextNearestPoint.latitude)
//                    ok_1 = false
//                if (point.longitude < )
//                if (point.longitude < nextNearestPoint.longitude || point.longitude > nearestPoint.first.longitude)
//                    ok_1 = false
//            } else {
//                if (point.latitude < nextNearestPoint.latitude)
//                    ok_1 = false
////                // punctul se afla intre prevNearest si nearest
////                if (point.latitude < prevNearestPoint.latitude)
////                    ok_2 = false
////                if (point.longitude < nearestPoint.first.longitude || point.longitude > prevNearestPoint.longitude)
////                    ok_2 = false
//            }
        }
        return ok_1 && ok_2
    }
}