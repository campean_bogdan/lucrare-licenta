package com.main.application.utils

import com.main.application.constants.Commands.BACK_RO
import com.main.application.constants.Commands.DELETE_POINT_OF_INTEREST_RO
import com.main.application.constants.Commands.GET_CURRENT_LOCATION_INFORMATION_RO
import com.main.application.constants.Commands.GET_ROUTE_RO
import com.main.application.constants.Commands.GET_TO_DESTINATION_RO
import com.main.application.constants.Commands.GET_TO_POINT_OF_INTEREST_RO
import com.main.application.constants.Commands.LOGOUT
import com.main.application.constants.Commands.NEED_HELP_RO
import com.main.application.constants.Commands.SET_NEW_POINT_OF_INTEREST_RO
import org.koin.core.KoinComponent

class CommandsUtils : KoinComponent {

    /**
     * @return a pair with command and place as string
     */
    fun parseCommand(text: String): Pair<String, List<String>>? {
        val words = text.split(" ")

        if (words.size > 1) {
            if ((words[0] + " " + words[1]).equals(GET_TO_DESTINATION_RO, true)) {
                val destinationAsString = text.removePrefix(words[0] + " " + words[1] + " ")
                return Pair(GET_TO_DESTINATION_RO, listOf(destinationAsString))
            }
            if ((words[0].equals(GET_ROUTE_RO, true))) {
                if (words[words.size - 1] == BACK_RO) {
                    val definedRouteName = this.parseName(words)
                    println("AICI: NA 0=$definedRouteName")
                    return Pair(GET_ROUTE_RO, listOf(BACK_RO, definedRouteName))
                }
                val definedRouteName = words.subList(1, words.size).listToString()
                return Pair(GET_ROUTE_RO, listOf("", definedRouteName))
            }
            if ((words[0] + " " + words[1]).equals(GET_TO_POINT_OF_INTEREST_RO, true)) {
                val destinationAsString = text.removePrefix(words[0] + " " + words[1] + " ")
                return Pair(GET_TO_POINT_OF_INTEREST_RO, listOf(destinationAsString))
            }
            if ((words[0] + " " + words[1]).equals(GET_CURRENT_LOCATION_INFORMATION_RO, true)) {
                return Pair(GET_CURRENT_LOCATION_INFORMATION_RO, listOf(""))
            }
        }
        if (words[0] == SET_NEW_POINT_OF_INTEREST_RO) {
            val destinationAsString = text.removePrefix(words[0] + " ")
            return Pair(SET_NEW_POINT_OF_INTEREST_RO, listOf(destinationAsString))
        }
        if (words[0] == DELETE_POINT_OF_INTEREST_RO) {
            val destinationAsString = text.removePrefix(words[0] + " ")
            return Pair(DELETE_POINT_OF_INTEREST_RO, listOf(destinationAsString))
        }
        if (words[0] == NEED_HELP_RO) {
            return Pair(NEED_HELP_RO, listOf(""))
        }
        if (words[0] == LOGOUT) {
            return Pair(LOGOUT, listOf(""))
        }

        return null
    }

    private fun parseName(text: List<String>): String {
        val destination = text.subList(1, text.size - 1)
        return destination.listToString()
    }

    private fun List<String>.listToString(): String {
        var string = ""
        for (element in this) {
            string += "$element "
        }
        string = string.removeSuffix(" ")
        return string
    }
}