package com.main.application.utils

import android.content.Context
import com.main.application.model.auth.AuthResponse

class LocalStorageUtils {

    companion object {
        private const val DEFAULT_STRING = "NOT FOUND"
        private const val LOGIN_ACTIVITY_SHARED_PREFERENCES = "LoginActivity"
        private const val EMAIL = "Email"
        private const val TOKEN = "Token"
        private const val REFRESH_TOKEN = "RefreshToken"
        private const val LOCAL_ID = "LocalId"

        private const val DEVICE_TOKEN_SHARED_PREFERENCES = "DeviceRegisterToken"


        fun saveUserAndToken(email: String, authResponse: AuthResponse, context: Context) {
            val sharedPref = context.getSharedPreferences(LOGIN_ACTIVITY_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            var editor = sharedPref.edit()
            editor.putString(EMAIL, email)
            editor.apply()

            editor = sharedPref.edit()
            editor.putString(TOKEN, authResponse.idToken)
            editor.apply()

            editor = sharedPref.edit()
            editor.putString(REFRESH_TOKEN, authResponse.refreshToken)
            editor.apply()

            editor = sharedPref.edit()
            editor.putString(LOCAL_ID, authResponse.uuid)
            editor.apply()
        }

        fun getUserAndToken(context: Context): Pair<String, AuthResponse> {
            val sharedPref = context.getSharedPreferences(LOGIN_ACTIVITY_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            val email = sharedPref.getString(EMAIL, DEFAULT_STRING)
            val authResponse = AuthResponse()
            authResponse.idToken = sharedPref.getString(TOKEN, DEFAULT_STRING)
            authResponse.refreshToken = sharedPref.getString(REFRESH_TOKEN, DEFAULT_STRING)
            authResponse.uuid = sharedPref.getString(LOCAL_ID, DEFAULT_STRING)
            return Pair(email!!, authResponse)
        }

        fun saveDeviceRegisterToken(token: String, context: Context) {
            val sharedPref = context.getSharedPreferences(DEVICE_TOKEN_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            var editor = sharedPref.edit()
            editor.putString(DEVICE_TOKEN_SHARED_PREFERENCES, token)
            editor.apply()
        }

        fun getDeviceRegistrationToken(context: Context): String {
            val sharedPref = context.getSharedPreferences(DEVICE_TOKEN_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            val token = sharedPref.getString(DEVICE_TOKEN_SHARED_PREFERENCES, DEFAULT_STRING)
            return token!!
        }

        fun removeAll(context: Context) {
            val sharedPref = context.getSharedPreferences(LOGIN_ACTIVITY_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            sharedPref.edit()
                .remove(EMAIL)
                .remove(TOKEN)
                .remove(REFRESH_TOKEN)
                .remove(LOCAL_ID)
                .apply()
        }
    }
}