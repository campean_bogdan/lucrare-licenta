package com.main.application.utils

import android.content.Context
import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.main.application.R

/**
 * This class is used to create requests for Google Services
 */
class RequestUtils(val context: Context) {

    companion object {
        fun getStaticImage(points: List<LatLng>): String {
            // compute image center
            var averageLat = 0.0
            var averageLon = 0.0
            for (point in points) {
                averageLat += point.latitude
                averageLon += point.longitude
            }
            averageLat /= points.size
            averageLon /= points.size
            val key = "AIzaSyAepikK8YpQCFe6y8jAWMQCiSzfMt5rugg"

            // path
            var path = "color:0xff0000|weight:3"
            for (p in points) {
                path += "|${p.latitude},${p.longitude}"
            }
            return "https://maps.googleapis.com/maps/api/staticmap?" +
                    "center=$averageLat,$averageLon}&" +
                    "size=100x100&" +
                    "zoom=12&" +
                    "path=$path&" +
                    "key=$key"
        }
    }

    /**
     * Create a direction request to Google Directions API
     * @param lastLocation user's last location as Location
     * @param destinationAsString user's destination by String (ex: "Strada Victoriei numarul 5 Gherla")
     */
    fun getOptimizedDirectionsRequest(lastLocation: Location, destinationAsString: String): Map<String, String> {
        val options = mutableMapOf<String, String>()
        val locationAsString = lastLocation.latitude.toString() + "," + lastLocation.longitude.toString()
        options["origin"] = locationAsString
        options["destination"] = destinationAsString
        options["optimize"] = "true"
        options["mode"] = "walking"
        options["key"] = context.getString(R.string.google_maps_key)
        return options
    }

    /**
     * Create a direction request to Google Directions API
     * @param lastLocation user's last location as Location
     * @param destinationAsString user's destination by LatLng (ex: 47.35085, 23.43984)
     */
    fun getOptimizedDirectionsRequest(lastLocation: Location, destinationAsString: LatLng): Map<String, String> {
        val options = mutableMapOf<String, String>()
        val locationAsString = lastLocation.latitude.toString() + "," + lastLocation.longitude.toString()
        options["origin"] = locationAsString
        options["destination"] = "${destinationAsString.latitude},${destinationAsString.longitude}"
        options["optimize"] = "true"
        options["mode"] = "walking"
        options["key"] = context.getString(R.string.google_maps_key)
        return options
    }

    fun getCurrentLocationRequest(lastLocation: Location): Map<String, String> {
        val options = mutableMapOf<String, String>()
        options["latlng"] = lastLocation.latitude.toString() +
                "," + lastLocation.longitude
        options["key"] = context.getString(R.string.google_maps_key)
        return options
    }

    fun getCrossingOsm(position: LatLng): String {
        val minLat = position.latitude - 0.000125f
        val maxLat = position.latitude + 0.000125f
        val minLon = position.longitude - 0.000125f
        val maxLon = position.longitude + 0.000125f

        val str = "(" + minLat.toString() + "," +
                minLon.toString() + "," +
                maxLat.toString() + "," +
                maxLon.toString() + ");out meta;"
        val query = "[out:json];node[crossing]$str"
        return query
    }
}